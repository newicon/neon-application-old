<?php

namespace neon\tests\unit\core\view;

use \neon\core\view\SmartySharedPlugins;

class SmartySharedPluginsTest extends \Codeception\Test\Unit
{
	/** @var SmartySharedPlugins */
	protected $smartySharedPlugins;

	public function setUp()
	{
		$reflector = new \ReflectionClass(SmartySharedPlugins::class);
		$this->smartySharedPlugins = $reflector->newInstanceWithoutConstructor();
	}

	public function tearDown()
	{
		$this->smartySharedPlugins = null;
	}

	/** tests for image_srcset */
	public function test_imgage_srcset_must_have_firefly_id()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset([]);
	}

	public function test_image_srcset_firefly_id_is_used()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_custom_widths()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'widths'=>'100,200,300']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=100' sizes='100px, 200px, 300px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=100 100w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=200 200w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=300 300w'>";
		$this->assertEquals($expected, $actual);

	}

	public function test_image_srcset_bad_custom_widths()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'widths'=>'blah blah blah']);
	}

	public function test_image_srcset_quality()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'quality'=>'123']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350&amp;q=123' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350&amp;q=123 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640&amp;q=123 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768&amp;q=123 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024&amp;q=123 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280&amp;q=123 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536&amp;q=123 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920&amp;q=123 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_bad_quality()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','quality'=>'blah']);
	}

	public function test_image_srcset_src_width()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'src-width'=>'100']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=100' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_src_width_and_custom_widths()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'widths'=>'100,200,300', 'src-width'=>'50']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=50' sizes='100px, 200px, 300px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=100 100w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=200 200w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=300 300w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_bad_width()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','src-width'=>'blah']);
	}

	public function test_image_srcset_breaks_px()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'breaks'=>'100px,200px']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='100px, 200px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_breaks_vw()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'breaks'=>'50vw,100vw']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='50vw, 100vw' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_breaks_bad_units()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'breaks'=>'50ii,100ii']);
	}


	public function test_image_srcset_bad_breaks()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'breaks'=>'blah blah blah']);
	}

	public function test_image_srcset_min_break_px()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','min-break'=>'100px']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='(min-width: 100px) 350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_min_break_vw()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','min-break'=>'20vw']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='(min-width: 20vw) 350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_min_break_bad_unit()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','min-break'=>'20ii']);
	}

	public function test_image_srcset_bad_min_break()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','min-break'=>'blah']);
	}

	public function test_image_srcset_resolutions()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','resolutions'=>'0.1x,0.2x,0.3x,0.4x,0.5x,0.6x']);
		$expected = "<img src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 0.1x, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 0.2x, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 0.3x, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 0.4x, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 0.5x, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 0.6x'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_resolutions_wrong_count()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','resolutions'=>'0.1x,0.2x,0.3x,0.4x,0.5x']);
	}

	public function test_image_srcset_resolutions_bad_unit()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','resolutions'=>'0.1x,0.2x,0.3x,0.4x,0.5x,0.6i']);
	}

	public function test_image_srcset_bad_resolutions()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','resolutions'=>'blah']);
	}

	public function test_image_srcset_arbitrary_attributes()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','foo'=>'bar','wibble'=>'hatstand']);
		$expected = "<img foo='bar' wibble='hatstand' src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_overwriting_generated_attributes_src()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123','src'=>'blah']);
		$expected = "<img src='blah' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_overwriting_generated_attributes_sizes()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'sizes'=>'blah']);
		$expected = "<img sizes='blah' src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' srcset='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350 350w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=640 640w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=768 768w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1024 1024w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1280 1280w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1536 1536w, http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=1920 1920w'>";
		$this->assertEquals($expected, $actual);
	}

	public function test_image_srcset_overwriting_generated_attributes_srcset()
	{
		$actual = $this->smartySharedPlugins->image_srcset(['firefly_id'=>'abc123', 'srcset'=>'blah']);
		$expected = "<img srcset='blah' src='http://neon-url-test.test/firefly/file/img?id=abc123&amp;w=350' sizes='350px, 640px, 768px, 1024px, 1280px, 1536px, 1920px'>";
		$this->assertEquals($expected, $actual);
	}

}
