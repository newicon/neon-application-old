<?php

namespace neon\cms\controllers;

use neon\core\web\AdminController;
use \neon\cms\models\CmsPage;
use \neon\cms\models\CmsUrl;

class PageController extends AdminController
{
	/**
	 * Adds a new page
	 */
	public function actionAdd()
	{
		$page = new CmsPage();
		$page->save();
	}

	/**
	 * @return array
	 */
	public function getTemplates()
	{
		$themeAlias = neon('cms')->getThemeAlias();
		$themePath  = \Neon::getAlias("$themeAlias/widgets");
		$templateFiles = \yii\helpers\FileHelper::findFiles($themePath, ['only'=>['*.tpl']]);
		$templates = [];
		foreach ($templateFiles as $t) {
			$templates[] = [
				'name' => str_replace($themePath, '', $t),
				'path' => $t,
				'content' => file_get_contents($t)
			];
		}
		return $templates;
	}

	/**
	 * @return \neon\cms\App
	 */
	public function getCms()
	{
		return neon('cms');
	}

}