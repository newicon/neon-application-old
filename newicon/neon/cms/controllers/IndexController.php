<?php

namespace neon\cms\controllers;

use neon\core\web\AdminController;


class IndexController extends AdminController
{
	public $layout = '/admin-workbench';
	public function actionIndex()
	{
		// turn edit mode off when in the grid page view
		neon()->cms->page->setEditMode(false);
		$grid = new \neon\cms\grid\PageGrid();
		return $this->render('index.tpl', [
				'grid'=> $grid,
				'can_develop' => (neon()->env == 'dev')
			]
		);
	}

}