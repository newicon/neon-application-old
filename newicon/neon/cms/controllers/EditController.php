<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 05/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\cms\controllers;

use neon\core\form\assets\FormAsset;
use neon\core\web\AdminController;
use neon\cms\models\CmsPage;
use neon\cms\models\CmsUrl;
use yii\web\HttpException;

class EditController extends AdminController
{
	public function actionIndex()
	{
		FormAsset::register($this->view);
		return $this->render('index.tpl', []);
	}

	/**
	 * Render the website preview in edit mode
	 *
	 * @param int $id request GET param
	 * @throws \yii\web\HttpException 404 if no page found
	 * @return string
	 */
	public function actionPage($id)
	{
		$page = $this->getCms()->getPage();
		if (!$page->setById($id)) {
			throw new \yii\web\HttpException(404, 'No page found');
		}

		$page->setEditMode();

		return $page->render();
	}

	/**
	 * @return \neon\cms\App
	 */
	public function getCms()
	{
		return neon('cms');
	}
}