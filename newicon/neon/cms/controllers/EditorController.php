<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 05/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\cms\controllers;

use neon\cms\form\Page;
use neon\core\web\AdminController;


class EditorController extends AdminController
{
	public $layout = '/admin-workbench';

	public function actionIndex($id=null)
	{
		$grid = new \neon\cms\grid\PageGrid();
		$pages = $grid->getRows();
		neon()->cms->page->setEditMode(true);
		return $this->render('index', [
			'pages' => $pages,
			'id' => $id
		]);
	}

	/**
	 * Add a new page
	 * @return array|string
	 * @throws \yii\base\UnknownPropertyException
	 */
	public function actionPageAdd()
	{
		$pageForm = new Page();
		if (neon()->request->isAjax) {
			return $pageForm->ajaxValidation();
		}
		if ($pageForm->processRequest()) {
			if (!$pageForm->save($errors)) {
				// something went wrong
				throw new \Exception( 'Something went wrong ' . print_r($errors, true));
			}
			$data = $pageForm->getData();
			return $this->redirect(['/cms/editor/index', 'id' => $data['nice_id']]);
		}
		return $this->render('add.tpl', ['page' => $pageForm]);
	}

	/**
	 * Show a message when no page is selected
	 *
	 * @return string
	 */
	public function actionSelectPage()
	{
		return '';
	}
}