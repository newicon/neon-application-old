<?php

namespace neon\cms\controllers;

use neon\core\web\Controller;
use yii\filters\AccessControl;
use \neon\user\services\apiTokenManager\JwtTokenAuth;
use yii\web\HttpException;

/**
 * The RenderController will draw a page using Cobe. It needs to be both
 * non-secure for public pages and secure for private role-based pages
 */
class RenderController extends Controller
{
	private $_secure = false;
	private $_roles = [];

	public function __construct($id, $module, $config = [])
	{
		profile_begin('__construct');
		parent::__construct($id, $module, $config);
		// determine whether or not the page request needs to be secure
		if (neon('user')->routeHasRoles('/'.neon()->getRequest()->getPathInfo(), $roles)) {
			$this->_secure = true;
			$this->_roles = $roles;
		}
		profile_end('__construct');
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		profile_begin('controller-before-action');
		if ($this->_secure) {
			// Generate the JWT api token - allows logged in users to have access
			// to REST api methods without the need to generate an API token
			JwtTokenAuth::createCookie(neon()->request, neon()->response);
		}
		$return = parent::beforeAction($action);
		profile_end('controller-before-action');
		return $return;
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviours = parent::behaviors();
		if ($this->_secure) {
			$behaviours = array_merge($behaviours, [
				'access' => [
					'class' => AccessControl::className(),
					'rules' => [
						// allow authenticated users
						[
							'allow' => true,
							'roles' => $this->_roles
						],
						// everything else is denied
					],
				],
			]);
		}
		return $behaviours;
	}

	/**
	 * Render a CMS Page
	 * @return string
	 * @throws \yii\web\HttpException
	 */
	public function actionPage($nice_id)
	{
		\Neon::beginProfile('COBE::RENDER_ACTION', 'cobe');
		$cms = $this->getCms();
		$page = $cms->getPage();

		// if the page exists it should already be loaded by the router process but if not

		if (!$page->hasPage()) {
			$this->pageNotFound();
		}

		// Why? Good question!
		if (($theme = neon()->request->get('theme')) && !neon()->user->isGuest) {
			// override either default or config settings via get
			$cms->setThemeName($theme);
		}

		// set the page mode
		if (neon()->request->get('edit') && !neon()->user->isGuest) {
			$page->setEditMode(true);
		}

		$render = $page->render();
		if ($render === false)
			throw new \yii\web\HttpException(404, 'No template found');

		\Neon::endProfile('COBE::RENDER_ACTION', 'cobe');
		return $render;
	}

	/**
	 * Set up the page from the routing information
	 * @return type
	 */
	public function actionPageFromRoute()
	{
		$request = neon()->request;
		$url = $request->get('page');
		if (!$url)
			$this->pageNotFound();
		\Neon::app('cms')->page->setByUrl($url);
		$pageData = neon()->getCms()->getPage()->getPageData();
		return $this->actionPage($pageData['nice_id']);
	}

	/**
	 * @return \neon\cms\App
	 */
	public function getCms()
	{
		return neon('cms');
	}

	/**
	 * Throw a page not found exception
	 * @throws \yii\web\HttpException
	 */
	protected function pageNotFound()
	{
		throw new \yii\web\HttpException(404, 'No page found');
	}
}