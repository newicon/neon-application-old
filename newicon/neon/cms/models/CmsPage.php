<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2008 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */
namespace neon\cms\models;

use neon\core\db\ActiveRecord;
use neon\cms\models\CmsUrl;

/**
 * CmsPage model
 *
 * The idea is that each url has a dedicated page.  A page then has content attached to it.
 * For example a page might show a an ecommerce product, a blog post and an author, each of these
 * items needs to be attached to the page.
 *
 * You could imagine a product manager for example would have to add a product and under the hood
 * configure a page record that shows the product.  This allows you to set custom page type things
 * and I think gives maximum flexibility, for example different templates for each page if you really
 * want.   However another approach could be to make a generic routes lookup table that defers everything
 * to the routing.
 */
class CmsPage extends ActiveRecord
{
	const STATUS_PUBLISHED = 'PUBLISHED';
	const STATUS_DRAFT = 'DRAFT';

	public static $timestamps = true;
	public static $blame = true;
	public static $idIsUuid64 = true;
	public static $softDelete = true;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%cms_page}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['nice_id', 'unique'],
			[['name', 'title', 'status', 'description', 'keywords', 'template', 'status'], 'safe']
		];
	}

	/**
	 * Get the canonical url for this Page
	 * @return \yii\db\ActiveQuery
	 */
	public function getUrl()
	{
		return $this->hasOne(CmsUrl::class, ['page_id' => 'nice_id']);
	}

}