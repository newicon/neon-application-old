<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2008 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */



namespace neon\cms\models;

use neon\core\db\ActiveRecord;

/**
 * CmsPage model
 * 
 * The idea is that each url has a dedicated page.  A page then has content attached to it.
 * For example a page might show a an ecommerce product, a blog post and an author, each of these
 * items needs to be attached to the page.
 * 
 * You could imagine a product manager for example would have to add a product and under the hood
 * configure a page record that shows the product.  This allows you to set custom page type things
 * and I think gives maximum flexibility, for example different templates for each page if you really
 * want.   However another approach could be to make a generic routes lookup table that defers everything
 * to the routing.
 *
 * @property string nice_id - the nice_id of the page this url points to
 * @property string url
 */
class CmsUrl extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%cms_url}}';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['url', 'unique'],
			[['nice_id', 'url'], 'safe']
		];
	}
}
