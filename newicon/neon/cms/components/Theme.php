<?php

namespace neon\cms\components;

use \Neon;

/**
 * This class is responsible for determing the theme information to be passed to various
 * objects that require this.
 * This includes the top level title and meta information but also loaded data for widgets
 */
class Theme
{
	/**
	 * Get hold of the theme hierarchy. This can come from a variety of sources
	 * - cms's themeHierarchy
	 * - page information from the database (not implemented yet)
	 * @return array  the theme hierarchy
	 */
	public static function getHierarchy()
	{
		static $hierarchy = null;

		if ($hierarchy === null) {
			// reverse themes so we can go from bottom to top in all dependencies
			$themes = array_reverse(neon('cms')->getThemeHierarchy());
			$root = self::getRoot();
			$hierarchy = [];
			if (is_string($root) && count($themes)) {
				foreach ($themes as $theme) {
					$themePath = $root.$theme;
					if (!isset($hierarchy[$theme]))
						$hierarchy[$theme] = [];

					// add the default template and plugin directories
					$hierarchy[$theme]['templates'][] = $themePath;
					$hierarchy[$theme]['templates'][] = $themePath.'/widgets';
					$hierarchy[$theme]['plugins'][] = $themePath.'/plugins';

					// add any component directories that are being used
					$themeComponents = self::getComponents($theme);
					foreach ($themeComponents as $theme=>$componentSet) {
						foreach ($componentSet as $component) {
							$hierarchy[$theme]['templates'][] = $root.$component;
							$hierarchy[$theme]['plugins'][] = $root.$component;
						}
					}
				}
			}
		}
		// reverse hiearchy back again to match order required for plugins
		$hierarchy = array_reverse($hierarchy);
		return $hierarchy;
	}

	/**
	 * Extract out the theme component information in a format suitable for
	 * the theme hiearchy. Components are assumed to reside inside the
	 * components directory within the theme.
	 *
	 * @param string $root  the root to the themes directory
	 * @param string $path  the path to the theme / components from the root path
	 *   initially this is the path to the theme. During recursion this is the
	 *   components path.
	 * @param array &$components - these are passed during recursive reads
	 * @param array &$read - the set of read components to prevent infinite looping
	 * @return array  the set of components
	 */
	private static function &getComponents($path, &$components=[], &$read=[])
	{
		$root = self::getRoot();
		$configFile = $root.$path.'/_config.php';
		if (file_exists($configFile)) {
			$config = require $configFile;
			try {
				if (!empty($config['requires']['components'])) {
					$requiredComponents = &$config['requires']['components'];
					foreach ($requiredComponents as $theme => $componentSet) {
						$componentSet = is_string($componentSet) ? [$componentSet] : $componentSet;
						foreach ($componentSet as $component) {
							// if we haven't seen this component before ...
							if (!isset($read[$theme][$component])) {
								$read[$theme][$component] = true;
								// ... add it and then add its component configuration
								$compath = "{$theme}/components/$component";
								// ... but go depth first
								$components = self::getComponents($compath, $components, $read);
								$components[$theme][$component] = $compath;
							}
						}
					}
				}
			} catch (\Exception $e) {
				throw new \InvalidArgumentException("Something went wrong when trying to parse the config file $configFile. The error was ".$e->getMessage(). ". The file contained <pre>".print_r($config,true)."</pre>.");
			}
		}
		return $components;
	}

	/**
	 * Get the theme root
	 * @return string
	 */
	private static function getRoot()
	{
		static $root = null;
		if ($root === null)
			$root = Neon::getAlias('@root/themes/');
		return $root;
	}
}
