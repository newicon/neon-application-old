<?php

namespace neon\cms\components;

use \neon\cms\models\CmsUrl;
use \neon\cms\models\CmsPage;
use \neon\cms\components\Theme;
use \neon\cms\components\Renderer;
use neon\core\form\assets\CkeditorAsset;
use \neon\core\helpers\Url;
use \neon\core\helpers\Html;
use yii\base\ViewContextInterface;

/**
 * This class is responsible for getting the data for a page
 * This includes the top level title and meta information but also loaded data for widgets
 *
 * See @render to understand how in-template data calls are made and affect the rendering
 */
class Page implements ViewContextInterface
{
	const COBE_EDITING = 'COBE_EDITING';

	/**
	 * the page we landed on initially
	 */
	protected $_page = null;

	/**
	 * the url requested for the page
	 */
	protected $_url = null;

	/**
	 * the set of requested data for a page
	 * @var array
	 */
	protected $requestedData = [];

	/**
	 * for dynamic requests, contains the 'start', 'length' and 'total
	 * counts for the data
	 * @var array
	 */
	protected $requestedDataMeta = [];

	/**
	 * the set of unactioned static data requests made for a page
	 * @var array
	 */
	protected $staticDataRequests = [];

	/**
	 * the set of unactioned dynamic data requests made for a page
	 * @var array
	 */
	protected $dynamicDataRequests = [];

	/**
	 * whether or not this page is in edit mode
	 */
	private $_editMode = false;

	/**
	 * ===============================================================================
	 * Methods on the current page
	 * ===============================================================================
	 * Methods related to the page landed on (the 'current' page)
	 */

	/**
	 * whether or not the page is in edit mode
	 * @return bool
	 */
	public function isInEditMode()
	{
		if (neon()->session===null) return false;
		return neon()->session->get(self::COBE_EDITING, false);
	}

	const CMS_STATIC_TABLE = 'cms_static_content';

	public function getViewPath()
	{
		return neon()->getAlias('@root/themes/default');
	}

	/**
	 * Set the page into edit mode
	 * You must be logged in to be able to edit pages
	 * @param bool $editable
	 */
	public function setEditMode($editable)
	{
		$editingRoles = neon('cms')->editingRoles;
		if (!empty($editingRoles) && neon()->user->hasRoles($editingRoles)) {
			neon()->session->set(self::COBE_EDITING, (bool) $editable);
		} else {
			// no permission to edit so remove session variable
			neon()->session->remove(self::COBE_EDITING);
		}
	}

	/**
	 * Performs necessary page changes when in edit mode
	 */
	public function handleEditMode()
	{
		if ($this->isInEditMode()) {
			\neon\cms\assets\CmsEditorAsset::register(neon()->view);
		}
	}

	/**
	 * Whether the database page data has been found and set
	 * @return bool
	 */
	public function hasPage()
	{
		return $this->_page ? true : false;
	}

	/**
	 * Sets a page by its URL
	 * @param string $url  url starting with /
	 * @return boolean true if the page is set
	 */
	public function setByUrl($url)
	{
		$this->_url = $url;
		$page = CmsPage::find()
			->select('cms_page.*')
			->innerJoin(CmsUrl::tableName(), 'cms_page.nice_id = cms_url.nice_id')
			->where(['cms_url.url' => $url, 'status' => CmsPage::STATUS_PUBLISHED])
			->asArray()
			->one();
		$this->setPageFromDb($page);
		return $this->hasPage();
	}

	/**
	 * Sets a page by its id
	 * @param $pageId  the id or nice_id of the page
	 * @return boolean  true if the page is set
	 */
	public function setById($pageId)
	{
		$page = CmsPage::find()
			->where(['nice_id' => $pageId])
			->orWhere(['id' => $pageId])
			->asArray()
			->one();
		$this->setPageFromDb($page);
		return $this->hasPage();
	}

	/**
	 * Get the layout file if it's set
	 * @return string|null
	 */
	public function getLayout()
	{
		return isset($this->_page['layout']) ? $this->_page['layout'] : null;
	}

	/**
	 * Gets the template file from the database row data
	 * @return string|null
	 */
	public function getTemplate()
	{
		return isset($this->_page['template']) ? $this->_page['template'] : null;
	}

	/**
	 * Get the database page information
	 *
	 * @return null
	 */
	public function getPageData()
	{
		// protect against missing pages
		$data = ['page_params'=>[]];
		if (!empty($this->_page)) {
			$data = $this->_page;
			// add the nice_id as CmnUrRoute will add this on first parse through Cobe without gets but
			// not on requests with get parameters which has led to hard-to-debug runtime coding errors
			if (!isset($data['page_params']['nice_id']) && isset($data['nice_id']))
				$data['page_params']['nice_id'] = $data['nice_id'];
		}
		$params = array_merge($data['page_params'], neon()->request->get(), neon()->request->post());
		unset($data['page_params']);
		$data['params'] = Html::encodeEntities($params);
		$data['url'] = $this->_url;
		$data['uri'] = $_SERVER['REQUEST_URI'];
		return $data;
	}

	public function getId()
	{
		return $this->hasPage() ? $this->_page['id'] : null;
	}

	/**
	 * Get the page title as defined by the database
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->hasPage() ? $this->_page['title'] : '';
	}

	/**
	 * ===============================================================================
	 * Other pages
	 * ===============================================================================
	 * Methods dealing with finding information about other pages by id, nice_id
	 * and their URLs
	 */

	/**
	 * Get the main url for a page from its id
	 * @param string  $id  The id of a page or the nice id
	 * @return string
	 */
	public function getUrlOfPage($id)
	{
		if (!$id)
			return '';
		$url = $this->loadUrlByPageId($id);
		return $url ? url($url['url']) : '';
	}

	/**
	 * Get the main url for a page from its nice_id
	 *
	 * @param string  $niceId  The nice id of a page
	 * @return string
	 */
	public function getUrlOfPageByNiceId($niceId)
	{
		$url = '';
		if ($niceId)
			$url = $this->loadUrlByNiceId($niceId);
		return $url ? url($url['url']) : '';
	}

	/**
	 * get the page id from the page's nice id
	 * @param type $niceId
	 * @return null|string  the page id or null if not found
	 */
	public function niceId2PageId($niceId)
	{
		if (!$niceId)
			return null;
		if (isset($this->_niceId2PageId[$niceId]))
			return $this->_niceId2PageId[$niceId];
		$url = $this->loadUrlByNiceId($niceId);
		return $url ? $url['page_id'] : null;
	}

	/**
	 * get the page's nice id from its page id
	 * @param string $pageId
	 * @return null|string  the page id or null if not found
	 */
	public function pageId2NiceId($pageId)
	{
		if (!$pageId)
			return null;
		if (isset($this->_pageId2NiceId[$pageId]))
			return $this->_pageId2NiceId[$pageId];
		$url = $this->loadUrlByPageId($pageId);
		return $url ? $url['page_id'] : null;
	}

	/**
	 * ===============================================================================
	 * Template functions
	 * ===============================================================================
	 * note these should not be aware of smarty as we may want to switch the template
	 * engine in the future (twig or other) but should do the work
	 *
	 */

	/**
	 * Get the global site information
	 *
	 * @return array
	 */
	public function getSiteData()
	{
		return neon('cms')->siteData;
	}

	/**
	 * Will force copy assets when in dev mode
	 * @param $path
	 * @return string
	 */
	public function asset($path)
	{
		$options = [];
		$themeAssets = neon('cms')->getThemeAlias().'/assets';
		list($assetPath, $assetUrl) = neon()->assetManager->publish($themeAssets, $options);

		$path = ltrim($path, '/');
		$timestamp = @filemtime("$assetPath/$path");

		return $assetUrl . '/' . $path . (($timestamp > 0) ? '?v='.$timestamp : '');
	}

	/**
	 * Add header scripts to the page
	 * @return string
	 */
	public function headScript()
	{
//		$out = Html::csrfMetaTags();
		// This can all be done elsewhere and just register into the head scripts.
//		if ($this->isInEditMode()) {
//			$options = [];
//			$res = neon()->assetManager->publish('@neon/cms/assets', $options);
//			$url = $res[1];
//			$out .= '<link rel="stylesheet" type="text/css" href="'."$url/editor/theme.css".'"></link>';
//		}
//		return $out;
	}

	/**
	 * Add footer scripts to the page
	 */
	public function bodyScript()
	{
//		$out = '<script> var neon = {url: {base: "' . Url::base() . '"} }; </script>';
//		if ($this->isInEditMode()) {
//			CmsEditorAsset::register(neon()->view);
//			$cms = neon('cms');
//			$formAssets = new CkeditorAsset();
//			list($path, $url) = neon()->assetManager->publish($formAssets->sourcePath);
//			$out .=
//				'<script src="' . $cms->assetsUrl() . '/vendor/jquery.min.js" type="text/javascript"></script>'
//				. '<script src="' . $url . '/ckeditor.js" type="text/javascript"></script>'
//				. '<script src="' . $url . '/adapters/jquery.js" type="text/javascript"></script>'
//				. '<script src="' . $cms->assetsUrl() . '/vendor/jquery.ui.min.js" type="text/javascript"></script>'
//				. '<script src="' . $cms->assetsUrl() . '/editor/editor.js" type="text/javascript"></script>';
//		}
//		return $out;
	}

	/**
	 * ===============================================================================
	 * Page rendering, static and dynamic data methods
	 * ===============================================================================
	 * Methods relating to the rendering of the page and getting data
	 * from the database
	 */

	/**
	 * Renders the page
	 *
	 * If during rendering in-template data requests are made then the page is re-rendered
	 * once the data has been collected and passed to the template. If there are additional
	 * data requests that rely on the results from previous ones, then these are collected
	 * on additional parses. This means data can be collected in a dynamic and flexible
	 * way and data can be collected in bulk calls rather than individual calls throughout
	 * a page.
	 *
	 * This works well when the data requests are heavy compared to rendering time
	 * however it works not so well when rendering time is heavy compared to the db calls.
	 *
	 * To improve the rendering time of the page keep the data requests either at the top
	 * of the page allowing no rendering until all the data has been collected, or put them
	 * into a plugin called at the top of the page. Ensure the plugin uses the request
	 * commit approach of Daedalus rather than making individual calls so that it can take
	 * advantage of Daedalus' bulk database calling.
	 *
	 * @return html | bool  the rendered page or false if the template couldn't be found
	 */
	public function render()
	{
		$this->handleEditMode();

		// use this to trouble shoot slow page rendering
		static $numberOfRenders = 0;

		\Neon::beginProfile('COBE::RENDER_PAGE', 'cobe');

		$template = $this->getTemplate();
		if ($template === null) {
			\Neon::endProfile('COBE::RENDER_PAGE', 'cobe');
			return false;
		}

		// render the template - we repeat this until we have no data requests to
		// manage. Each time the page is rendered, widgets can make a data request.
		// If the data isn't available this is added to a list of new data requests
		// and eventually (assuming no circular references) we'll have none left
		// at which point our job here is done (happy sigh)
		$renderer = $this->getRenderer();
		$html = '';
		$hasDataRequests = false;

		// render the page until all data sources are fulfilled
		do {
			$numberOfRenders++;
			$html = $renderer->renderTemplate($template);
			if (($hasDataRequests=$this->hasDataRequests()))
				$this->makeDataRequests();
		} while ($hasDataRequests == true);

		// If the number of renders are excessive, issue a warning.
		// Simple cases can result a few renders so take anything over
		// 4 renders as excessive.
		if ($numberOfRenders >= 5)
			\Neon::debug('Excessive number of renders (>=5): ' . $numberOfRenders);

		// inject assets into template
		$html = neon()->view->injectHtml($html);

		\Neon::endProfile('COBE::RENDER_PAGE', 'cobe');
		return $html;
	}

	/**
	 * A widget requests its data from the page, passing in its data request
	 * If the data exists, it is returned, and if not the request is noted
	 * ready for the next bout of requests to Daedalus or static data
	 * @param string | array $dataRequest
	 * @param array &$data  the returned data (which can be an empty array)
	 * @param string &$id  on return set to a unique identifier for the request.
	 * @param boolean $isStatic  whether or not this request is for static or
	 *   dynamic (daedalus) data
	 * @param array &$meta  if the data is dynamic, this is set to the
	 *   meta information for that request (currently start, length, total)
	 * @return boolean true if data was returned and false if the data was
	 *   not yet available
	 */
	public function requestWidgetData($dataRequest, &$data, &$id, $isStatic, &$meta)
	{
		$data = [];
		$meta = null;
		$id = $this->canonicaliseRequest($dataRequest, $isStatic);
		// see if we have the data
		if (isset($this->requestedData[$id])) {
			$data = $this->requestedData[$id];
			if (!$isStatic && isset($this->requestedDataMeta[$id]))
				$meta = $this->requestedDataMeta[$id];
			return true;
		}
		// if not then add it to the list of requested data
		if ($isStatic)
			$this->staticDataRequests[$id] = $dataRequest;
		else
			$this->dynamicDataRequests[$id] = $dataRequest;
		return false;
	}

	/**
	 * ===============================================================================
	 * Protected and Private functions
	 * ===============================================================================
	 * Implementation details methods
	 */

	/**
	 * Get hold of a renderer that can render the page
	 * @return \neon\cms\components\Renderer
	 */
	private function getRenderer()
	{
		if ($this->_renderer === null) {
			$this->_renderer = new Renderer($this, Theme::getHierarchy());
		}
		return $this->_renderer;
	}
	protected $_renderer = null;

	/** ------------------------------------------------------- **/
	/** ---------- Bulk loading of urls and page ids ---------- **/

	/**
	 * all of the urls and page ids from the database
	 * These are loaded once if required to save multiple database calls.
	 * They are indexed either by the page or nice ids
	 */
	private $_allUrlsByPageId = null;
	private $_allUrlsByNiceId = null;
	private $_pageId2NiceId = null;
	private $_niceId2PageId = null;

	/**
	 * Get an individual url details by a page's Id.
	 * @param string $id
	 * @return string | null if not found
	 */
	protected function loadUrlByPageId($id)
	{
		return $this->loadUrlByNiceId($id);
	}

	/**
	 * Get an individual url details by its niceId.
	 * @param string $id
	 * @return string | null if not available
	 */
	protected function loadUrlByNiceId($id)
	{
		if (!$id)
			return null;
		$this->loadUrls();
		if (isset($this->_allUrlsByNiceId[$id]))
			return $this->_allUrlsByNiceId[$id];
		if (isset($this->_allUrlsByPageId[$id]))
			return $this->_allUrlsByPageId[$id];
		return $this->loadUrl($id);
	}

	/**
	 * This loads in a single url via the provided field
	 * @param string $id  the id to search for - can be a nice_id or an id (uuid)
	 * @return $url | null if not available
	 */
	protected function loadUrl($id)
	{
		if (!$id)
			return null;
		$url = CmsUrl::find()->select("cms_url.*, cms_page.id as page_id")
			->innerJoin('cms_page','`cms_page`.`nice_id`=`cms_url`.`nice_id`')
			->where(['cms_page.id' => $id])
			->orWhere(['cms_url.nice_id' => $id])
			->asArray()->one();
		if ($url)
			$this->storeUrl($url);
		return $url;
	}

	/**
	 * This loads up to $limit pages from the database in one go
	 * which should cover most websites. Additional urls can be
	 * added using @see loadUrlByPageId
	 * @param integer $limit  max number to load in
	 */
	protected function loadUrls($limit=500)
	{
		if ($this->_allUrlsByPageId === null) {
			$urls = CmsUrl::find()
				->select("cms_url.*, cms_page.id as page_id")
				->innerJoin('cms_page','`cms_page`.`nice_id`=`cms_url`.`nice_id`')
				->asArray()->limit($limit)->all();
			$this->_allUrlsByPageId = [];
			foreach ($urls as $u) {
				$this->storeUrl($u);
			}
		}
	}

	/**
	 * store a url row and associated data
	 * @param type $url
	 */
	protected function storeUrl($url)
	{
		$this->_allUrlsByPageId[$url['page_id']] = $url;
		$this->_allUrlsByNiceId[$url['nice_id']] = $url;
		$this->_niceId2PageId[$url['nice_id']] = $url['page_id'];
		$this->_pageId2NiceId[$url['page_id']] = $url['nice_id'];
	}

	/** ----- end - bulk loading of urls and page ids - end ----- **/
	/** --------------------------------------------------------- **/


	/**
	 * convert a request into a format that can be used in the rest of the
	 * page component. Return an id based on a canonicalised form of the request
	 * @param array | string $dataRequest  the request or a request key.
	 *   The content of this depends on whether or not this is a static or dynamic
	 *   request. For static ones, this should contain a 'key' and 'page_id'. The
	 *   page_id can be null. For dynamic calls, it should contain any or all of
	 *   'class', 'filters', 'order', and 'limit' or be a data request string
	 * @param
	 * @return string
	 */
	protected function canonicaliseRequest(&$dataRequest, $isStatic=false)
	{
		if ($isStatic) {
			$id = $this->createStaticContentId($dataRequest);
			$dataRequest['id'] = $id;
		} else {
			$request = array('class'=>null, 'filters'=>[], 'order'=>[], 'limit'=>[]);
//			if (is_string($dataRequest)) {
//				// TODO IF THIS IS A STRING THEN WE SHOULD ASK THE DB FOR THE ACTUAL REQUEST
//	// HACK ALERT - TEMPORARY CODE
//				// meanwhile hack this to use the data files
//				$id = md5($dataRequest);
//				if (!isset($this->requestedData[$id])) {
//					$data = $this->getDataFile($dataRequest);
//					$this->requestedData[$id] = $data;
//				}
//				return $id;
//	// END OF HACK ALERT - GO HOME NOTHING TO SEE
//			}
			$dataRequest = array_merge($request, $dataRequest);
			$id = md5(str_replace([' '],'', json_encode($dataRequest)));
		}
		return $id;
	}

	/**
	 * Make any data requests and store the results.
	 */
	protected function makeDataRequests()
	{
		\Neon::beginProfile('COBE::MAKE_DATA_REQUESTS', 'cosmos');
		$this->makeDynamicDataRequests();
		$this->makeStaticDataRequests();
		\Neon::endProfile('COBE::MAKE_DATA_REQUESTS', 'cosmos');
	}


	/**
	 * Make any data requests to DDS and store the results.
	 * This then clears the data requests so if you need to know if there
	 * were any data requests, use @see hasDataRequests before calling
	 * this method
	 */
	protected function makeDynamicDataRequests()
	{
		if (count($this->dynamicDataRequests)>0) {
			$dds = $this->getIDdsObjectManagement();
			foreach ($this->dynamicDataRequests as $id=>$dr) {
				$dds->addObjectRequest($dr['class'], $dr['filters'], $dr['order'], $dr['limit'], $id);
			}
			$data = $dds->commitRequests();
			foreach ($data as $i=>$d) {
				$this->requestedData[$i] = $d['rows'];
				$this->requestedDataMeta[$i] = [
					'start' => $d['start'],
					'length' => $d['length'],
					'total' => $d['total']
				];
			}
			$this->dynamicDataRequests = [];
		}
	}

	/**
	 * Make any static data requests to CosMoS and store the results.
	 * This then clears the data requests so if you need to know if there
	 * were any data requests, use @see hasDataRequests before calling
	 * this method
	 */
	protected function makeStaticDataRequests()
	{
		if (count($this->staticDataRequests)>0) {
			$cms = $this->getICmsStaticData();
			$data = $cms->bulkStaticContentRequest($this->staticDataRequests);
			foreach ($data as $i=>$d)
				$this->requestedData[$i] = ($d===null ? 'not found' : $d);
			$this->staticDataRequests = [];
		}
	}

	/**
	 * Check to see if there are any data requests available
	 * @return boolean
	 */
	protected function hasDataRequests()
	{
		return (count($this->staticDataRequests)+count($this->dynamicDataRequests) > 0);
	}

	/**
	 * Get hold of an object manager
	 * @return \neon\daedalus\services\ddsManager\interfaces\IDdsObjectManagement
	 */
	protected function getIDdsObjectManagement()
	{
		if (!$this->_iDdsObjectManagement) {
			$this->_iDdsObjectManagement = neon('dds')->IDdsObjectManagement;
		}
		return $this->_iDdsObjectManagement;
	}
	protected $_iDdsObjectManagement = null;

	/**
	 * create a unique id from a static page request key and page id
	 * @param type $request
	 * @return string
	 */
	private function createStaticContentId($request) {
		return $this->getICmsStaticData()->createStaticContentId($request);
	}

	/**
	 * private reference to a cmsStaticData handler
	 * @var neon\cms\services\cmsManager\interfaces\ICmsStaticData
	 */
	private $cmsStaticData = null;
	private function getICmsStaticData()
	{
		if ($this->cmsStaticData === null)
			$this->cmsStaticData = neon('cms')->ICmsStaticData;
		return $this->cmsStaticData;
	}

	/**
	 * Set the page retrieved from the database and perform any conversions
	 * @param array $page
	 */
	private function setPageFromDb($page)
	{
		if ($page) {
			$pageParams = json_decode($page['page_params'], true);
			$page['page_params'] = empty($pageParams) ? [] : $pageParams;
			$this->_page = $page;
		}
	}

}
