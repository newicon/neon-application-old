<?php

namespace neon\cms\components;

use \Neon;
use neon\core\helpers\Arr;
use neon\core\helpers\Collection;
use neon\core\helpers\Url;
use \Smarty;
use \neon\cms\components\Widget;
use \neon\cms\components\Daedalus;
use \neon\core\view\SmartySharedPlugins;

Class Renderer extends Smarty
{
	const RENDER_STATE_REGISTRATION = 'RENDER_STATE_REGISTRATION';
	const RENDER_STATE_WIDGETS = 'RENDER_STATE_WIDGETS';

	/**
	 * a cached copy of the page - use getPage() to get this
	 * @var \neon\cms\components\Page
	 */
	private $_page = null;

	public function __construct(\neon\cms\components\Page $page, array $themeHierarchy)
	{
		//$this->escape_html = true;
		$this->setPage($page);
		// Class Constructor.
		parent::__construct();

		$this->setCompileDir(Neon::getAlias('@runtime/smarty/compile'));
		$this->setCacheDir(Neon::getAlias('@runtime/smarty/cache'));
		// set up the theme directories
		foreach ($themeHierarchy as $theme) {
			if (isset($theme['templates'])) {
				foreach ($theme['templates'] as $tplDir)
					$this->addTemplateDir($tplDir);
			}
			if (isset($theme['plugins'])) {
				foreach ($theme['plugins'] as $pluginDir)
					$this->addPluginsDir($pluginDir);
			}
		}

		// add CMS core theme
		$cmsCoreThemeDir = Neon::getAlias('@neon/cms/theme/');
		if (is_string($cmsCoreThemeDir)) {
			$this->addTemplateDir($cmsCoreThemeDir);
			$this->addPluginsDir([$cmsCoreThemeDir.'plugins']);
		}
		$this->setCaching(false);

		// -------------------------
		// Register template plugins
		// -------------------------
		// configure a default handler enables custom plugin tags (assumes its a CmsWidget)
		$this->registerDefaultPluginHandler([$this, 'pluginHandler']);
		// We have to set cachable to false otherwise it only caches the id output not the full rendering
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'widget', ['\neon\cms\components\Widget', 'factory'], false);
		$this->registerPlugin(Smarty::PLUGIN_BLOCK, 'drop_zone', [$this, 'layout'] , false);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'layout', [$this, 'layout'] , false);

		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'asset', [$this, 'asset']);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'dds', ['\neon\cms\components\Daedalus', 'dds'], false);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'dds_choice', ['\neon\cms\components\Daedalus', 'dds_choice'], false);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'dds_map', ['\neon\cms\components\Daedalus', 'dds_map'], false);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'page_url', [$this, 'page_url']);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'on_page', [$this, 'on_page']);
		$this->registerPlugin(Smarty::PLUGIN_FUNCTION, 'is_logged_in', [$this, 'is_logged_in']);

		// Wrap this tag around content to become editable
		$this->registerPlugin(Smarty::PLUGIN_BLOCK, 'static_block', ['\neon\cms\components\Widget', 'staticBlock']);

		// Apply shared core plugins
		// =========================
		SmartySharedPlugins::apply($this->getPage(), $this);
		$this->setCompileCheck(env('NEON_DEBUG'));
	}

	/**
	 * Default Plugin Handler
	 *
	 * called when Smarty encounters an undefined tag during compilation
	 *
	 * @param string                     $name    name of the undefined tag
	 * @param string                     $type    tag type (e.g. Smarty::PLUGIN_FUNCTION, Smarty::PLUGIN_BLOCK,
	 *                                   Smarty::PLUGIN_COMPILER, Smarty::PLUGIN_MODIFIER, Smarty::PLUGIN_MODIFIERCOMPILER)
	 * @param Smarty_Internal_Template   $template    template object
	 * @param string                     &$callback    returned function name
	 * @param string                     &$script    optional returned script filepath if function is external
	 * @param bool                       &$cacheable    true by default, set to false if plugin is not cachable (Smarty >= 3.1.8)
	 * @return bool                      true if successfull
	 */
	public function pluginHandler($name, $type, $template, &$callback, &$script, &$cacheable)
	{
		// if already registered and type is block then we need to unregister the name as a function and change to a block this is because:
		// smarty first encounters an unknown opening tag it calls this function and adds the callback as type function
		// then it discovers the closing tag {/tag_name} this time it calls the function again
		switch ($type) {
			case Smarty::PLUGIN_FUNCTION:
				$callback = '\neon\cms\components\Renderer::registerUnknownTag___'.$name;
				$cacheable = false;
				return true;
				break;
			default:
				return false;
		}
	}

	/**
	 * Handles unknown smarty tags
	 *
	 * @since PHP 5.3
	 * @param $name
	 * @param $arguments
	 * @return string
	 */
	public static function __callStatic($name, $arguments)
	{
		$bits = explode('___', $name);
		if (count($bits) === 2 && strpos($bits[0], 'registerUnknown') === 0) {
			$params = $arguments[0];
			$template = $arguments[1];
			$type = $bits[1];
			$posClass = str_replace('_', '\\', $type);
			switch ($type) {
				default:
					$class = '\neon\cms\Components\Widget';
				break;
			}
			$params['type'] = $bits[1];
			$method = 'factory';
			return $class::$method($params, $template);
		}
	}

	/**
	 * Render the main template - note this will not render widgets at this phase.
	 * Any rendered widgets will output their id tag instead of their widget content
	 * @param $template
	 * @return mixed
	 */
	public function renderTemplate($template)
	{
		// keep a store of the created templates in case of multiple rendering
		static $templates = [];

		\Neon::beginProfile('COBE::RENDER_TEMPLATE - '.$template, 'cobe');

		// assign any template information from the page
		$this->assign('page', $this->_page->getPageData());
		$this->assign('site', $this->_page->getSiteData());

		// an expensive-ish call so reduce need for it in repeated renders
		if (!isset($templates[$template]))
			$templates[$template] = $this->createTemplate($template);
		$tpl = $templates[$template];

		// reset any parameters for the new render
		$tpl->tpl_vars = $tpl->config_vars = [];
		$ret = $tpl->fetch();

		\Neon::endProfile('COBE::RENDER_TEMPLATE - '.$template, 'cobe');
		return $ret;
	}

	/**
	 * Get the cms page object
	 * @return \neon\cms\components\Page
	 */
	public function getPage()
	{
		if (!$this->_page) {
			$this->setPage(neon('cms')->getPage());
		}
		return $this->_page;
	}

	/**
	 * Set the cms page object
	 * @param \neon\cms\components\Page $page
	 */
	public function setPage($page)
	{
		$this->_page = $page;
		Widget::$page = $this->_page;
		Daedalus::$page = $this->_page;
	}

/**
 * =================================================================================================================
 * Smarty adapter functions
 * =================================================================================================================
 */

	/**
	 * Usage:
	 * To get the correct url for an asset
	 *
	 * ```{asset path="path/to/theme/asset"}```
	 *
	 * @param array $params - should contain 'path' key
	 * @param $smarty
	 * @return string
	 */
	public function asset($params, $smarty)
	{
		return $this->getPage()->asset($params['path']);
	}

	/**
	 * tag: page_url
	 *
	 * Get a url for a page from it's id
	 * Usage:
	 *
	 * ```
	 *   {page_url} for the url of the current page
	 *   {page_url id="the page id"} if you know the page id
	 *   {page_url nice="the page nice id"} if you know the nice id
	 * ```
	 * Optional parameters:
	 *   full=true if you want the full URL include server
	 *   prefix=string  if you want to prefix the url with something
	 *     e.g. for alternative permissions on routing
	 *
	 * @param array $params
	 * @param object $smarty
	 * @throws \InvalidArgumentException if no page object is currently set and no id was specified
	 * @return string  the page URL
	 */
	public function page_url($params, $smarty=null)
	{
		$id = Arr::get($params, 'id', Arr::get($params, 'nice', false));
		if ($id === '#') return '#';
		if ($id) {
			$url = $this->getPage()->getUrlOfPageByNiceId($id);
		} else if (!$id && $this->getPage()->hasPage()) {
			$url = $this->getPage()->getUrlOfPage($this->getPage()->getId());
		} else {
			throw new \InvalidArgumentException('There is no current page - and no id or nice parameter was specified');
		}
		if (!empty($params['prefix']))
			$url = $params['prefix'].$url;

		if (isset($params['full']))
			return url_base()."$url";
		return $url;
	}

	/**
	 * Determine if we are on the current page.
	 * @see Url::isUrl
	 * Usage:
	 * ```
	 *   {on_page id='my-page-id'} for the url of the current page
	 *   {on_page id=['my-page', 'home', 'uuid']} if you know the page id
	 * ```
	 * @param $params
	 * @return boolean - if the current url matches one of the page urls
	 * @throws \yii\base\InvalidConfigException
	 */
	public function on_page($params)
	{
		$pages = Arr::getRequired($params, 'id');
		$allPages = is_array($pages) ? $pages : [$pages];
		$urls = []; foreach ($allPages as $page) $urls[] =  $this->page_url(['id' => $page]);
		return Url::isUrl($urls);
	}

	public function layout($params, $content, \Smarty_Internal_Template $template, &$repeat)
	{
		if (!$repeat) {
			// layout must have an id
			$layoutId = Arr::getRequired($params, 'id');
			$editing = $this->getPage()->isInEditMode();
			// lookup dropzone components data from layout by id
			// if no layout exists for the id then use parse $content for components
			//


			return 'hello  -- ' . $content . '---';

		}
	}

	/**
	 * See if a user is logged in
	 * usage: {if {is_logged_in}} ...
	 * @return boolean
	 */
	public function is_logged_in($params, $smarty)
	{
		return neon()->user->isGuest === false;
	}
}