<?php

namespace neon\cms\components;

use yii\web\UrlRuleInterface;
use \neon\cms\models\CmsUrl;
use yii\base\BaseObject;

class CmsUrlRule extends BaseObject implements UrlRuleInterface
{
	/**
	 * The controller action route where matching rules will be sent
	 * @var string
	 */
	public $cmsRoute = 'cms/render/page';

	/**
	 * Parse the request to see if it is a CMS route
	 * @param \yii\web\UrlManager $manager
	 * @param \yii\web\Request $request
	 * @return array|bool
	 */
	public function parseRequest($manager, $request)
	{
		$pathInfo = $this->getCanonicalPathFromRequest($request);
		$found = neon('cms')->page->setByUrl($pathInfo);
		if ($found)
			$pageData = neon('cms')->page->getPageData();
		return $found ? [$this->cmsRoute, ['nice_id' => $pageData['nice_id']]] : false;
	}

	/**
	 * Returns the canonical path from an initial request
	 *
	 * TODO - actually get this to work out canonical paths
	 * from a database table somewhere. THIS IS JUST A CONCEPT PLACEHOLDER
	 *
	 * ASSUMPTION - the canonical URL is only managed at this
	 * point - probably we need to put this EARLIER into the
	 * system so it's converted before anything can access it.
	 *
	 * @return string
	 */
	public function getCanonicalPathFromRequest($request)
	{
		// TODO - update the request info to be the canonical URL
		return 	'/'.$request->getPathInfo();
	}

	/**
	 * Convert a cms page route to its pretty url for e.g.:
	 * a route of: `['/cms/render/page', 'nice_id' => 'my_nice_id']`
	 * will return the pretty url as defined against the nice_id in the url table
	 *
	 * @param \yii\web\UrlManager $manager
	 * @param string $route
	 * @param array $params
	 * @return boolean
	 */
	public function createUrl($manager, $route, $params)
	{
		// only run this if trying to access a cms controller route otherwise
		// this would be called for every url call.
		if ($route === $this->cmsRoute && isset($params['nice_id'])) {
			$url = $this->getUrlOfPageByNiceId($params['nice_id']);
			return $url === '' ? false : $url;
		}
		return false;
	}

	/**
	 * Get the main url for a page from its nice_id
	 *
	 * @param string  $niceId  The nice id of a page
	 * @return string
	 */
	public function getUrlOfPageByNiceId($niceId)
	{
		$url = $this->loadUrlByNiceId($niceId);
		return $url ? $url['url'] : '';
	}

	/** ------------------------------------------------------- **/
	/** ---------- Bulk loading of urls and page ids ---------- **/

	/**
	 * all of the urls and page ids from the database
	 * These are loaded once if required to save multiple database calls.
	 * They are indexed either by the page or nice ids
	 */
	private $_allUrlsByPageId = null;
	private $_allUrlsByNiceId = null;
	private $_pageId2NiceId = null;
	private $_niceId2PageId = null;

	/**
	 * Get an individual url details by a page's Id.
	 * @param string $id
	 * @return array
	 */
	protected function loadUrlByPageId($id)
	{
		$this->loadUrls();
		if (isset($this->_allUrlsByPageId[$id]))
			return $this->_allUrlsByPageId[$id];
		return $this->loadUrl($id, 'cms_page.id');
	}

	/**
	 * Get an individual url details by its niceId.
	 * @param string $id
	 * @return array
	 */
	protected function loadUrlByNiceId($id)
	{
		$this->loadUrls();
		if (isset($this->_allUrlsByNiceId[$id]))
			return $this->_allUrlsByNiceId[$id];
		return $this->loadUrl($id, 'cms_url.nice_id');
	}

	/**
	 * This loads in a single url via the provided field
	 * @param string $id  the id to search for
	 * @param string $field  which field to use
	 * @return $url
	 */
	protected function loadUrl($id, $field)
	{
		$url = CmsUrl::find()->select("cms_url.*, cms_page.id as page_id")
			->innerJoin('cms_page','`cms_page`.`nice_id`=`cms_url`.`nice_id`')
			->where([$field => $id])->asArray()->one();
		if ($url)
			$this->storeUrl($url);
		return $url;
	}

	/**
	 * This loads up to $limit pages from the database in one go
	 * which should cover most websites. Additional urls can be
	 * added using @see loadUrlByPageId
	 * @param integer $limit  max number to load in
	 */
	protected function loadUrls($limit=500)
	{
		if ($this->_allUrlsByPageId === null) {
			$urls = CmsUrl::find()
				->select("cms_url.*, cms_page.id as page_id")
				->innerJoin('cms_page','`cms_page`.`nice_id`=`cms_url`.`nice_id`')
				->asArray()->limit($limit)->all();
			$this->_allUrlsByPageId = [];
			foreach ($urls as $u) {
				$this->storeUrl($u);
			}
		}
	}

	/**
	 * store a url row and associated data
	 * @param type $url
	 */
	protected function storeUrl($url)
	{
		$this->_allUrlsByPageId[$url['page_id']] = $url;
		$this->_allUrlsByNiceId[$url['nice_id']] = $url;
		$this->_niceId2PageId[$url['nice_id']] = $url['page_id'];
		$this->_pageId2NiceId[$url['page_id']] = $url['nice_id'];
	}

	/** ----- end - bulk loading of urls and page ids - end ----- **/
	/** --------------------------------------------------------- **/
}