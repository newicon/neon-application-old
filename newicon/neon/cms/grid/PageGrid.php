<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 12/12/2015
 * Time: 11:28
 */

namespace neon\cms\grid;

use neon\cms\models\CmsPage;
use neon\core\grid\query\IQuery;
use \neon\core\helpers\Url;
use \neon\core\helpers\Html;
use neon\user\models\User;
use yii\web\HttpException;

class PageGrid extends \neon\core\grid\Grid
{
	/**
	 * @inheritdoc
	 */
	public function configure()
	{
		$this->query = (new \yii\db\Query())
			->from('{{%cms_page}} page')
			->leftJoin('{{%cms_url}} url', 'page.nice_id = url.nice_id')
			// don't include partials as these don't work standalone
			->where("page.template NOT LIKE 'partials/%'")
			->orderBy('title');

		$this->title = 'Pages';

		$this->setPageSize(30);

		$this->addColumn('id')->setAsIndex()->setVisible(false);

		// columns
		$this->addTextColumn('nice_id')
			->setTitle('Id')
			->setDbField('page.nice_id')
			->width(100)
			->setVisible(true);

		$this->addTextColumn('name')->setTitle('Reference Name');
		$this->addTextColumn('url')->setTitle('Url slug');
		$this->addTextColumn('title');
		$this->addTextColumn('layout');
		$this->addTextColumn('template');


		$this->addButtonColumn('edit')
			->addButton('edit', ['/cms/editor/index' , 'id' => '{{nice_id}}'], '', '', '', ['data-toggle' => 'tooltip', 'title'=>'Edit']);

		$this->addScope('published', 'Published');
		$this->addScope('draft', 'Draft');
		$this->addScope('deleted', 'Deleted');
	}

	/**
	 * @inheritdoc
	 */
	public function setup()
	{
		if ($this->isScopeActive('published')) {
			$this->getColumn('nice_id')->addRowAction('actionDraft', 'Make Draft');
		}
		if ($this->isScopeActive('draft')) {
			$this->getColumn('nice_id')->addRowAction('actionPublish', 'Publish');
		}
	}

	/**
	 * Quick publish action
	 * @param string $id - page id (gets passed row column data marked as index)
	 * @throws HttpException - if no page found with id
	 */
	public function actionPublish($id)
	{
		$page = $this->_getPage($id);
		$page->status = CmsPage::STATUS_PUBLISHED;
		$page->save();
	}

	/**
	 * Quick make draft action
	 * @param string $id - page id (gets passed row column data marked as index)
	 * @throws HttpException
	 */
	public function actionDraft($id)
	{
		$page = $this->_getPage($id);
		$page->status = CmsPage::STATUS_DRAFT;
		$page->save();
	}

	/**
	 * Get user active record by id
	 * @throws HttpException 500 error if user with $id does not exist
	 * @param string $uuid
	 * @return User
	 */
	private function _getPage($uuid)
	{
		$page = CmsPage::find()->where(['id' => $uuid])->one();
		if ($page === null) {
			throw new HttpException(404, "No page exists with id '$uuid'");
		}
		return $page;
	}

	/**
	 * @param IQuery $query
	 */
	public function scopeDeleted(IQuery $query)
	{
		$query->where('deleted', '=', 1);
	}

	/**
	 * @param IQuery $query
	 */
	public function scopePublished(IQuery $query)
	{
		$query->where('status', '=', 'PUBLISHED');
	}

	/**
	 * @param IQuery $query
	 */
	public function scopeDraft(IQuery $query)
	{
		$query->where('status', '=', 'DRAFT');
	}

}
