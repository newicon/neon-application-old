<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 05/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 *
 * @var array $pages
 * @var string $id - the nice id of the page to load
 */
use \neon\core\widgets\Js;
use \neon\core\helpers\Url;
use \neon\core\helpers\Page;
?>
<style>
	#neon {height:100%;}
	#cobe {display: grid;grid-template-areas: 'header header header' 'left stage inspector';grid-template-columns: min-content auto min-content;grid-template-rows: min-content auto;height: 100%;--headerHeight: 50px;--bgPanelColor: #F4F6F9;}
	.toolbar {grid-area: header; grid-row: 1; height: var(--headerHeight);border-bottom: 1px solid #A7A7A8;background-color: var(--bgPanelColor);border-top: 1px solid #fff;}
	.tools { background-color:#E3E7ED }
	.stage {background-color: var(--bgPanelColor);padding:28px;}
	.stage_preview {height:100%; box-shadow:0 2px 8px 0 rgba(0,0,0,0.27); overflow: hidden; position:relative; }
	.stage_preview iframe { width:100%; height:100% }
	#stage_overlay {pointer-events: none;overflow: visible;}

	/** could be extracted into generic tree component: */
	.tree,.tree ul {margin:0;padding:0;list-style:none;position:relative;}
	.tree:before,.tree ul:before {content:"";display:block;width:0;position:absolute;top:0;bottom:0;left:0;}
	.tree li {margin:0;padding:0;line-height:2em;position:relative;font-weight: 500;}
	.tree li:before {content:"";display:block;width:10px;height:0;margin-top:-1px;position:absolute;top:1em;left:0;}
	.tree.isRoot > li:before {border:none;}

	li {text-align: left;}
	.tree .niNode {display:block; color:#333; white-space: nowrap; font-size:12px;line-height:27px;}
	.niNode_caret {margin-top:-2px;display: inline-block;width: 8px;height: 10px;vertical-align: middle;border-left: 8px solid #787979;border-top: 5px solid transparent;border-bottom: 5px solid transparent;margin-right:5px;}
	.niNode_icon {margin-top:-2px;display:inline-block;margin-right:5px;fill:#777; color:#777;}

	.niNode { padding:0 10px 0 10px; cursor:pointer}
	.niNode.isPage {font-weight:bold;}
	.niNode.isHover {outline:1px solid #007AFF !important}
	.niNode.isSelected {background-color: #007AFF;color: #fff;}
	.niNode.isLeaf .niNode_caret{visibility:hidden;}
	.niNode.isOpen .niNode_caret {transform:rotate(90deg);}
	.niNode.isSelected .niNode_caret {border-left-color:#fff;}
	.niNode.isSelected .niNode_icon {color:#fff;}
	/** ni:tree **/
</style>


<div id="cobe">
	<!-- title toolbar -->
	<div class="toolbar">
		<a class="btn btn-default" href="<?php echo Url::toRoute(['/cms/index/index'], true); ?>"><span class="fa fa-chevron-left">&nbsp;</span>Exit Edit Mode</a>
		&nbsp;
		<span class="workbenchHeader_title"> Cobe CMS: Select a page below to edit its content.</span>
	</div>

	<!-- sidebar navigator -->
	<div class="tools">
		<ul class="tree">
			<li v-for="page in pages"  @click="onPage = page.nice_id" :key="page.id"   >
				<div class="niNode isPage" :class="{'isSelected': (onPage==page.nice_id)}">
					<span class="niNode_icon"><i class="fa fa-file-o"></i></span>
					<span class="niNode_label" >
						<span class="page_title">{{page.name ? page.name : 'Unnamed'}}</span>
					</span>
				</div>
			</li>
		</ul>
	</div>

	<!-- stage -->
	<div class="stage">
		<div class="stage_preview">
			<div id="stage_overlay"></div>
			<iframe id="siteFrame" frameborder="0" :src="pageUrl"></iframe>
		</div>
	</div>

	<!-- sidebar inspector -->
	<div class="inspector">
		{{page}}
	</div>
</div>
<?php Page::JsBegin(); ?>
<script>
	// noinspection JSAnnotator
	const pages = <?= json_encode($pages) ?>;

	new Vue({
		el: '#cobe',
		data() {
			return {
				pages: pages,
				onPage: '<?= $id ?>'
			}
		},
		computed: {
			page() {
				return _.find(this.pages, {nice_id: this.onPage});
			},
			pageUrl() {
				return neon.url(_.get(this.page, 'url', '/cms/editor/select-page'));
			}
		},
		mounted() {
			// apply a hover effect to elements in stage window:
			// currently under development
			// works well but requires scroll position to be handled
			// setTimeout(() => {
			// 	$($('#siteFrame')[0].contentWindow.document.body).on('mousemove', e => {
			// 		var rect = $(e.target)[0].getBoundingClientRect();
			// 		$('#stage_overlay').css({left:rect.left, position:'absolute', right:rect.right, top:rect.top, width:rect.width+'px', height: rect.height+'px', border:'1px solid #0000ff'})
			// 	})
			// }, 1000);
		}
	});
	// set up communication with page inside iframe
	// function receiveMessage(event) { console.log(event, 'RECIEVED MESSAGE');}
	// window.addEventListener("cobeLoaded", receiveMessage, false);
</script>
<?php Page::JsEnd(); ?>