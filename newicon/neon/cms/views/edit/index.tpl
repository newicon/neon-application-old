<div id="editor">
	<el-container>
		<el-header>Header</el-header>
		<el-container>
			<el-aside width="200px">Aside</el-aside>
			<el-main>Main</el-main>
			<el-aside width="200px">Aside</el-aside>
		</el-container>
	</el-container>
</div>
{js}
<script>
	neon.mount('#editor')
</script>
{/js}