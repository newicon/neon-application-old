<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace neon\cms\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CmsEditorAsset extends AssetBundle
{
	public $sourcePath = __DIR__.'/publish';
	public $js = [
		(YII_DEBUG ? 'editor/editor.js' : 'editor/editor.build.js'),
	];
	public $css = [
		'editor/editorTheme.css'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'neon\core\assets\JQueryUiAsset',
		'neon\core\form\assets\FormAsset',
		'neon\core\form\assets\CkeditorAsset'
	];
}
