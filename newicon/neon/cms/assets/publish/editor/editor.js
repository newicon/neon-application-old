if (jQuery) {
	/**
	 * Add csrf validation to jQuery requests
	 */
	jQuery.ajaxSetup({
		headers: {
			'X-CSRF-Token': jQuery('meta[name="csrf-token"]').attr('content')
		}
	});
}

if (typeof neon === 'undefined') {
	neon = {};
}
if (typeof neon.cobe === 'undefined') {
	neon.cobe = {};
}

// ----------------
// Daedalus Editors
// ----------------
// neon.cobe.applyDaedalusEditor = function(element) {
// 	$(element).on('click', function(event) {
// 		event.preventDefault();
// 		// event.stopPropagation();
// 		var target= $(event.target);
// 		var key = target.attr('data-key');
// 		var classType = target.attr('data-class-type');
// 		var field = target.attr('data-field');
//
// 		neon.cobe.showForm('/cms/data/daedalus-editor', classType, key, field);
// 	});
// };

// neon.cobe.showForm = function(path, classType, key, field) {
// 	var modalSelector="#neon-modal";
// 	$.ajax({
// 		method: "POST",
// 		url: neon.url(path+'?classType='+classType+'&field='+field+'&key='+key),
// 		success: function (response) {
// 			neon.app.$on('form-'+key+'.mounted', function(formInstance) {
// 				console.log(formInstance);
// 				formInstance.onAfterSubmit(function(response) {
// 					alert('I have just been submitted and my response data is.... ' + JSON.stringify(response));
// 				})
// 			});
// 			$(modalSelector).html(response);
// 		},
// 		error: function(response) {
// 			alert('boooo');
// 		}
// 	});
// };


/**
 * Really basic editor used for inline text with little applied e.g. header
 **/
Vue.component('neon-cobe-editor', {
	props: {
		value: '',
		classType: '',
		field: '',
		uuid: ''
	},
	data() {
		return {test: 'hello im a test'}
	},
	template: `
		<div @click.prevent="onClick()" v-html="myValue"></div>
	`,
	created() {
		if (neon.Store.state.cobe === undefined)
			Vue.set(neon.Store.state, 'cobe', {});
		Vue.set(neon.Store.state.cobe, this.key, this.value);
	},
	computed: {
		key() {
			return this.createKey(this.field);
		},
		myValue: {
			get() {
				return neon.Store.state.cobe[this.key];
			},
			set(value) {
				Vue.set(neon.Store.state.cobe, this.key, value);
			}
		}
	},
	methods: {
		createKey: function(field) {
			return this.classType + this.uuid + field;
		},
		onClick: function() {
			var path = '/cms/data/daedalus-editor';
			var modalSelector="#neon-modal";
			var that = this;
			$.ajax({method: "POST",  url: neon.url(path+'?classType='+this.classType+'&field='+this.field+'&key='+this.uuid),
				success: function (response) {
					neon.app.$on('form-'+that.uuid+'.mounted', function(formInstance) {
						formInstance.onAfterSubmit(function(response) {
							var responseData = JSON.parse(response.response);
							for (var field in responseData) {
								if (responseData.hasOwnProperty(field)) {
									Vue.set(neon.Store.state.cobe, that.createKey(field), responseData[field]);
								}
							}
						});
					});
					$(modalSelector).html(response);
				},
				error: function(response) {
					console.log(response);
					alert('This cannot be edited currently. Sorry');
				}
			});
		}
	}
});


// neon.cobe.applyDaedalusEditor('.cobe-editable.cobe-daed');



// ---------------
// wysiwyg Editors
// ---------------

neon.cobe.applyWysiwygEditor = function(element, path, config) {
	// stop any links from activating
	$(element).on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});
	console.log(element);
	// set up the editor
	$(element).ckeditor(function(element) {
		var editor = this;
		var key = $(element).attr('data-key');
		var pageId = $(element).attr('data-page-id');

		editor.on('blur', function(){
			if (editor.checkDirty()) {
				var data = {
					content: editor.getData(),
					key: key,
					pageId: pageId
				};
				$.ajax({
					method: "POST",
					url: neon.url(path),
					data: data,
					success: function(response) {
						if (response.success == true) {
							editor.setData(response.saved);
							$(element).addClass('m_success');
							setTimeout(function () {
								$(element).removeClass('m_success');
							}, 1000);
						} else {
							$(element).addClass('m_error');
							setTimeout(function () {
								$(element).removeClass('m_error');
								if (typeof response.errors !== 'undefined') {
									alert('Debug mode - error while saving:'+JSON.stringify(response.errors));
								}
							}, 1000);
						}
					},
					error: function(response) {
						$(element).addClass('m_error');
						setTimeout(function () {
							$(element).removeClass('m_error');
						}, 1000);
					}
				});
			}
		});
	}, config);
};
/**
 * Wysiwyg editor for free text areas that can have paragraphs, images etc added
 */
neon.cobe.applyWysiwygEditor('.cobe-editable.cobe-static-block', '/cms/data/update-content-widget', {
	allowedContent: true,
	toolbar:  [
		{ name: 'paragraph', items: ['Format', 'Bold', 'Italic', 'Underline', '-', 'Superscript', 'Subscript', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote'] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor', '-', 'EmojiPanel' ] },
		{ name: 'insert', items: [ 'Table', 'SpecialChar' ] },
		{ name: 'image', items: ['Image', 'Fireflyimage'] },
		{ name: 'document', items: ['Maximize', 'Source' ] },
		{ name: 'source', items: [ 'RemoveFormat', 'Sourcedialog' ] },
	],
	extraPlugins: 'fireflyimage,emoji'
});


/**
 * Really basic editor used for inline text with little applied e.g. header
 **/
// neon.cobe.applyWysiwygEditor('.cobe-editable.cobe-static', '/cms/data/update-widget-field', {
// 	toolbar: [
// 		{ name: 'paragraph', items: [ 'Bold', 'Italic', 'Underline' ] },
// 		{ name: 'source', items: [ 'Source' ] }
// 	]
// });

// (new Vue()).$mount('body');