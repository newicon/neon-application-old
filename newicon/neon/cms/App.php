<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2008 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\cms;

use neon\cms\models\CmsPage;
use \neon\core\BaseApp;
use \neon\cms\components\CmsUrlRule;
use neon\core\helpers\Arr;
use neon\core\helpers\Collection;
use neon\core\interfaces\IDataMapProvider;
use Symfony\Component\VarDumper\Cloner\Data;
use yii\debug\models\timeline\DataProvider;

/**
 * The neon cms app class
 *
 * @property \neon\cms\components\Page $page
 */
class App extends BaseApp
implements IDataMapProvider
{
	public $requires = 'dds';

	/**
	 * @var array of roles that can edit page content via cobe
	 */
	public $editingRoles = ['neon-administrator'];

	/**
	 * @throws \yii\base\InvalidConfigException
	 * @inheritdoc
	 */
	public function configure()
	{
		$this->set('page', [
			'class' => '\neon\cms\components\Page'
		]);
		$this->set('ICmsStaticData', [
			'class' => '\neon\cms\services\cmsManager\CmsManager'
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function setup()
	{
		// Add the url rule to map urls to pages
		neon()->urlManager->rules[] = new CmsUrlRule();
	}

	/**
	 * @inheritdoc
	 */
	public function getDefaultMenu()
	{
		// add menu item (to be deprecated)
		return [
			'label' => $this->getName(),
			'order' => 1100,
			'url' => ['/cms/index/index'],
			'active' => is_route('/cms/*'),
		];
	}

	/**
	 * Get the name for this app
	 * @return string
	 */
	public function getName()
	{
		return 'Cobe CMS';
	}

	/**
	 * @inheritdoc
	 */
	public function getSettings()
	{
		return [
			'google_analytics' => [
				'label' => 'Google Analytics Code',
				'hint' => 'Set the google analytics code if applicable'
			]
		];
	}

	/**
	 * Get the page component
	 * @return \neon\cms\components\Page
	 */
	public function getPage()
	{
		return $this->get('page');
	}

	/**
	 * request an ICmsStaticData interface
	 * @return \neon\cms\services\cmsManager\interfaces\ICmsStaticData
	 */
	public function getICmsStaticData() {
		return $this->get('ICmsStaticData');
	}

	/** Setting and Getting the Theme information **/
	/***********************************************/

	/**
	 * Get the current theme name
	 * Will return the current theme if no theme hierarchy was set
	 * or the top item in the theme hierarchy if set
	 * @return string
	 */
	public function getThemeName()
	{
		if (count($this->_themeHierarchy)) {
			return $this->_themeHierarchy[0];
		}
		return $this->_themeName;
	}

	/**
	 * Set the theme name for site
	 * This can be done via the 'cms' => 'themeName' configuration information
	 * @param string $name  must be alphanumeric and _ - only
	 * @throws \InvalidArgumentException  if the name is not valid
	 */
	public function setThemeName($name)
	{
		$this->checkThemeName($name);
		$this->_themeName = $name;
	}
	private $_themeName = 'default';

	/**
	 * Get the current theme hierarchy
	 * Will return the current theme hierarchy if set or
	 * the [themeName] otherwise
	 * @return array [theme]
	 */
	public function getThemeHierarchy()
	{
		// if we have a theme hierarchy use that
		if (count($this->_themeHierarchy))
			return $this->_themeHierarchy;
		// otherwise drop back to the themeName
		if (!empty($this->_themeName))
			return [$this->_themeName];
		// otherwise return nothing
		return [];
	}

	/**
	 * Set the theme hierarchy for the site.
	 * This is an array of themes in reverse precedence
	 * i.e. first one overrides second etc
	 * @param array $themes
	 */
	public function setThemeHierarchy(array $themes)
	{
		$this->_themeHierarchy = [];
		foreach ($themes as $theme) {
			$this->checkThemeName($theme);
			$this->_themeHierarchy[] = $theme;
		}
	}
	private $_themeHierarchy = [];


	/** Setting and Getting site information **/
	/******************************************/

	/**
	 * @return array  information about the site
	 */
	public function getSiteData()
	{
		return $this->_siteData;
	}

	/**
	 * set site specific information
	 * This can be set via the 'cms' => 'siteData' adding
	 * whatever parameters are required.
	 * @param array $siteData
	 */
	public function setSiteData($siteData)
	{
		$this->_siteData = $siteData;
	}
	private $_siteData = [];

	/**
	 * path alias to the theme directory
	 * @return string
	 */
	public function getThemeAlias()
	{
		return '@root/themes/' . $this->getThemeName();
	}

	/**
	 * Get the basepath to the cms theme directory
	 * @return bool|string
	 */
	public function getThemeBasePath()
	{
		return neon()->getAlias($this->getThemeAlias());
	}

	/**
	 * Publish this modules assets directory
	 * @return array
	 */
	public function assets()
	{
		return neon()->assetManager->publish('@neon/cms/assets');
	}

	/**
	 * @return mixed
	 */
	public function assetsUrl()
	{
		list($path, $url) = $this->assets();
		return $url;
	}

	/**
	 * @return mixed
	 */
	public function assetsPath()
	{
		list($path, $url) = $this->assets();
		return $path;
	}

	/**
	 * @inheritdoc
	 * @unsupported - $filters is not currently supported
	 */
	public function getDataMap($key, $query='',  $filters=[], $fields=[], $start = 0, $limit = 100)
	{
		if ($key === 'pages') {
			return $this->getPagesMap($query, $filters, $fields, $start, $limit);
		}
	}

	/**
	 * @var string[] - string requestKey to array of makeMapLookupRequest arguments
	 */
	private $_mapRequestKey = null;

	/**
	 * @inheritdoc
	 */
	public function getMapResults($requestKey)
	{
		if (!isset($this->_mapRequestKey[$requestKey]))
			throw new \InvalidArgumentException('The provided request key "'.$requestKey.'" does not exist');
		list($key, $ids, $fields) = $this->_mapRequestKey[$requestKey];
		if ($key === 'pages') {
			$pages = CmsPage::find()->select(array_merge(['id', 'nice_id', 'title'], $fields))
				->where(['id' => $ids])
				->orderBy('nice_id')
				->asArray()
				->all();
			return $this->formatPageMapResults($pages, $fields);
		}
		return [];
	}

	/**
	 * Format a list of page rows into a map
	 * @param array $pages - assoc db rows
	 * @return array - maps typically contain an id to a single string value however if additional fields have been
	 *   defined then the map needs to return the id => array of values keyed by field name
	 *   for e.g.
	 * ```php
	 *  // when additional $fields have been requested:
	 *  ['uuid' => [
	 *      'field1' => 'value',
	 *      'field2' => 'value2',
	 *   ]]
	 *
	 *  // when no additional fields defined:
	 *  ['uuid' => 'nice_id: title'],
	 * ```
	 *   Essentially there is a dual responsibility.
	 *   An advanced select box could specify additional map fields additional filters and even display the data in a
	 *   different format. However we want the maps to work well with no additional configuration
	 */
	public function formatPageMapResults($pages, $fields)
	{
		return collect($pages)->mapWithKeys(function ($item, $key) use($fields) {
			if (empty($fields))
				return [$item['id'] => $item['nice_id'] . ': ' . $item['title']];
			return [$item['id'] => $item];
		})->all();
	}

	/**
	 * @inheritdoc
	 */
	public function makeMapLookupRequest($key, $ids, $fields = [])
	{
		$requestKey = md5(serialize(func_get_args()));
		$this->_mapRequestKey[$requestKey] = func_get_args();
		return $requestKey;
	}

	/**
	 * Perform a search to find a map - used for map searches - dropdown displays
	 * where a string search can be used as well as fixed array defined filters.
	 * @see \neon\core\form\fields\SelectDymaic
	 * @see \neon\core\form\fields\SelectDymaic\Link
	 * @see \neon\core\interfaces\IDataMapProvider::getDataMap()
	 * Note this is not used directly to perform the getDataMap but it is used specifically to provide the map for
	 * cms pages (the getDataMap interface supports multiple maps per object)
	 * @param string $query - a string based search
	 * @param array $filters - an array of search criteria key => value
	 * @param array $fields
	 * @return array - @see $this->formatPageMapResults();
	 */
	public function getPagesMap($query=null, $filters=[], $fields=[], $start=0, $limit=100)
	{
		$pages = CmsPage::find()
			->select(array_merge(['id', 'nice_id', 'title'], $fields))
			->where(['or', ['like', 'nice_id', $query], ['like', 'title', $query]])
			->andWhere($filters)
			->offset($start)
			->limit(100)
			->orderBy('nice_id')
			->asArray()
			->all();
		return $this->formatPageMapResults($pages, $fields);
	}

	/**
	 * @inheritdoc
	 */
	public function getDataMapTypes()
	{
		return [
			'pages' => 'Pages'
		];
	}

	/** ********** Private and Protected Methods ********** **/

	/**
	 * check a theme name is valid
	 * - consists of alphanumeric and _- chars only
	 * @param string $name
	 * @throws \InvalidArgumentException
	 */
	private function checkThemeName($name)
	{
		if ( preg_match('/[^A-Za-z0-9_-]/', $name, $matches) ) {
			throw new \InvalidArgumentException("Error trying to set the theme name to '$name'"
				."\n"."The theme name must consist of only 'a-z', '0-9', '_' and '-' characters."
				."\n".'The following character is not allowed in theme names: ' . \yii\helpers\VarDumper::dumpAsString($matches[0]) );
		}
	}

}