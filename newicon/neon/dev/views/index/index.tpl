<h2><a href="{url route="/dev/docs"}">Neon Developer Guide</a></h2>
<h2><a href="{url route="/dev/index/terminal"}">PHP Fiddle Terminal</a></h2>
<h2><a href="{url route="/dev/index/terminal-js"}">JS Fiddle Terminal</a></h2>



{if $debugToolbarEnabled}
	<a class="btn btn-primary" href="{url route="/dev/index/toggle-debug-toolbar"}">Turn off debug toolbar (just for me) </a>
{else}
	<a class="btn btn-default" href="{url route="/dev/index/toggle-debug-toolbar"}">Turn on debug toolbar (just for me) </a>
{/if}