<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2017 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 25/05/2017
 * @package neon
 */

namespace neon\firefly\controllers\api;

use neon\core\web\ApiController;
use neon\firefly\services\fileManager\actions\DownloadAction;
use neon\firefly\services\fileManager\actions\UploadAction;
use neon\firefly\services\fileManager\actions\MetaAction;

class FileController extends ApiController
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = neon()->firefly->fileAccessRules;
		return empty($rules) ? parent::rules() : $rules;
	}

	public function verbs()
	{
		return [
			'meta' => ['GET', 'POST'],
			'upload' => ['POST'],
			'download' => ['GET', 'POST']
		];
	}

	public function actions()
	{
		return [
			'upload' => UploadAction::class,
			'meta' => MetaAction::class,
			'download' => DownloadAction::class
		];
	}
}