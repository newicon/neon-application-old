"use strict";

/**
 * Created by newicon on 23/05/2017.
 */
Vue.component('firefly-item-sidebar', {
  props: {
    item: Object,
    name: {
      type: String,
      required: true
    }
  },
  template: "\n\t\t<div v-if=\"item\">\n\t\t\n\t\t\t<div v-for=\"upload in itemsUploading\">\n\t\t\t<pre>{{upload}}</pre>\n\t\t\t</div>\n\t\t\n\t\t\t<div v-for=\"item in selectedItems\" style=\"display:inline-block;\" >\n\t\t\t\t<img :src=\"urlPreview(item)\" style=\"max-width:40px;max-height:40px;\">\n\t\t\t</div>\n\t\t\t<dl>\n\t\t\t\t<template v-if=\"item.type == 'dir'\">\n\t\t\t\t\t<dt>Name</dt><dd>{{itemName}}</dd>\n\t\t\t\t\t<dt>Name</dt><dd>Folder</dd>\n\t\t\t\t</template>\n\t\t\t\t<template v-if=\"item.file\">\n\t\t\t\t\t<dt>Name</dt><dd>{{itemName}}</dd>\n\t\t\t\t\t<dt>Type</dt>        <dd>{{item.file.mime_type}}</dd>\n\t\t\t\t\t<dt>Uploaded On</dt> <dd>{{item.file.created_at}}</dd>\n\t\t\t\t\t<dt>Size</dt>        <dd>{{item.file.size}}</dd>\n\t\t\t\t\t<dt>File Path</dt>   <dd><div style=\"overflow:scroll\">{{item.file.drive}}/{{item.file.path}}</div></dd>\n\t\t\t\t</template>\n\t\t\t</dl>\n\t\t\t<button class=\"btn btn-danger\" :disabled=\"!itemExists\" @click=\"deleteItem\">Delete</button>\n\t\t</div>\n\t",
  methods: {
    urlPreview: function urlPreview(item) {
      return neon.base() + '/firefly/file/img?id=' + item.id;
    },
    deleteItem: function deleteItem() {
      this.$store.dispatch('Firefly/DELETE_ITEM', {
        id: this.item.id,
        name: this.name
      });
    }
  },
  computed: {
    itemsUploading: function itemsUploading() {
      return this.$store.getters['Firefly/getItemsUploading'](this.name);
    },
    selectedItems: function selectedItems() {
      return this.$store.getters['Firefly/getSelectedItems'](this.name);
    },
    itemName: function itemName() {
      return this.$store.getters['Firefly/getItemName'](this.item);
    },
    itemExists: function itemExists() {
      return this.$store.getters['Firefly/getItemExists'](this.name, this.item.id);
    }
  }
});