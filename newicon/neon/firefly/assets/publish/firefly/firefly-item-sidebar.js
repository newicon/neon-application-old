/**
 * Created by newicon on 23/05/2017.
 */
Vue.component('firefly-item-sidebar', {
	props: {
		item: Object,
		name: {type: String, required: true}
	},
	template: `
		<div v-if="item">
		
			<div v-for="upload in itemsUploading">
			<pre>{{upload}}</pre>
			</div>
		
			<div v-for="item in selectedItems" style="display:inline-block;" >
				<img :src="urlPreview(item)" style="max-width:40px;max-height:40px;">
			</div>
			<dl>
				<template v-if="item.type == 'dir'">
					<dt>Name</dt><dd>{{itemName}}</dd>
					<dt>Name</dt><dd>Folder</dd>
				</template>
				<template v-if="item.file">
					<dt>Name</dt><dd>{{itemName}}</dd>
					<dt>Type</dt>        <dd>{{item.file.mime_type}}</dd>
					<dt>Uploaded On</dt> <dd>{{item.file.created_at}}</dd>
					<dt>Size</dt>        <dd>{{item.file.size}}</dd>
					<dt>File Path</dt>   <dd><div style="overflow:scroll">{{item.file.drive}}/{{item.file.path}}</div></dd>
				</template>
			</dl>
			<button class="btn btn-danger" :disabled="!itemExists" @click="deleteItem">Delete</button>
		</div>
	`,
	methods: {
		urlPreview: function(item) { return neon.base() + '/firefly/file/img?id=' + item.id; },
		deleteItem: function() { this.$store.dispatch('Firefly/DELETE_ITEM', {id: this.item.id, name: this.name}); }
	},
	computed: {
		itemsUploading: function () { return this.$store.getters['Firefly/getItemsUploading'](this.name); },
		selectedItems: function() { return this.$store.getters['Firefly/getSelectedItems'](this.name); },
		itemName:   function() { return this.$store.getters['Firefly/getItemName'](this.item); },
		itemExists: function() { return this.$store.getters['Firefly/getItemExists'](this.name, this.item.id); }
	}
});