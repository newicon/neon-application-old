"use strict";

/**
 * ------------------------
 * <firefly-media-browser>
 * ------------------------
 * @param {String} name
 */
Vue.component('firefly-media-browser', {
  props: {
    /**
     * The name of the media picker - all browsers store their state against their picker key
     * state.pickers[name]
     * neon.Store.state.Firefly.pickers[name]
     * @type {String}
     */
    name: {
      type: String,
      required: true
    }
  },
  data: function data() {
    return {
      showDeleted: false
    };
  },
  template: "\n\t\t<div class=\"fireflyMediaContainer\">\n\t\t\t<!-- File List -->\n\t\t\t<div class=\"ffFilesList\" @click=\"unSelect\">\n\t\t\t\t<!--<firefly-item-parent :name=\"name\" @click.stop=\"selectItem(item, $event)\" @dblclick=\"parentDoubleClick(item)\"></firefly-item-parent>-->\n\t\t\t\t<!--<transition-group name=\"modal\" mode=\"in-out\">-->\n\t\t\t\t\t<firefly-item\n\t\t\t\t\t\tv-for=\"(item, key) in items\"\n\t\t\t\t\t\tv-if=\"item.deleted != 1\"\n\t\t\t\t\t\t:name=\"name\"\n\t\t\t\t\t\t:key=\"item.id\"\n\t\t\t\t\t\t:item=\"item\"\n\t\t\t\t\t\t:selected=\"isSelected(item.id)\"\n\t\t\t\t\t\t@click.stop=\"selectItem(item, $event)\"\n\t\t\t\t\t\t@dblclick=\"doubleClick(item)\"\n\t\t\t\t\t\t@dragStart=\"dragStart(item)\"\n\t\t\t\t\t\t>\n\t\t\t\t\t</firefly-item>\n\t\t\t\t<!--</transition-group>-->\n\t\t\t</div>\n\t\t\t<!-- drop upload -->\n\t\t\t<firefly-media-uploader :path=\"path\" :name=\"name\" :hide=\"!itemsLoading && items.length == 0\"></firefly-media-uploader>\n\t\t\t<!--<div class=\"fireflySidebar\">-->\n\t\t\t\t<!--<firefly-item-sidebar v-if=\"selectedItem\" :item=\"selectedItem\" :name=\"name\"></firefly-item-sidebar>-->\n\t\t\t<!--</div>-->\n\t\t</div>\n\t",
  computed: {
    selectedItem: function selectedItem() {
      return this.$store.getters['Firefly/getSelectedItem'](this.name);
    },
    path: function path() {
      return this.$store.getters['Firefly/getPath'](this.name);
    },
    startPath: function startPath() {
      return this.$store.getters['Firefly/getStartPath'](this.name);
    },
    items: function items() {
      return this.$store.getters['Firefly/getItems'](this.name);
    },
    browserItems: function browserItems() {
      return this.$store.getters['Firefly/getItemsOrderedByName'](this.name);
    },
    hasEditor: function hasEditor() {
      return this.$store.getters['Firefly/getItemHasEditor'](this.selectedItem);
    },
    itemsLoading: function itemsLoading() {
      return this.$store.getters['Firefly/getItemsLoading'](this.name);
    }
  },
  methods: {
    isEditingItemName: function isEditingItemName(item) {
      return this.$store.getters['Firefly/getIsEditingItemName'](item);
    },
    isSelected: function isSelected(id) {
      return this.$store.getters['Firefly/getIsSelected'](this.name, id);
    },
    selectPrevious: function selectPrevious(shift) {
      this.$store.commit('Firefly/SELECT_PREVIOUS_ITEM', {
        name: this.name,
        shift: shift
      });
    },
    selectNext: function selectNext(shift) {
      this.$store.commit('Firefly/SELECT_NEXT_ITEM', {
        name: this.name,
        shift: shift
      });
    },
    selectItem: function selectItem(item, event) {
      if (event.shiftKey) {
        this.$store.commit('Firefly/SELECT_ALL_BETWEEN', {
          item: item,
          name: this.name
        });
      } else {
        this.$store.commit('Firefly/SELECT_ITEM', {
          item: item,
          name: this.name,
          multiple: event.metaKey
        });
      }

      this.$emit('onSelect', item);
    },
    unSelect: function unSelect() {
      this.$store.commit('Firefly/UNSELECT', {
        name: this.name
      });
    },
    loadPath: function loadPath(path) {
      this.$store.dispatch('Firefly/LOAD_ITEMS', {
        path: path,
        name: this.name
      });
    },
    doubleClick: function doubleClick(item) {
      if (item.type == 'dir') {
        // Don't redirect on double click if we are editing the items name
        if (!this.isEditingItemName(item)) this.loadPath(item.path);
      } else {
        this.$emit('onDoubleClickItem', item);
      }
    },
    dragStart: function dragStart(item) {
      // if item is not selected then select it
      if (!this.isSelected(item.id)) {
        this.$store.commit('Firefly/SELECT_ITEM', {
          item: item,
          name: this.name
        });
      }
    },
    selectAll: function selectAll() {
      this.$store.commit('Firefly/SELECT_ALL', {
        path: this.path,
        name: this.name
      });
    },
    testy: function testy() {
      alert('testy');
    },
    onKeyup: function onKeyup(event) {
      if (event.which === 37) this.selectPrevious(event.shiftKey);
      if (event.which === 39) this.selectNext(event.shiftKey);
    }
  },
  beforeDestroy: function beforeDestroy() {
    window.removeEventListener('keyup', this.onKeyup);
  },
  mounted: function mounted() {
    this.loadPath(this.startPath);
    window.addEventListener('keyup', this.onKeyup); // $(window).on('keydown', (event) => {
    // 	// Select all cmd+a
    // 	if (event.code === 65 && event.metaKey) {
    // 		event.preventDefault();
    // 		this.selectAll();
    // 	}
    // });
  }
});