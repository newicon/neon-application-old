"use strict";

/**
 * ------------------------
 * <firefly-media-inline>
 * ------------------------
 *
 */
Vue.component('firefly-media-inline', {
  props: {
    name: {
      type: String,
      required: true
    },
    startPath: {
      type: String,
      default: '/'
    }
  },
  template: "\n\t\t<div class=\"fireflyFinder\">\n\t\t\t<template v-if=\"pickerExists\">\n\t\t\t\t<!-- toolbar -->\n\t\t\t\t<firefly-media-browser-toolbar :name=\"name\"></firefly-media-browser-toolbar>\n\t\t\t\t<firefly-media-browser-breadcrumb :name=\"name\"></firefly-media-browser-breadcrumb>\n\t\t\t\t<firefly-media-browser :name=\"name\" ></firefly-media-browser>\n\t\t\t</template>\n\t\t</div>\n\t",
  mounted: function mounted() {
    this.$store.commit('Firefly/' + 'CREATE_PICKER', {
      name: this.name,
      startPath: this.startPath
    });
  },
  computed: {
    pickerExists: function pickerExists() {
      return this.$store.getters['Firefly/getPickerExists'](this.name);
    }
  }
});