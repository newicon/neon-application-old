"use strict";

/**
 * ------------------------
 * <firefly-media-browser>
 * ------------------------
 *
 */
Vue.component('firefly-media-browser-toolbar', {
  props: {
    /**
     * The name of the media picker - all browsers store their state against their picker key
     * state.pickers[name]
     * neon.Store.state.Firefly.pickers[name]
     * @type {String}
     */
    name: {
      type: String,
      required: true
    }
  },
  template: "\n\t\t<div class=\"workbenchHeader\">\n\t\t\t<button class=\"btn btn-default\" @click.prevent=\"addFolder\"><i class=\"fa fa-folder-o\"></i> Add Folder</button>\n\t\t\t<firefly-media-upload-button class=\"mls\" :name=\"name\" :path=\"path\">Upload...</firefly-media-upload-button>\n\t\t\t<button :disabled=\"noEditorExists\" @click=\"edit\" class=\"btn btn-default mls\" ><i class=\"fa fa-pencil\"></i> Edit...</button>\n\t\t\t<button :disabled=\"!selectedItems.length\" @click=\"deleteSelected\"  class=\"btn btn-danger mls\"><i class=\"fa fa-trash-o\"></i> Delete</button>\n\t\t\t<div :disabled=\"!selectedItems.length\" @click=\"download()\" class=\"btn btn-default mls\"><i class=\"fa fa-cloud-download\"></i> Download</div>\n\n\t\t\t<div class=\"mls\">{{dropText}}</div>\n\t\t\t<div v-show=\"uploaderStatus\" class=\"progress progressThin\" style=\"width:100px\"><div class=\"progress-bar\" :style=\"{width: uploadPercent+'%'}\"></div></div>\n\t\t\t<!-- uploading status -->\n\t\t\t<div v-show=\"uploaderStatus\" class=\"mls\">{{uploaderStatus}}</div>\n\n\t\t</div>\n\t",
  methods: {
    addFolder: function addFolder() {
      this.$store.dispatch('Firefly/CREATE_DIRECTORY', {
        name: this.name
      });
    },
    loadPath: function loadPath(path) {
      this.$store.dispatch('Firefly/LOAD_ITEMS', {
        path: path,
        name: this.name
      });
    },
    edit: function edit() {
      var _this = this;

      FIREFLY.edit(this.selectedItem, {}, function (editedItem) {
        _this.$store.dispatch('Firefly/LOAD_ITEMS', {
          name: _this.name,
          path: editedItem.path
        }); // select the new edited item


        _this.$store.commit('Firefly/SELECT_ITEM', {
          name: _this.name,
          item: editedItem
        });
      });
    },
    deleteSelected: function deleteSelected() {
      this.$store.dispatch('Firefly/DELETE_SELECTED', {
        name: this.name
      });
    },
    download: function download() {
      if (_.isDefined(this.selectedItem.id)) {
        window.location = neon.url('firefly/api/file/download?id=' + this.selectedItem.id);
      }
    }
  },
  computed: {
    uploadPercent: function uploadPercent() {
      return this.$store.getters['Firefly/getTotalUploadPercent'](this.name);
    },
    uploaderStatus: function uploaderStatus() {
      return this.$store.getters['Firefly/getUploadingFilesStatus'](this.name);
    },
    dropText: function dropText() {
      return this.$store.getters['Firefly/getDropMoveText'](this.name);
    },
    path: function path() {
      return this.$store.getters['Firefly/getPath'](this.name);
    },
    pathBits: function pathBits() {
      return this.$store.getters['Firefly/getPathBits'](this.name);
    },
    selectedItems: function selectedItems() {
      return this.$store.getters['Firefly/getSelectedItems'](this.name);
    },
    selectedItem: function selectedItem() {
      return this.$store.getters['Firefly/getSelectedItem'](this.name);
    },
    noEditorExists: function noEditorExists() {
      return !this.hasEditor;
    },
    hasEditor: function hasEditor() {
      return this.$store.getters['Firefly/getHasEditor'](this.selectedItem);
    }
  }
});