/**
 * Created by newicon on 09/05/2017.
 */
/**
 * ------------------------
 * <firefly-item>
 * ------------------------
 *
 * Firefly item represents an individual item stored - this could be either a directory or a file
 */
Vue.component('firefly-item', {
	props: {
		name: {type: String}, // this picker name
		item: {type: Object},
		selected: {type: Boolean}
	},
	template: `
		<div class="fireflyItem" :class="{'is-selected':selected, dragOver:dragOver}" @click="$emit('click', $event)" @dblclick="$emit('dblclick')">
			<div :class="itemClassName">
	
				<!-- Directory -->
				<div v-if="item.type == 'dir'">
					<div class="fireflyThumb_folder fireflyThumb_preview"></div>
					<firefly-item-name :item="item" :selected="selected"></firefly-item-name>
				</div>
				
				<!-- File -->
				<div v-if="item.type == 'file'" >
					<div v-if="isImage" class="fireflyThumb_preview fireflyThumb_image" >
						<firefly-img ondragstart="return false;" :item="item" ></firefly-img>
					</div>
					<div v-else-if="isVideo" class="fireflyThumb_preview fireflyThumb_image" >
						<video controls style="width:100%; max-height:150px;" ondragstart="return false;" :src="urlBase + '/firefly/file/get?id=' + item.id + '&w=250&h=250'" ></video>
					</div>
					<div v-else class="fireflyThumb_preview fireflyThumb_file">
						<div class="fireflyThumb_previewText">{{fileType}}</div>
					</div>
					<firefly-item-name :item="item" :selected="selected"></firefly-item-name>
				</div>
	
				<!-- Uploading file -->
				<div v-if="item.type == 'upload'" :style="uploadStyles">
					
					<div class="fireflyThumb_preview fireflyThumb_progress">
						<div class="fireflyThumb_previewText">{{item.upload_percent}}%</div>
						<div class="progress progressThin"><div class="progress-bar" :style="{width: item.upload_percent+'%'}"></div></div>
					</div>
					<firefly-item-name :can-edit="false" :item="item" :selected="selected"></firefly-item-name>
				</div>
			</div>
		</div>
	`,
	computed: {
		dragOver: function() { return this.$store.getters['Firefly/getIsItemBeingDroppedOver'](this.name, this.item.id); },
		selectedItems: function() { return this.$store.getters['Firefly/getSelectedItems'](this.name); },
		uploadStyles: function() {
			var opacity = this.item.upload_percent / 100;
			if (opacity < 0.4) { opacity = 0.4; }
			return {opacity : opacity}
		},
		urlBase: function() { return neon.base(); },
		itemClassName: function () {
			return 'fireflyThumb ' + 'fireflyThumb' + _.capitalize(this.item.type);
		},
		isVideo: function() {
			if (this.item.file) {
				var bits = this.item.file.mime_type.split('/');
				return bits[0] === 'video'
			}
			return false;
		},
		fileType: function() {
			if (this.item.file) {
				var bits = this.item.file.mime_type.split('/');
				return bits[1];
			}
			return '';
		},
		isImage: function() {
			if (!this.item.file) return false;
			var mime = this.item.file.mime_type || '';
			return (mime === 'image/png' || mime === 'image/jpeg' || mime === 'image/gif' || mime === 'image/svg+xml');
		}

	},
	mounted: function() {
		var vm = this;
		if (this.item.type == 'dir') {
			$(vm.$el).droppable({
				accept: ".fireflyItem",
				over: function(event, ui) { vm.$store.commit('Firefly/DROP_OVER', {id: vm.item.id, name: vm.name}); },
				out: function(event, ui) { vm.$store.commit('Firefly/DROP_OUT', {name: vm.name}); },
				drop: function(event, ui) {
					vm.$store.dispatch('Firefly/ITEMS_DROPPED', {into: vm.item.id, items: vm.selectedItems, name: vm.name});
					vm.$store.commit('Firefly/DROP_OUT', {name: vm.name});
				}

			})
		}
		if (this.item.type == 'file') {

			$(vm.$el).draggable({
				helper: function() {
					return $("<div><span class=\"numberDragging label label-danger\"></span></div>")
						.append($(vm.$el).clone().removeClass('is-selected'))},
				opacity:0.7,
				start: function(event, ui) {
					// register this item as being dragged
					vm.$emit('dragStart');
					var numberDragging = vm.selectedItems.length;
					ui.helper.find('.numberDragging').text((numberDragging > 1) ? numberDragging : '');
				}
			})
		}
	}
});