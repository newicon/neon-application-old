"use strict";

/**
 * Created by newicon on 09/05/2017.
 */

/**
 * ------------------------
 * <firefly-img>
 * ------------------------
 */
Vue.component('firefly-img', {
  props: {
    loadingSrc: {
      type: String
    },
    item: {
      type: Object
    }
  },
  data: function data() {
    return {
      loaded: false
    };
  },
  template: "<img :src=\"src\" class=\"fireflyThumb_img\" :class=\"{'neonLoadingImg': !loaded}\" />",
  computed: {
    src: function src() {
      return neon.base() + '/firefly/file/img?id=' + this.item.id + '&w=250&h=250';
    }
  },
  mounted: function mounted() {
    var vm = this;
    vm.loaded = false;

    vm.$el.onload = function () {
      vm.loaded = true;
    };
  }
});