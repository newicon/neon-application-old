"use strict";

// The image upload component could end up being quite complex.
// With a file browser and media manager.
// A cropper and image manipulator...
// So it makes sense we deffer to javascript for the heavy lifting
Vue.component('firefly-form-file-browser', {
  props: {
    name: {
      type: String,
      required: true
    },
    value: String,
    // the uuid of the file
    startPath: {
      type: String,
      default: '/'
    },
    readOnly: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      selectedId: ''
    };
  },
  template: "\n\t\t<div>\n\t\t\t<input @input=\"$emit('input', $event.target.value)\" type=\"hidden\" :name=\"name\" :value=\"selectedId\" :readonly=\"readOnly\">\n\t\t\t<div v-if=\"!readOnly\">\n\t\t\t\t<a class=\"btn btn-default\" @click.prevent=\"launchPicker\"><i class=\"fa fa-file-o\"></i>\n\t\t\t\t\t<span v-if=\"!selectedId\">Pick File...</span>\n\t\t\t\t\t<span v-if=\"selectedId\">Change File...</i></span>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<firefly-form-file-list style=\"margin: 0;padding: 0;list-style: none; margin-bottom:5px;\"\n\t\t\t\t:files=\"files\"\n\t\t\t\t@onClearClick=\"onClear\">\n\t\t\t</firefly-form-file-list>\n\t\t</div>\n\t",
  mounted: function mounted() {
    this.selectedId = this.value;
    if (this.selectedId) this.$store.dispatch('Firefly/FILE_INFO', {
      id: this.selectedId
    });
  },
  methods: {
    launchPicker: function launchPicker() {
      var _this = this;

      FIREFLY.picker(this.name, function (item) {
        _this.selectedId = item.id;

        _this.$store.commit('Firefly/FILE_INFO', {
          id: _this.selectedId,
          info: item.file
        });

        _this.$emit('input', _this.selectedId);
      }, {
        startPath: this.startPath
      });
    },
    onClear: function onClear() {
      this.selectedId = '';
    }
  },
  computed: {
    files: function files() {
      if (this.selectedId === '' || _.isEmpty(this.fileInfo)) {
        return [];
      }

      return [this.fileInfo];
    },
    fileInfo: function fileInfo() {
      if (this.$store.state.Firefly.files[this.selectedId]) return this.$store.state.Firefly.files[this.selectedId];
      return {};
    }
  }
});