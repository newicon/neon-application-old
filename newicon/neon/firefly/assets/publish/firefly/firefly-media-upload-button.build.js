"use strict";

/**
 * ------------------------
 * <firefly-media-browser>
 * ------------------------
 *
 */
Vue.component('firefly-media-upload-button', {
  props: {
    name: {
      type: String,
      required: true
    },
    path: {
      type: String,
      default: '/'
    }
  },
  data: function data() {
    var id = 'uploader_' + _.uniqueId();

    return {
      container: id + '_container',
      drop: id + '_drop'
    };
  },
  template: "\n\t\t<div :id=\"drop\" class=\"btn btn-default\">\n\t\t\t<div>\n\t\t\t\t<i class=\"fa fa-cloud-upload\" aria-hidden=\"true\"></i>\n\t\t\t\t<slot></slot>\n\t\t\t</div>\n\t\t</div>\n\t",
  mounted: function mounted() {
    FIREFLY.createPlupload(this.name, {
      drop_element: this.drop,
      browse_button: this.drop
    });
  }
});