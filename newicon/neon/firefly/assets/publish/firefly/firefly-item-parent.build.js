"use strict";

/**
 * Created by newicon on 09/05/2017.
 */

/**
 * ------------------------
 * <firefly-item>
 * ------------------------
 *
 * Firefly item represents an individual item stored - this could be either a directory or a file
 */
Vue.component('firefly-item-parent', {
  props: {
    /**
     * The picker name
     */
    name: {
      type: String
    },
    selected: {
      type: Boolean
    }
  },
  template: "\n\t\t<div v-show=\"path != '/'\" class=\"fireflyItem\" :class=\"{'is-selected':selected, dragOver:dragOver}\"  @dblclick=\"doubleClick\">\n\t\t\t<div class=\"fireflyThumb fireflyThumbDir\">\n\t\n\t\t\t\t<!-- Directory -->\n\t\t\t\t<div>\n\t\t\t\t\t<div class=\"fireflyThumb_folder fireflyThumb_preview\"></div>\n\t\t\t\t\t<h5 class=\"fireflyThumb_caption\" >..</h5>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t</div>\n\t\t</div>\n\t",
  computed: {
    dragOver: function dragOver() {
      return this.$store.getters['Firefly/getIsItemBeingDroppedOver'](this.name, this.parentPath);
    },
    path: function path() {
      return this.$store.getters['Firefly/getPath'](this.name);
    },
    parent: function parent() {
      return this.$store.getters['Firefly/getParent'](this.name);
    },
    selectedItems: function selectedItems() {
      return this.$store.getters['Firefly/getSelectedItems'](this.name);
    },
    parentPath: function parentPath() {
      return this.parent ? this.parent.path : '/';
    }
  },
  methods: {
    doubleClick: function doubleClick() {
      this.$store.dispatch('Firefly/LOAD_ITEMS', {
        path: this.parentPath,
        name: this.name
      });
    }
  },
  mounted: function mounted() {
    var vm = this;
    $(vm.$el).droppable({
      accept: ".fireflyItem",
      tolerance: "pointer",
      over: function over(event, ui) {
        vm.$store.commit('Firefly/DROP_OVER', {
          id: vm.parentPath,
          name: vm.name
        });
      },
      out: function out(event, ui) {
        vm.$store.commit('Firefly/DROP_OUT', {
          name: vm.name
        });
      },
      drop: function drop(event, ui) {
        vm.$store.dispatch('Firefly/ITEMS_DROPPED', {
          into: vm.parentPath,
          items: vm.selectedItems,
          name: vm.name
        });
        vm.$store.commit('Firefly/DROP_OUT', {
          name: vm.name
        });
      }
    });
  }
});