"use strict";

/**
 * This component displays a list of files - their upload progress and actions to remove from the list
 */
Vue.component('firefly-form-file-list', {
  props: {
    files: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    urlDownload: {
      type: String,
      default: '/firefly/api/file/download'
    },
    printOnly: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      UPLOADING: 2,
      timeout: {}
    };
  },
  template: "\n\t\t<transition tag=\"ul\" name=\"fade\">\n\t\t\t<ul v-if=\"files.length\" style=\"margin: 0;padding: 0;list-style: none; margin-bottom: 5px;\">\n\t\t\t\t<li v-for=\"file in files\" :key=\"file.uuid\" tabindex=\"0\" class=\"el-upload-list__item is-success\">\n\t\t\t\t\t<div v-if='printOnly'>\n\t\t\t\t\t\t<a class=\"el-upload-list__item-name\" :href=\"fileUrl(file)\" :target=\"file.name\">\n\t\t\t\t\t\t\t<i class=\"el-icon-document\"></i> {{file.name}}\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div v-else>    \n\t\t\t\t\t\t<a class=\"el-upload-list__item-name\" :href=\"fileUrl(file)\" :target=\"file.name\">\n\t\t\t\t\t\t\t<i class=\"el-icon-document\"></i>{{file.name}}\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<i class=\"el-icon-close\" style=\"width:20px;\" @click=\"$emit('onClearClick', file.id)\"></i>\n\t\t\t\t\t\t<el-progress v-if=\"file.status == UPLOADING\" type=\"line\" :stroke-width=\"2\" :percentage=\"file.percent\"></el-progress>\n\t\t\t\t\t</div>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</transition>\n\t",
  methods: {
    afterUploadSuccessEnter: function afterUploadSuccessEnter(fileId) {
      var _this = this;

      setTimeout(function () {
        Vue.set(_this.timeout, fileId, true);
      }, 500);
    },
    fileUrl: function fileUrl(file) {
      if (file.uuid) return neon.url(this.urlDownload, {
        id: file.uuid
      });
      return false;
    }
  }
});