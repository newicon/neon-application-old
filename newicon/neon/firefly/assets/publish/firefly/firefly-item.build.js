"use strict";

/**
 * Created by newicon on 09/05/2017.
 */

/**
 * ------------------------
 * <firefly-item>
 * ------------------------
 *
 * Firefly item represents an individual item stored - this could be either a directory or a file
 */
Vue.component('firefly-item', {
  props: {
    name: {
      type: String
    },
    // this picker name
    item: {
      type: Object
    },
    selected: {
      type: Boolean
    }
  },
  template: "\n\t\t<div class=\"fireflyItem\" :class=\"{'is-selected':selected, dragOver:dragOver}\" @click=\"$emit('click', $event)\" @dblclick=\"$emit('dblclick')\">\n\t\t\t<div :class=\"itemClassName\">\n\t\n\t\t\t\t<!-- Directory -->\n\t\t\t\t<div v-if=\"item.type == 'dir'\">\n\t\t\t\t\t<div class=\"fireflyThumb_folder fireflyThumb_preview\"></div>\n\t\t\t\t\t<firefly-item-name :item=\"item\" :selected=\"selected\"></firefly-item-name>\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t<!-- File -->\n\t\t\t\t<div v-if=\"item.type == 'file'\" >\n\t\t\t\t\t<div v-if=\"isImage\" class=\"fireflyThumb_preview fireflyThumb_image\" >\n\t\t\t\t\t\t<firefly-img ondragstart=\"return false;\" :item=\"item\" ></firefly-img>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div v-else-if=\"isVideo\" class=\"fireflyThumb_preview fireflyThumb_image\" >\n\t\t\t\t\t\t<video controls style=\"width:100%; max-height:150px;\" ondragstart=\"return false;\" :src=\"urlBase + '/firefly/file/get?id=' + item.id + '&w=250&h=250'\" ></video>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div v-else class=\"fireflyThumb_preview fireflyThumb_file\">\n\t\t\t\t\t\t<div class=\"fireflyThumb_previewText\">{{fileType}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<firefly-item-name :item=\"item\" :selected=\"selected\"></firefly-item-name>\n\t\t\t\t</div>\n\t\n\t\t\t\t<!-- Uploading file -->\n\t\t\t\t<div v-if=\"item.type == 'upload'\" :style=\"uploadStyles\">\n\t\t\t\t\t\n\t\t\t\t\t<div class=\"fireflyThumb_preview fireflyThumb_progress\">\n\t\t\t\t\t\t<div class=\"fireflyThumb_previewText\">{{item.upload_percent}}%</div>\n\t\t\t\t\t\t<div class=\"progress progressThin\"><div class=\"progress-bar\" :style=\"{width: item.upload_percent+'%'}\"></div></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<firefly-item-name :can-edit=\"false\" :item=\"item\" :selected=\"selected\"></firefly-item-name>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t",
  computed: {
    dragOver: function dragOver() {
      return this.$store.getters['Firefly/getIsItemBeingDroppedOver'](this.name, this.item.id);
    },
    selectedItems: function selectedItems() {
      return this.$store.getters['Firefly/getSelectedItems'](this.name);
    },
    uploadStyles: function uploadStyles() {
      var opacity = this.item.upload_percent / 100;

      if (opacity < 0.4) {
        opacity = 0.4;
      }

      return {
        opacity: opacity
      };
    },
    urlBase: function urlBase() {
      return neon.base();
    },
    itemClassName: function itemClassName() {
      return 'fireflyThumb ' + 'fireflyThumb' + _.capitalize(this.item.type);
    },
    isVideo: function isVideo() {
      if (this.item.file) {
        var bits = this.item.file.mime_type.split('/');
        return bits[0] === 'video';
      }

      return false;
    },
    fileType: function fileType() {
      if (this.item.file) {
        var bits = this.item.file.mime_type.split('/');
        return bits[1];
      }

      return '';
    },
    isImage: function isImage() {
      if (!this.item.file) return false;
      var mime = this.item.file.mime_type || '';
      return mime === 'image/png' || mime === 'image/jpeg' || mime === 'image/gif' || mime === 'image/svg+xml';
    }
  },
  mounted: function mounted() {
    var vm = this;

    if (this.item.type == 'dir') {
      $(vm.$el).droppable({
        accept: ".fireflyItem",
        over: function over(event, ui) {
          vm.$store.commit('Firefly/DROP_OVER', {
            id: vm.item.id,
            name: vm.name
          });
        },
        out: function out(event, ui) {
          vm.$store.commit('Firefly/DROP_OUT', {
            name: vm.name
          });
        },
        drop: function drop(event, ui) {
          vm.$store.dispatch('Firefly/ITEMS_DROPPED', {
            into: vm.item.id,
            items: vm.selectedItems,
            name: vm.name
          });
          vm.$store.commit('Firefly/DROP_OUT', {
            name: vm.name
          });
        }
      });
    }

    if (this.item.type == 'file') {
      $(vm.$el).draggable({
        helper: function helper() {
          return $("<div><span class=\"numberDragging label label-danger\"></span></div>").append($(vm.$el).clone().removeClass('is-selected'));
        },
        opacity: 0.7,
        start: function start(event, ui) {
          // register this item as being dragged
          vm.$emit('dragStart');
          var numberDragging = vm.selectedItems.length;
          ui.helper.find('.numberDragging').text(numberDragging > 1 ? numberDragging : '');
        }
      });
    }
  }
});