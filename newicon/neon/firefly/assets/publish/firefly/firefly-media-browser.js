/**
 * ------------------------
 * <firefly-media-browser>
 * ------------------------
 * @param {String} name
 */
Vue.component('firefly-media-browser', {
	props: {
		/**
		 * The name of the media picker - all browsers store their state against their picker key
		 * state.pickers[name]
		 * neon.Store.state.Firefly.pickers[name]
		 * @type {String}
		 */
		name: {type: String, required:true},
	},
	data:function(){return {showDeleted:false}},
	template: `
		<div class="fireflyMediaContainer">
			<!-- File List -->
			<div class="ffFilesList" @click="unSelect">
				<!--<firefly-item-parent :name="name" @click.stop="selectItem(item, $event)" @dblclick="parentDoubleClick(item)"></firefly-item-parent>-->
				<!--<transition-group name="modal" mode="in-out">-->
					<firefly-item
						v-for="(item, key) in items"
						v-if="item.deleted != 1"
						:name="name"
						:key="item.id"
						:item="item"
						:selected="isSelected(item.id)"
						@click.stop="selectItem(item, $event)"
						@dblclick="doubleClick(item)"
						@dragStart="dragStart(item)"
						>
					</firefly-item>
				<!--</transition-group>-->
			</div>
			<!-- drop upload -->
			<firefly-media-uploader :path="path" :name="name" :hide="!itemsLoading && items.length == 0"></firefly-media-uploader>
			<!--<div class="fireflySidebar">-->
				<!--<firefly-item-sidebar v-if="selectedItem" :item="selectedItem" :name="name"></firefly-item-sidebar>-->
			<!--</div>-->
		</div>
	`,
	computed: {
		selectedItem  () { return this.$store.getters['Firefly/getSelectedItem'](this.name); },
		path          () { return this.$store.getters['Firefly/getPath'](this.name); },
		startPath     () { return this.$store.getters['Firefly/getStartPath'](this.name); },
		items         () { return this.$store.getters['Firefly/getItems'](this.name); },
		browserItems  () { return this.$store.getters['Firefly/getItemsOrderedByName'](this.name); },
		hasEditor     () { return this.$store.getters['Firefly/getItemHasEditor'](this.selectedItem); },
		itemsLoading  () { return this.$store.getters['Firefly/getItemsLoading'](this.name); },
	},
	methods: {
		isEditingItemName (item) { return this.$store.getters['Firefly/getIsEditingItemName'](item); },
		isSelected     (id) { return this.$store.getters['Firefly/getIsSelected'](this.name, id); },
		selectPrevious (shift) { this.$store.commit('Firefly/SELECT_PREVIOUS_ITEM', {name:this.name, shift:shift}); },
		selectNext     (shift) { this.$store.commit('Firefly/SELECT_NEXT_ITEM', {name:this.name, shift:shift}); },
		selectItem (item, event) {
			if (event.shiftKey) {
				this.$store.commit('Firefly/SELECT_ALL_BETWEEN', {item: item, name: this.name});
			} else {
				this.$store.commit('Firefly/SELECT_ITEM', {item: item, name: this.name, multiple: event.metaKey});
			}
			this.$emit('onSelect' , item);
		},
		unSelect       () { this.$store.commit('Firefly/UNSELECT', {name: this.name}) },
		loadPath   (path) { this.$store.dispatch('Firefly/LOAD_ITEMS', {path: path, name: this.name}); },
		doubleClick: function (item) {
			if (item.type == 'dir') {
				// Don't redirect on double click if we are editing the items name
				if (!this.isEditingItemName(item))
					this.loadPath(item.path);
			} else {
				this.$emit('onDoubleClickItem', item);
			}
		},
		dragStart: function(item) {
			// if item is not selected then select it
			if (!this.isSelected(item.id)) {
				this.$store.commit('Firefly/SELECT_ITEM', {item: item, name: this.name})
			}
		},
		selectAll: function() {
			this.$store.commit('Firefly/SELECT_ALL', {path: this.path, name: this.name});
		},
		testy: function() {alert('testy')},
		onKeyup: function(event) {
			if (event.which === 37)
				this.selectPrevious(event.shiftKey);
			if (event.which === 39)
				this.selectNext(event.shiftKey);
		}
	},
	beforeDestroy: function() {
		window.removeEventListener('keyup', this.onKeyup);
	},
	mounted: function () {
		this.loadPath(this.startPath);
		window.addEventListener('keyup', this.onKeyup);
		// $(window).on('keydown', (event) => {
		// 	// Select all cmd+a
		// 	if (event.code === 65 && event.metaKey) {
		// 		event.preventDefault();
		// 		this.selectAll();
		// 	}
		// });

	},
});

