"use strict";

/**
 * ------------------------
 * <firefly-media-browser>
 * ------------------------
 *
 */
Vue.component('firefly-media-browser-breadcrumb', {
  props: {
    name: {
      type: String,
      required: true
    }
  },
  template: "\n\t\t<nav class=\"breadcrumb\">\n\t\t\t<firefly-item-breadcrumb :picker=\"name\" name=\"Media\" path=\"/\"></firefly-item-breadcrumb>\n\t\t\t<firefly-item-breadcrumb v-for=\"part in pathBits\" :picker=\"name\" :name=\"part.name\" :path=\"part.path\" :key=\"part.path\"></firefly-item-breadcrumb>\n\t\t</nav> \n\t",
  methods: {
    loadPath: function loadPath(path) {
      this.$store.dispatch('Firefly/LOAD_ITEMS', {
        path: path,
        name: this.name
      });
    }
  },
  computed: {
    dropText: function dropText() {
      return this.$store.getters['Firefly/getDropMoveText'](this.name);
    },
    path: function path() {
      return this.$store.getters['Firefly/getPath'](this.name);
    },
    pathBits: function pathBits() {
      return this.$store.getters['Firefly/getPathBits'](this.name);
    }
  }
});