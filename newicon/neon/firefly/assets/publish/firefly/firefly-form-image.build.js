"use strict";

// The image upload component could end up being quite complex.
// With a file browser and media manager.
// A cropper and image manipulator...
// So it makes sense we deffer to javascript for the heavy lifting

/**
 *
 * @events:
 * **on-error** event to listen to specific plupload errors:
 * @example:
 * `<firefly-form-image @on-error="onError($event)"></firefly-form-image>`
 * The event object will have {name: name, error: err, uploader: up} where:
 * - {String} name: Name of the picker with the plupload that fired the event
 * - {Object} error: An object containing keys:
 *     - {Number} code: The plupload error code
 *     - {String} message: Description of the error
 * - {Object} uploader: plupload.Uploader object
 *
 * **click** - fired when the picker modal is launched
 */
Vue.component('firefly-form-image', {
  props: {
    name: {
      type: String,
      required: true
    },
    value: String,
    // the uuid of the file
    crop: false,
    cropWidth: '',
    cropHeight: '',
    startPath: {
      type: String,
      default: '/'
    },
    readOnly: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      editing: false,
      selectedId: '',
      editItem: null
    };
  },
  template: "\n\t\t<div>\n\t\t\t<input @input=\"$emit('input', $event.target.value)\" type=\"hidden\" :name=\"name\" :value=\"selectedId\" :readonly=\"readOnly\">\n\t\t\t<span v-if=\"crop\" class=\"hint-block hint-bottom\">The image aspect ratio: {{cropWidth}} x {{cropHeight}}</span>\n\t\t\t<div v-if=\"!readOnly\">\n\t\t\t\t<a class=\"btn btn-default\" @click.prevent=\"launchPicker()\"><i class=\"fa fa-image\"></i>\n\t\t\t\t\t<span v-if=\"!selectedId\">Pick Image...</span>\n\t\t\t\t\t<span v-if=\"selectedId\">Change Image...</i></span>\n\t\t\t\t</a>\n\t\t\t\t<button title=\"Remove\" v-if=\"selectedId\" class=\"btn btn-default\" @click.prevent=\"removeImage\"><i class=\"fa fa-times\"></i></button>\n\t\t\t</div>\n\t\t\t<div class=\"mtm\" :id=\"previewId\" v-if=\"selectedId\" >\n\t\t\t\t<img :src=\"url\" :style=\"imagePreviewStyle\" />\n\t\t\t</div>\n\t\t</div>\n\t",
  methods: {
    launchPicker: function launchPicker() {
      this.$emit('click');
      FIREFLY.picker(this.name, this.onSelected);
      FIREFLY.events.$on('uploadError', this.onError);
    },
    onError: function onError(err) {
      // only bubble events related to this picker
      if (err.name === this.name) {
        this.$emit('on-error', err);
      }
    },
    onSelected: function onSelected(item) {
      var vm = this;
      vm.selectedId = item.id;
      vm.$emit('input', vm.selectedId);

      if (vm.crop) {
        var settings = {
          cropAuto: true,
          cropWidth: vm.cropWidth,
          cropHeight: vm.cropHeight
        };
        FIREFLY.edit(item, settings, function (newItem) {
          vm.selectedId = newItem.id; // make compatible with v-model

          vm.$emit('input', vm.selectedId);
        });
      }
    },
    removeImage: function removeImage(e) {
      this.selectedId = '';
    }
  },
  beforeDestroy: function beforeDestroy() {
    FIREFLY.events.$off('uploadError', this.onError);
  },
  beforeMount: function beforeMount() {
    this.selectedId = this.value;
  },
  computed: {
    previewId: function previewId() {
      return this.name + '_imagePreview';
    },
    url: function url() {
      return FIREFLY.getImageUrl(this.selectedId);
    },
    imagePreviewStyle: function imagePreviewStyle() {
      return {
        maxWidth: '250px',
        maxHeight: '250px'
      };
    }
  }
});