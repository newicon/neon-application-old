/**
 * ------------------------
 * <firefly-editor-image>
 * ------------------------
 */
Vue.component('firefly-editor-null', {
	props: {
	},
	data: function() {
		return {};
	},
	template: `<div>There is no supported editor for this file</div>`,
});