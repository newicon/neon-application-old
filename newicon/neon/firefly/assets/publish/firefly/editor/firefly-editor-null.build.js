"use strict";

/**
 * ------------------------
 * <firefly-editor-image>
 * ------------------------
 */
Vue.component('firefly-editor-null', {
  props: {},
  data: function data() {
    return {};
  },
  template: "<div>There is no supported editor for this file</div>"
});