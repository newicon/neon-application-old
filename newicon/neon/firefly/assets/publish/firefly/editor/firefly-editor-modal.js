/**
 * ------------------------
 * <firefly-editor-modal>
 * ------------------------
 * This component houses a firefly file editor in a modal
 * Depends on neon modal component
 */
Vue.component('firefly-editor-modal', {
	props: {
		item: {type: Object, required:true},
        settings: {type: Object, required:false},
	},
	template: `
			<firefly-editor-image :item="item" :settings="settings" @on-saved="saved"></firefly-editor-image>
			<!--<button class="btn btn-primary" @click="doSave" >Save</button>-->
			<!--<button class="btn btn-default" @click="$emit('close')">Close</button>-->
	`,
	methods: {
        // Tell the editor to save
        doSave: function() {
            FIREFLY.editor.save();
        },
        saved: function(edited) {
            FIREFLY.events.$emit('edited', edited);
        }
	}
});
