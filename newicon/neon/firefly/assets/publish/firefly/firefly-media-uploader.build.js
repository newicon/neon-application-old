"use strict";

Vue.component('firefly-media-uploader', {
  props: {
    name: {
      type: String,
      required: true
    },
    path: {
      type: String,
      default: '/'
    },
    hide: false
  },
  data: function data() {
    var id = _.uniqueId('uploader_');

    return {
      container: id + '_container',
      drop: id + '_drop',
      browse: id + '_browse',
      dragOver: false
    };
  },
  template: "\n\t\t<div class=\"fireflyMediaUploder\">\n\t\t\t<div :id=\"container\">\n\t\t\t\t<div :id=\"drop\" v-show=\"hide\">\n\t\t\t\t\t<div class=\"text-center\" ><br/>\n\t\t\t\t\t\t<h3>Drop files anywhere to upload</h3>\n\t\t\t\t\t\t<p>or</p>\n\t\t\t\t\t\t<a :id=\"browse\" style=\"font-size:20px;\" href=\"#\" class=\"btn btn-lg btn-primary native-file-picker\">\n\t\t\t\t\t\t\t<i class=\"fa fa-upload\"></i>\n\t\t\t\t\t\t\tSelect files...\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n \t\t\t<transition name=\"fade\">\n\t\t\t\t<div id=\"bigDropUploader\" class=\"bigDropUploader\" v-show=\"dragOver\" style=\"opacticy:0.5;background:rgba(0,0,0,0.5);top:0;bottom:0;left:0;right:0;position:fixed;z-index:1000\">\n\t\t\t\t\t<div class=\"bigDropUploader_content\" style=\"\">\t\n\t\t\t\t\t\t<h1>Drop files here to upload</h1>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</transition>\n\n\t\t</div>\n\t",
  methods: {
    attachDragInOutEvents: function attachDragInOutEvents() {
      var vm = this;
      this.timer = {}; //do not care much for this code...

      window.addEventListener("dragenter", this.dragenter, false);
      window.addEventListener('dragover', this.dragover, true);
      window.addEventListener("dragleave", this.dragleave, false);
    },
    dragenter: function dragenter(e) {
      if (this.timer) clearTimeout(this.timer);
      this.dragOver = true;
    },
    dragover: function dragover(e) {
      if (this.timer) clearTimeout(this.timer);
      var vm = this;
      this.timer = setTimeout(function () {
        vm.dragOver = false;
      }, 200);
      this.dragOver = true;
    },
    dragleave: function dragleave(e) {
      if (this.timer) clearTimeout(this.timer);
      var vm = this;
      this.timer = setTimeout(function () {
        vm.dragOver = false;
      }, 200);
    }
  },
  mounted: function mounted() {
    var _this = this;

    this.uploader = FIREFLY.createPlupload(this.name, {
      container: this.container,
      drop_element: this.drop,
      browse_button: this.browse
    }); // Now that we've initialized the pluploader
    // we can wire up the larger full screen drop zone
    // -- Configure DROPZONE. -- //
    // Wiki: https://github.com/moxiecode/moxie/wiki/FileDrop

    var dropzone = new moxie.file.FileDrop({
      drop_zone: $('#bigDropUploader')[0]
    }); // When the event is fired, the context (ie, this)
    // is the actual dropzone. As such, you can access
    // the files using any of the following:
    // --
    // * this.files
    // * dropzone.files
    // * event.target.files

    dropzone.ondrop = function (event) {
      // The addFile() method can accept an individual
      // file or an array of file sources.
      _this.dragOver = false;

      _this.uploader.addFile(dropzone.files);
    };

    dropzone.init();
    this.attachDragInOutEvents();
  },
  beforeDestroy: function beforeDestroy() {
    window.removeEventListener("dragenter", this.dragenter, false);
    window.removeEventListener('dragover', this.dragover, true);
    window.removeEventListener("dragleave", this.dragleave, false);
    this.uploader.destroy();
  }
});