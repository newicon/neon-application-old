"use strict";

/**
 * ------------------------
 * <firefly-browser-picker>
 * ------------------------
 *
 */
Vue.component('firefly-picker-modal', {
  props: {
    name: {
      type: String,
      required: true
    }
  },
  template: "\n\t\t<div class=\"fireflyPicker\">\n\t\t\t<firefly-media-browser-toolbar slot=\"header\" :name=\"name\" class=\"workbenchHeader-modal\"></firefly-media-browser-toolbar>\n\t\t\t<firefly-media-browser-breadcrumb :name=\"name\"></firefly-media-browser-breadcrumb>\n\t\t\t<firefly-media-browser :name=\"name\" @onDoubleClickItem=\"selected\"></firefly-media-browser>\n\t\t</div>\n\t",
  computed: {
    selectedItem: function selectedItem() {
      return this.$store.getters['Firefly/getSelectedItem'](this.name);
    },
    isSelectedItemImage: function isSelectedItemImage() {
      return this.selectedItem;
    } // return this.$store.getters['Firefly/getIsItemImage'](this.selectedItem); }

  },
  methods: {
    selected: function selected() {
      // Launch a gloabl event
      FIREFLY.events.$emit('selected', this.selectedItem);
    }
  }
});