<?php

/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 17/01/2017 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */
namespace neon\firefly\assets;

use neon\core\assets\CoreAsset;
use neon\core\assets\JQueryUiAsset;
use yii\web\AssetBundle;

class BrowserAsset extends AssetBundle
{
	public $sourcePath = __DIR__.'/publish';

	public $js = [
		'vendor/plupload/js/plupload.full.min.js',
		'vendor/cropper/cropper.min.js',

		(YII_DEBUG ? 'firefly/firefly.js' : 'firefly/firefly.build.js'),

		(YII_DEBUG ? 'firefly/store/getters.js' : 'firefly/store/getters.build.js'),
		(YII_DEBUG ? 'firefly/store/actions.js' : 'firefly/store/actions.build.js'),
		(YII_DEBUG ? 'firefly/store/mutations.js' : 'firefly/store/mutations.build.js'),
		(YII_DEBUG ? 'firefly/store/store.js' : 'firefly/store/store.build.js'),

		//'plupload/js/moxie.min.js',
		// Firefly files to be built
		(YII_DEBUG ? 'firefly/firefly-form-file-list.js' : 'firefly/firefly-form-file-list.build.js'),
		(YII_DEBUG ? 'firefly/firefly-form-input-file-upload.js' : 'firefly/firefly-form-input-file-upload.build.js'),
		(YII_DEBUG ? 'firefly/firefly-form-file-browser.js' : 'firefly/firefly-form-file-browser.build.js'),
		(YII_DEBUG ? 'firefly/firefly-form-image.js' : 'firefly/firefly-form-image.build.js'),
		(YII_DEBUG ? 'firefly/firefly-picker-modal.js' : 'firefly/firefly-picker-modal.build.js'),

		(YII_DEBUG ? 'firefly/firefly-item-sidebar.js' : 'firefly/firefly-item-sidebar.build.js'),
		(YII_DEBUG ? 'firefly/firefly-img.js' : 'firefly/firefly-img.build.js'),

		(YII_DEBUG ? 'firefly/firefly-item-name.js' : 'firefly/firefly-item-name.build.js'),
		(YII_DEBUG ? 'firefly/firefly-item.js' : 'firefly/firefly-item.build.js'),
		(YII_DEBUG ? 'firefly/firefly-item-parent.js' : 'firefly/firefly-item-parent.build.js'),

		(YII_DEBUG ? 'firefly/editor/firefly-editor-modal.js' : 'firefly/editor/firefly-editor-modal.build.js'),
		(YII_DEBUG ? 'firefly/editor/firefly-editor-image.js' : 'firefly/editor/firefly-editor-image.build.js'),
		(YII_DEBUG ? 'firefly/editor/firefly-editor-null.js' : 'firefly/editor/firefly-editor-null.build.js'),

		(YII_DEBUG ? 'firefly/firefly-media-inline.js' : 'firefly/firefly-media-inline.build.js'),
		(YII_DEBUG ? 'firefly/firefly-media-uploader.js' : 'firefly/firefly-media-uploader.build.js'),
		(YII_DEBUG ? 'firefly/firefly-media-browser.js' : 'firefly/firefly-media-browser.build.js'),
		(YII_DEBUG ? 'firefly/firefly-media-browser-breadcrumb.js' : 'firefly/firefly-media-browser-breadcrumb.build.js'),
		(YII_DEBUG ? 'firefly/firefly-item-breadcrumb.js' : 'firefly/firefly-item-breadcrumb.build.js'),
		(YII_DEBUG ? 'firefly/firefly-media-browser-toolbar.js' : 'firefly/firefly-media-browser-toolbar.build.js'),
		(YII_DEBUG ? 'firefly/firefly-media-upload-button.js' : 'firefly/firefly-media-upload-button.build.js')

	];

	public $css = [
		'vendor/cropper/cropper.css',
		'css/browser.css'
	];

	public $depends = [
		CoreAsset::class,
		JQueryUiAsset::class
	];

}
