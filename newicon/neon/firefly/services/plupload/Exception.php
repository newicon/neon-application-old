<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2017 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 11/03/2017
 * @package neon
 */

namespace neon\firefly\services\plupload;

class PluploadException extends \Exception
{
}