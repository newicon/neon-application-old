<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 26/11/2016 12:24
 * @package neon
 */

namespace neon\firefly\services;

use neon\core\helpers\Url;
use neon\core\helpers\Hash;
use neon\core\web\UploadedFile;
use neon\firefly\services\fileManager\FileNotFoundException;
use \neon\firefly\services\fileManager\interfaces\IFileManager;
use \neon\firefly\services\fileManager\models\FileManager as Model;
use neon\firefly\services\fileManager\FileExceedsMemoryLimitException;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;

/**
 * The file manager intends to abstract things like paths and drives from the application
 * instead giving the application a uuid that refers to a file.  Meanwhile this file can be moved,
 * and changed about without breaking.
 * Class FileManager
 * @package neon\firefly
 */
class FileManager implements IFileManager
{

	/**
	 * The default drive to use to store files
	 * @var  string  the Drive name
	 */
	public $defaultDrive = null;

	/**
	 * @inheritdoc
	 */
	public function save($contents, $name = null, $meta = null)
	{
		$driver = $this->getDrive();
		$fm = new Model();
		$path = $this->_generatePath($name, $fm);
		$success = $driver->put($path, $contents);
		if ($success) {
			$fm->attributes = [
				'name' => $name,
				'path' => $path,
				'drive' => $driver->getName(),
				'mime_type' => $driver->getMimeType($path),
				'size' => $driver->getSize($path),
				'meta' => $meta
			];
			$success = $fm->save();
		}
		return $success ? $fm->uuid : null;
	}

	/**
	 * @inheritdoc
	 */
	public function saveFile($file, $meta = null)
	{
		neon()->firefly->isFileObjectValid($file);
		$stream = fopen($file->getRealPath(), 'r+');
		$uuid = $this->save($stream, $file->getFilename(), $meta);
		if (is_resource($stream)) fclose($stream);
		return $uuid;
	}

	/**
	 * @inheritdoc
	 */
	public function getMeta($uuid)
	{
		try {
			$model = $this->_get($uuid);
		} catch (FileNotFoundException $e) {
			return [];
		}
		return $model->toArray();
	}

	/**
	 * @inheritdoc
	 */
	public function exists($uuid)
	{
		try {
			$this->_get($uuid);
		} catch (FileNotFoundException $e) {
			return false;
		}
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function read($uuid)
	{
		try {
			$drive = $this->getDriveFor($uuid, $file);
		} catch (FileNotFoundException $e) {
			return false;
		}
		$this->_checkFileSizeLimit($uuid);
		return $drive->read($file['path']);
	}

	/**
	 * @inheritdoc
	 */
	public function readStream($uuid)
	{
		try {
			$drive = $this->getDriveFor($uuid, $file);
		} catch (FileNotFoundException $e) {
			return false;
		}
		return $drive->readStream($file['path']);
	}

	/**
	 * @inheritdoc
	 */
	public function getUrl($uuid)
	{
		return  Url::to(['/firefly/file/get', 'id' => $uuid], true);
	}

	/**
	 * @inheritdoc
	 */
	public function getImage($uuid, $params=[])
	{
		$p = array_merge(['/firefly/file/img', 'id'=>$uuid], $params);
		return Url::to($p, true);
	}

	/**
	 * @inheritdoc
	 */
	public function getVisibility($uuid)
	{
		try {
			$drive = $this->getDriveFor($uuid, $file);
		} catch (FileNotFoundException $e) {
			return false;
		}
		return $drive->getVisibility($file['path']);
	}

	/**
	 * @inheritdoc
	 */
	public function setVisibility($uuid, $visibility)
	{
		try {
			$drive = $this->getDriveFor($uuid, $file);
		} catch (FileNotFoundException $e) {
			return false;
		}
		return $drive->setVisibility($file['path'], $visibility);
	}

	/**
	 * @inheritdoc
	 */
	public function copy($uuid)
	{
		$model = $this->_get($uuid);
		$content = neon()->firefly->readStream($uuid);
		$newUuid = neon()->firefly->save($content, $model->name, $model->meta);
		return $newUuid;
	}

	/**
	 * @inheritdoc
	 */
	public function delete($uuid)
	{
		// TODO: `$model = $this->_get($uuid); $model->softDelete();`
	}

	/**
	 * @inheritdoc
	 */
	public function destroy($uuid)
	{
		$driver = $this->getDriveFor($uuid, $file);
		$deletedFile = $driver->delete($file['path']);
		$deletedDb = $file->destroy();
		return $deletedDb;
	}

	/**
	 * @inheritdoc
	 */
	public function restore($uuid)
	{
		// TODO : implement model restore
	}

	/**
	 * @inheritdoc
	 */
	public function prepend($uuid, $contents, $separator = PHP_EOL)
	{
		$drive = $this->getDriveFor($uuid, $file);
		return $drive->prepend($file['path'], $contents, $separator);
	}

	/**
	 * @inheritdoc
	 */
	public function append($uuid, $contents, $separator = PHP_EOL)
	{
		$drive = $this->getDriveFor($uuid, $file);
		return $drive->append($file['path'], $contents, $separator);
	}

	/**
	 * @inheritdoc
	 */
	public function getDrive()
	{
		if (empty($this->defaultDrive)) {
			throw new InvalidConfigException('You must define a default drive for the File Manager - Set "defaultDrive" configuration option to be one of the Drive Managers configured drives - see the Drive Manager\'s "drives" config option');
		}
		return neon()->firefly->drive($this->defaultDrive);
	}

	/**
	 * @inheritdoc
	 */
	public function getDriveFor($uuid, &$file)
	{
		$file = $this->_get($uuid);
		return neon()->firefly->drive($file->drive);
	}

	/**
	 * @inheritdoc
	 */
	public function sendFile($uuid, $download=false)
	{
		$meta = $this->getMeta($uuid);
		$drive = $this->getDriveFor($uuid, $file);
		$read = $this->readStream($uuid);
		$meta['mime_type'] = $drive->getMimeType($meta['path']);
		// log the download stat
		if ($download === true) {
			$file->downloaded = $file->downloaded + 1;
			$file->save();
		}
		neon()->response->sendStreamAsFile($read, $meta['path'], [
			'mimeType' => $meta['mime_type'],
			'inline' => !$download
		]);
		return neon()->response;
	}

	/**
	 * @inheritdoc
	 */
	public function getFileHash($uuid, $refresh = false)
	{
		$data = $this->getMeta($uuid);
		$hash = $data['file_hash'] ? $data['file_hash'] : null;
		if ($refresh || $hash === null) {
			$driver = $this->getDriveFor($uuid, $file);
			$hash = $driver->hash($file['path']);
			$model = $this->_get($uuid);
			$model->file_hash = $hash;
			$model->save();
		}
		return $hash;
	}

	/**
	 * @inheritdoc
	 */
	public function getFileName($uuid)
	{
		$meta = $this->getMeta($uuid);
		return isset($meta['name']) ? $meta['name'] : false ;
	}

	/**
	 * @inheritdoc
	 */
	public function setFileName($uuid, $name)
	{
		try {
			$drive = $this->getDriveFor($uuid, $file);
		} catch (FileNotFoundException $e) {
			return false;
		}
		$file->name = $name;
		return $file->save();
	}

	/**
	 * @inheritdoc
	 */
	public function getSize($uuid)
	{
		$meta = $this->getMeta($uuid);
		return isset($meta['size']) ? $meta['size'] : false ;
	}

	/**
	 * @inheritdoc
	 */
	public function setMeta($uuid, $meta)
	{
		try {
			$model = $this->_get($uuid);
			$model->meta = $meta;
			return $model->save();
		} catch (FileNotFoundException $e) {
			return false;
		}
	}

	/**
	 * Find a file manager file model by its path and drive location
	 *
	 * @param string $drive
	 * @param string $path
	 * @return array|null
	 */
	public function findFile($drive, $path)
	{
		$model = Model::find()->where(['drive' => $drive, 'path' => $path])->one();
		if ($model === null)
			return null;
		return $model->toArray();
	}

	/**
	 * Checks the file does not exceed PHP memory limit - and throws an exception.
	 *
	 * @param $uuid
	 * @throws \neon\firefly\services\fileManager\FileExceedsMemoryLimitException
	 */
	protected function _checkFileSizeLimit($uuid)
	{
		if (function_exists('ini_get')) {
			$bytes = to_bytes(ini_get('memory_limit'));
			$size = $this->getSize($uuid);
			if ($size >= $bytes) {
				throw new FileExceedsMemoryLimitException();
			}
		}
	}

	/**
	 * Get the row from the database by uuid.
	 * This function is a stub as can add a simple session cache.
	 *
	 * @param  string $uuid
	 * @return  Model
	 */
	protected function _get($uuid)
	{
		return Model::get($uuid);
	}

	/**
	 * Auto generate a sensible path
	 * By default this will generate path based on the year/month/[fileName]_[uuid].extension
	 * where [fileName] is the origin file name supplied
	 * and [uuid] is the filemanager uuid key for this file
	 * e.g. 2016/11
	 *
	 * @param  string  $name  - The actual file name
	 * @return  string  - The full file past on the drive
	 */
	protected function _generatePath(&$name=null, $fm)
	{
		$info = pathinfo($name);
		$fileName = $info['filename'];
		$extension = isset($info['extension']) ? '.' . $info['extension'] : '';
		if (empty($fileName)) {
			$fileName = 'nameless';
			$name = 'nameless';
		}
		return date('Y') . '/' . date('m') . '/' . $fileName . '.' . $fm->uuid . $extension;
	}

}
