<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2017 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 06/05/2017
 * @package neon
 */

namespace neon\firefly\services;

use \Intervention\Image\ImageManager as InterventionImageManager;
use \Intervention\Image\Exception\NotReadableException;
use \Intervention\Image\Image;
use \neon\core\helpers\Arr;

class ImageManager
{
	/**
	 * @param string $id fileManager uuid
	 * @return mixed
	 * @throws \Exception  any exceptions are thrown for controllers to handle
	 */
	public function process($id, $params)
	{
		$imageContent = neon()->firefly->fileManager->exists($id)
			? neon()->firefly->fileManager->readStream($id)
			: $this->getPlaceholderImageContents();

		// Create an image manager instance with favored driver
		$manager = new InterventionImageManager();
		$image = $manager->make($imageContent);
		$image->interlace();
		// Get url params
		$width = Arr::get($params, 'w', Arr::get($params, 'width', false));
		$height = Arr::get($params, 'h', Arr::get($params, 'height', false));

		if (Arr::get($params, 'fit', false)) {
			// add callback functionality to retain maximal original image size
			$image->fit($width, $height, function ($constraint) {
				$constraint->upsize();
			});
		} elseif ($width || $height) {
			$image->resize($width, $height, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
		}

		// ================================
		// Rotate
		// ================================
		$rotate = Arr::get($params, 'rotate', false);
		if ($rotate) {
			$image->rotate($rotate);
		}

		// =================================
		// Flip
		// =================================
		$flip = Arr::get($params, 'flip', false);
		if ($flip !== false) {
			// flip horizontal
			if ($flip === 'v') {
				$image->flip('v');
			} else {
				// by default it will flip horizontally
				$image->flip('h');
			}
		}

		// =================================
		// filter
		// =================================
		$filter = Arr::get($params, 'filter', false);
		if ($filter) {
			$image = $this->runFilter($image, $filter);
		}

		// =================================
		// Crop
		// =================================
		$crop = Arr::get($params, 'crop', false);
		if ($crop) {
			$image = $this->runCrop($image, $crop);
		}

		// =================================
		// quality
		// =================================
		$quality = Arr::get($params, 'q', 90);

		$response = $image->response(null, $quality);


		return $response;
	}

	/**
	 * Perform filter image manipulation.
	 * @param  Image $image The source image.
	 * @return Image The manipulated image.
	 */
	public function runFilter(Image $image, $filter)
	{
		$filters = explode(',',$filter);
		if (in_array('greyscale', $filters, true)) {
			$this->runGreyscaleFilter($image);
		}
		if (in_array('sepia', $filters, true)) {
			$this->runSepiaFilter($image);
		}
		if (in_array('invert', $filters, true)) {
			$image->invert();
		}
		return $image;
	}
	/**
	 * Perform greyscale manipulation.
	 * @param  Image $image The source image.
	 * @return Image The manipulated image.
	 */
	public function runGreyscaleFilter(Image $image)
	{
		return $image->greyscale();
	}
	/**
	 * Perform sepia manipulation.
	 * @param  Image $image The source image.
	 * @return Image The manipulated image.
	 */
	public function runSepiaFilter(Image $image)
	{
		$image->greyscale();
		$image->brightness(-10);
		$image->contrast(10);
		$image->colorize(38, 27, 12);
		$image->brightness(-10);
		$image->contrast(10);
		return $image;
	}

	public function runCrop(Image $image, $params)
	{
		if (is_string($params)) {
			$bits = explode(',', $params);
			$w = Arr::get($bits, 0, null);
			$h = Arr::get($bits, 1, null);
			$x = Arr::get($bits, 2, null);
			$y = Arr::get($bits, 3, null);
		}
		if (is_array($params)) {
			$w = Arr::get($params, 'width', null);
			$h = Arr::get($params, 'height', null);
			$x = Arr::get($params, 'x', null);
			$y = Arr::get($params, 'y', null);
		}
		if ($w == 0 && $h == 0 && $x == 0 && $y ==0) {
			return $image;
		}
		$image->crop($w, $h, $x, $y);
		$manager = new InterventionImageManager();
		$bg = $manager->canvas($w, $h, array(255, 255, 255, 0));
		// Fill up the blank spaces with transparent color
		$bg->insert($image, 'center');

		return $bg;
	}

	/**
	 * @return string
	 */
	public function getPlaceholderImageContents()
	{
		// should be configurable by settings somewhere
		return file_get_contents(dirname(__DIR__).'/assets/placeholder.jpg');
	}

	public function getNotReadableImageContents()
	{
		// should be configurable by settings somewhere
		return file_get_contents(dirname(__DIR__).'/assets/placeholder-broken.png');
	}
}