<?php

namespace neon\firefly\services\mediaManager;

use \yii\web\BadRequestHttpException;

class InvalidDirectoryException extends BadRequestHttpException
{
}