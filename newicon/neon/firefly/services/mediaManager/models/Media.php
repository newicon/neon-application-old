<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/11/2016 23:44
 * @package neon
 */

namespace neon\firefly\services\mediaManager\models;

use \neon\core\db\ActiveRecord;
use neon\core\helpers\Str;
use neon\firefly\services\fileManager\models\FileManager;
use neon\firefly\services\mediaManager\DirectoryExistsException;

/**
 * This is the model class for table "firefly_media".
 *
 * @property  string  $id
 * @property  string  $parent_id
 * @property  string  $path
 * @property  string  $type
 */
class Media extends ActiveRecord
{
	/**
	 * Turn on soft delete
	 * @var bool
	 */
	public static $softDelete = true;

	/**
	 * Auto generate ids when saving
	 * @var bool
	 */
	public static $idIsUuid64 = true;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%firefly_media}}';
	}

	/**
	 * Find media directory object by its path
	 *
	 * @param string $path
	 * @param boolean $withDeleted
	 * @return Media|null|\yii\db\ActiveRecord
	 */
	public static function findDirectoryByPath($path, $withDeleted=false)
	{
		$path = static::getPath($path);
		return static::query($withDeleted)
			->where(['path' => $path])
			->andWhere(['or', ['type' => 'dir'], ['type' => 'root']])
			->one();
	}

	/**
	 * Finds a file by its path
	 *
	 * @param string $path
	 * @return array|null|\yii\db\ActiveRecord
	 */
	public static function findFileByPath($path)
	{
		$path = static::getPath($path);
		return static::find()
			->where(['path' => $path])
			->andWhere(['type' => 'file'])
			->one();
	}

	/**
	 * Will return the parent of the given path for example
	 * a path of '/root/parent/thing' will return the '/root/parent' directory node
	 * or null if it does not exist
	 *
	 * @param string $path
	 * @param string $parentPath - the parent path used to find the parent node
	 * @return Media|null|\yii\db\ActiveRecord
	 */
	public static function findParentByPath($path, &$parentPath='')
	{
		$info = pathinfo($path);
		$parentPath = $info['dirname'];
		$node = Media::findDirectoryByPath($parentPath);
		return $node;
	}

	/**
	 * Create a new directory by its path and return its array representation
	 * This function assumes the parent node exists and throws an exception if the parent
	 * can not be found.
	 *
	 * @param string $path
	 * @return array
	 * @throws \Exception if the parent does not exist or model validation fails
	 */
	public static function createDirectory($path)
	{
		$parent = static::findParentByPath($path, $parentPath);
		if ($parent === null)
			throw new \Exception("Directory does not exist with path '$parentPath'");
		$node = new static(['path' => $path, 'type' => 'dir', 'parent_id' => $parent->id]);
		$node->save();
		if ($node->hasErrors()) {
			throw new \Exception('Model validation errors: ' . print_r($node->getErrors(), true));
		}
		return $node->toArray();
	}

	/**
	 * Get an array of Media object that are children
	 *
	 * @return array|Media[]
	 */
	public function getChildren()
	{
		return $this->getChildrenQuery()
			->all();
	}

	/**
	 * Create a relation for use in queries (->with('file'))
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getFile()
	{
		return $this->hasOne(FileManager::class, ['uuid' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['path'], 'validatePath'],
		];
	}

	/**
	 * Add validation errors if the path is wrong
	 *
	 * @param $attribute
	 * @param $params
	 */
	public function validatePath($attribute, $params)
	{
		$path = $this->$attribute;
		// must be absolute and start with /
		if ($path[0] !== '/')
			$this->addError('path', "Must be absolute path and start with a '/' provided: '$path'");
		// prevent folders with no names being created
		if (strpos($path, '//') !== false)
			$this->addError('path', "Path can not contain a double forward slash");
	}

	/**
	 * Whether this record represents a directory
	 *
	 * @return bool
	 */
	public function isDir()
	{
		return $this->type === 'dir' || $this->type === 'root';
	}

	/**
	 * Whether this record represents a file
	 *
	 * @return bool
	 */
	public function isFile()
	{
		return $this->type === 'file';
	}

	/**
	 * Get the root node
	 *
	 * @return Media
	 */
	public static function getRootNode()
	{
		return static::findDirectoryByPath('/');
	}

	/**
	 * Add a child node to this node
	 *
	 * @param Media|array $node
	 * @return boolean
	 */
	public function addChild($node)
	{
		$node->parent_id = $this->id;
		return $node->save();
	}

	/**
	 * Move this node into the specified node
	 *
	 * @param Media $intoNode - The node to move this node into
	 * @return boolean - whether the Media record saved successfully
	 * @throws \Exception
	 */
	public function moveInto(Media $intoNode)
	{
		// update the parent
		$this->parent_id = $intoNode->id;
		$success = false;

		// updating files is nice and easy
		if ($this->isFile()) {
			$this->path = $intoNode->path;
			$success = $this->save();
		}

		// update directories - and their child paths
		if ($this->isDir()) {
			$originalPath = $this->path;
			$newPath = $intoNode->path . $this->path;
			$this->path = $newPath;
			$success = $this->save();
			if ($success) {
				// update the children of this directory
				Media::replacePaths("$originalPath/", "$newPath/", "$originalPath/%");
			}
		}

		return $success;
	}

	/**
	 * Replace a path with a new path across all nodes
	 *
	 * @param string $findPath - the path string to find
	 * @param string $replacePath - the path string to replace the found path with
	 * @param string $wherePath - The mysql where path
	 * @return int - Number of affected rows
	 * @throws \Exception if query fails
	 */
	public static function replacePaths($findPath, $replacePath, $wherePath)
	{
		$tableName = static::tableName();
		$sql = 'UPDATE ' . $tableName . ' SET path = REPLACE(path, :findPath, :replacePath) WHERE path LIKE :wherePath';
		return Media::getDb()->createCommand($sql)
			->bindValues([
				':findPath' => $findPath,
				':replacePath' => $replacePath,
				':wherePath' => $wherePath
			])
			->execute();
	}

	/**
	 * Get a query object configured to filter by children
	 *
	 * @return \neon\core\db\ActiveQuery
	 */
	public function getChildrenQuery($withDeleted=false)
	{
		return static::query($withDeleted)->alias('media')
			->where(['parent_id' => $this->id]);
	}

	/**
	 * @inheritdoc
	 * Overwrite the path with a chroot - defined root path
	 * this prevents the full path being sent to the client if a root is defined
	 */
	public function toArray(array $fields = [], array $expand = [], $recursive = true)
	{
		$array = parent::toArray($fields, $expand, $recursive);
		$array['path'] = static::formatPath($array['path']);
		return $array;
	}

	/**
	 * Replaces root paths
	 * @param string $path
	 * @return string formatted path e.g. /a/b/c/d where root is defined as /a/b/ will give /c/d
	 */
	public static function formatPath($path)
	{
		return Str::iReplaceFirst(neon()->firefly->mediaManager->getRootPath(), '/', $path);
	}

	/**
	 * Lots of systems will ask for root access '/' as a convenient way to access the top folder of the a directory
	 * or for example when not specifying any particular folder hierarchy
	 * however we MUST always only serve the root as defined by neon()->firefly->mediaManager->getRootPath()
	 * as '/' may be mapped to a different root.
	 * @param string $path
	 * @return string
	 */
	public static function getPath($path)
	{
		if ($path === '/' && neon()->firefly->mediaManager->getRootPath() !== '/')
			$path = neon()->firefly->mediaManager->getRootPath();
		return $path;
	}
}