<?php
/**
 * Created by PhpStorm.
 * User: newicon
 * Date: 17/03/2017
 * Time: 13:14
 */

namespace neon\core\view;

use \Neon;
use neon\core\helpers\Html;
use neon\core\helpers\Hash;
use neon\core\helpers\Page;
use neon\core\helpers\Str;
use neon\core\helpers\Url;
use neon\core\helpers\Arr;
use neon\core\web\View;

use \Smarty;
use yii\base\InvalidConfigException;

/**
 * Class SmartySharedPlugins
 * This class provides smarty plugins that can be shared between the admin smarty renderer and cosmos
 * @package neon\core\view
 */
class SmartySharedPlugins
{
	const IMG_SRCSET_DEFAULT_WIDTHS = "350,640,768,1024,1280,1536,1920";

	/**
	 * Apply these plugin functions to a smarty object
	 * @param mixed $parent
	 * @param Smarty $smarty
	 * @throws \SmartyException
	 */
	public static function apply($parent, $smarty)
	{
		new self($parent, $smarty);
	}

	/**
	 * Maintain a list of tags that will be deprecated so we can aid upgrades and provide sensible error messages
	 * @return array
	 */
	public static function deprecatedTags()
	{
		return ['cosmos_head_script', 'cosmos_body_script'];
	}

	/**
	 * SmartySharedPlugins constructor.
	 * @param mixed $parent - should implement a parent page interface
	 * @param \Smarty $smarty
	 * @throws \SmartyException
	 */
	public function __construct($parent, $smarty)
	{
		// app plugin smarty directories
		$coreApps = neon()->coreApps;
		foreach ($coreApps as $key=>$app) {
			$smarty->addPluginsDir(Neon::getAlias("@neon/$key/plugins/smarty"));
		}

		// plugins
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'firefly_url', [$this, 'firefly_url']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'firefly_image', [$this, 'firefly_image']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'image_srcset', [$this, 'image_srcset']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'setting', [$this, 'setting']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'time', [$this, 'time']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'csrf_element', [$this, 'csrf_element']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'csrf_meta_tags', [$this, 'csrf_meta_tags']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'url', [$this, 'url']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'registerAsset', [$this, 'registerAsset']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'jsFile', [$this, 'jsFile']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'cssFile', [$this, 'cssFile']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'neon_head', [$this, 'neonHead']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'neon_body_begin', [$this, 'neonBodyBegin']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'neon_body_end', [$this, 'neonBodyEnd']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'hasPermission', [$this, 'hasPermission']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'style', [$this, 'style']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'uuid', [$this, 'uuid']);

		// modifiers
		$smarty->registerPlugin(Smarty::PLUGIN_MODIFIER, 'htmlAttributes', [$this, 'htmlAttributes']);
		$smarty->registerPlugin(Smarty::PLUGIN_MODIFIER, 'humanize', '\neon\core\helpers\Str::humanize');
		$smarty->registerPlugin(Smarty::PLUGIN_MODIFIER, 'json', [$this, 'json']);

		// blocks
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'markdown', [$this, 'markdown']);
		// All the same - ways to register a block of js :
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'script', [$this, 'js']);
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'js', [$this, 'js']);
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'Js', [$this, 'js']);
		// Register a block of css
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'css', [$this, 'css']);

		// experimental
		$smarty->registerPlugin(Smarty::PLUGIN_BLOCK, 'cmp', [$this, 'cmp']);


		// deprecated tags
		$this->registerDeprecatedTags($smarty);
	}

	/**
	 * @param \Smarty $smarty
	 * @throws \SmartyException
	 */
	public function registerDeprecatedTags($smarty)
	{
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'cosmos_head_script', [$this, 'neonHead']);
		$smarty->registerPlugin(Smarty::PLUGIN_FUNCTION, 'cosmos_body_script', [$this, 'neonBodyEnd']);
	}

	/**
	 * Parse a block of content as markdown
	 *
	 * ```smarty
	 * {markdown} ***markdown content*** {/markdown}
	 * ```
	 *
	 * @param $params
	 * @param $content
	 * @return string
	 */
	public function markdown($params, $content)
	{
		$parser = new MarkdownNeon();
		return $parser->parse($content);
	}


	/**
	 * @param $params
	 * @param $content
	 * @param $template
	 * @param $repeat
	 * @return string
	 */
	public function cmp($params, $content, $template, &$repeat)
	{
		// only output on the closing tag
		if (!$repeat) {
			$class = Arr::remove($params, 'class');
			$params['content'] = $content;
			if (class_exists($class)) {
				$widget = new $class();
				foreach($params as $key => $val) {
					if ($widget->hasProperty($key))
						$widget->$key = $val;
				}
				return $widget->run();
			} else {
				return "<div class=\"neonWidgetError alert alert-danger\">Widget class '$class' not found</div>";
			}
		}
	}

	/**
	 * Register a style configurable parameter
	 *
	 * @param array $params
	 * @param Smarty $smarty
	 */
	public function style($params, $smarty)
	{
		// possible vue style implementation
//		Arr::props($params, [
//			'name' => ['required' => true, 'type' => 'string'],
//			'type' => ['required' => true, 'type' => 'string'],
//			'options' => ['required' => false, 'type' => 'array', 'default' => []]
//		]);

		$name = Arr::getRequired($params, 'name');
		$type = Arr::getRequired($params, 'type');
		$options = Arr::get($params, 'options', []);
		if (!is_array($options))
			throw new \InvalidArgumentException('"options" property must be an array "' . gettype($options) . '" given');
		$default = Arr::get($params, 'default', '');

		$value = neon()->view->styleManager->add($name, $default, $type, $options);
		$smarty->assign($name, $value);
	}

	/**
	 * Function to expand an array into html attributes
	 *
	 * @param array $params
	 * @param Object $smarty
	 *
	 * @return string
	 */
	public function htmlAttributes($params, $smarty)
	{
		dp($params);
	}

	/**
	 * Smarty template function to get absolute URL for using in links
	 *
	 * Usage is the following:
	 *
	 * {url route='blog/view' alias=$post.alias user=$user.id}
	 *
	 * where route is Yii route and the rest of parameters are passed as is.
	 *
	 * @param array $params
	 * @return string
	 */
	public function url($params)
	{
		if (!isset($params['route'])) {
			trigger_error("path: missing 'route' parameter");
		}
		array_unshift($params, $params['route']) ;
		unset($params['route']);
		return Url::toRoute($params, true);
	}

	/**
	 * tag: csrf_element
	 *
	 * Output a form element with the csrf key in it.
	 * This is a hidden field and allows the form to post to yii
	 *
	 * ```{csrf_element}```
	 *
	 * @param array $params ['id']
	 * @return string html
	 */
	public function csrf_element($params)
	{
		return '<input type="hidden" name="'.neon()->request->csrfParam.'" value="' . neon()->request->getCsrfToken() . '" />';
	}

	/**
	 * Output Csrf meta tags
	 * @param $params
	 * @param $smarty
	 * @return string
	 */
	public function csrf_meta_tags($params, $smarty)
	{
		return Html::csrfMetaTags();
	}

	/**
	 * tag: firefly_url
	 *
	 * Get a url for a file managed by firefly from it's id
	 *
	 * ```{firefly_url id="the file uuid"}```
	 *
	 * @param array $params ['id']
	 * @return string  the URL to the file
	 * @throws \InvalidArgumentException
	 */
	public function firefly_url($params)
	{
		if (isset($params['id']))
			return neon()->firefly->getUrl($params['id']);
	}

	/**
	 * tag: firefly_image
	 *
	 * Get a url for an image managed by firefly from it's id
	 *
	 * ```{firefly_url id="the file uuid" ...}```
	 *
	 * @param array $params
	 *   'id' - the image id
	 *   'w' - the image width
	 *   'h' - the image height
	 *   'q' - quality of compression (0-100)
	 *   'fit' - fit the image into the shape
	 *   'rotate' - angle to rotate image by
	 *   'flip' - flip image horizontally (h) or vertically (v)
	 *   'crop' - crop the image
	 *
	 * @return string  the URL to the file
	 * @throws \InvalidArgumentException
	 */
	public function firefly_image($params)
	{
		if (isset($params['id']))
			return neon()->firefly->getImage(Arr::pull($params, 'id'), $params);
	}

	/**
	 * tag: image_srcset
	 *
	 * Gets a string suitable for placing inside an image tag srcset, works in conjunction with firefly_image_srcset
	 *
	 * ```{image_srcset firefly_id="the file uuid" ...}```
	 *
	 * Examples:
	 * ```{image_srcset image_id='z08MMpEHcW27bd_diYLIMw'}```
	 * ```{image_srcset image_id='z08MMpEHcW27bd_diYLIMw' id='image-tag-id' alt='image alt text' style='object-fit:cover' class='imgsetclass'}```
	 * ```{image_srcset image_id='z08MMpEHcW27bd_diYLIMw' widths='1000,2000'}```
	 * ```{image_srcset image_id='z08MMpEHcW27bd_diYLIMw' widths='1000,2000' breaks='50vw,100vw' min-break='600px'}```
	 * ```{image_srcset image_id='z08MMpEHcW27bd_diYLIMw' widths='300,600,900' resolutions='2x,3x'}```
	 *
	 * @todo: consider supporting additional firefly parameters
	 * @param $params
	 *    'firefly_id   - the image id
	 *    'quality'     - quality parameter is passed through as a firefly q parameter
	 *    'widths'      - comma separated list of widths in pixels as ints
	 *    'src-width'   - width to use for the img src (if unspecified the the smallest from widths will be used) in pixels as int
	 *    'breaks'      - comma separated list of break point sizes (will be automatically generated if not present) as int with 'px' or 'vw' specified
	 *    'min-break'   - force use of a specific min-width as int with 'px' or 'vw' specified
	 *    'resolutions' - alternative to breaks, a set of resolutions (will be ignored if breaks are used) as float/int with 'x' specified
	 *    + any other parameters passed in will be built into the resulting tag string (these are not validated)
	 * @return string the srcset string
	 * @throws \InvalidArgumentException
	 * @see https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
	 */
	public function image_srcset($params)
	{
		if (!isset($params['firefly_id'])) {
			throw new \InvalidArgumentException("'firefly_id' is a required parameter");
		}
		if (!isset($params['widths'])) {
			// todo: this should be possible to set via configuration
			// use some hopefully sensible defaults
			$params['widths'] = self::IMG_SRCSET_DEFAULT_WIDTHS;
		}

		$id = Arr::pull($params,'firefly_id');
		$widths = array_map('trim',explode(',',Arr::pull($params,'widths')));
		$srcsetParts = [];
		$srcSizeParts = [];
		$useResolutions = [];
		$minBreak = false;
		if (isset($params['min-break'])) {
			$minBreak = Arr::pull($params,'min-break');
			if (!Str::validateNumericWithUnits($minBreak,['px','vw'])) {
				throw new \InvalidArgumentException("The min-break value '".$minBreak."' should be numeric and end with 'px' or 'vw'");
			}
		}
		if (isset($params['breaks'])) {
			// optionally validate & collate the breaks for the 'sizes' attribute
			$breaks = array_map('trim',explode(',',Arr::pull($params,'breaks')));
			foreach ($breaks as $idx=>$break) {
				if (!Str::validateNumericWithUnits($break, ['px','vw'])) {
					throw new \InvalidArgumentException("The break value '".$break."' should be numeric and end with 'px' or 'vw'");
				}
				if ($idx==0 && $minBreak) {
					$break = "(min-width: ".$minBreak.") ".$break;
				}
				$srcSizeParts[]=$break;
			}
		}
		elseif (isset($params['resolutions'])) {
			// optionally validate & collate any specified resolutions to use (if breaks haven't been specified)
			$useResolutions = array_map('trim',explode(',',Arr::pull($params,'resolutions')));
			if (count($useResolutions) != count($widths)-1) {
				throw new \InvalidArgumentException("Since there are ".count($widths)." widths you should specify ".(count($widths)-1)." resolutions to apply");
			}
			foreach ($useResolutions as $useResolution) {
				if (!Str::validateNumericWithUnits($useResolution,['x'])) {
					throw new \InvalidArgumentException("The resolution '".$useResolution."' is not a valid resolution, it should be numeric and then end with an 'x'");
				}
			}
		}
		else {
			// if neither 'breaks' or 'resolutions' was specified then use the widths
			foreach ($widths as $idx=>$width) {
				if ($idx==0 && $minBreak) {
					$width = "(min-width: ".$minBreak.") ".$width;
				}
				$srcSizeParts[] = $width."px";
			}
		}

		// validate and collate the widths
		$useQuality = false;
		if (isset($params['quality'])) {
			$useQuality = Arr::pull($params, 'quality');
			if (!is_numeric($useQuality)) {
				throw new \InvalidArgumentException("the quality '".$useQuality."' must be numeric");
			}
		}
		foreach ($widths as $idx=>$width) {
			if (!is_numeric($width)) {
				throw new \InvalidArgumentException("One of the widths specified was not numeric '".$width."'");
			}
			$useParams = ['id'=>$id, 'w'=>(int)$width];
			if ($useQuality) {
				$useParams['q'] = $useQuality;
			}
			$imageUrl = $this->firefly_image($useParams);
			if ($useResolutions) {
				$srcsetParts[] = $imageUrl.( ($idx>=1) ? ' '.$useResolutions[$idx-1] : '' );
			}
			else {
				$srcsetParts[] = $imageUrl . ' ' . $width . 'w';
			}
			if ($idx==0 && !isset($params['src-width']) && !isset($params['src'])) {
				$params['src'] = $imageUrl;
			}
		}

		// collate the tag attributes
		if (isset($params['src-width'])) {
			$srcWidth = Arr::pull($params,'src-width');
			if (!is_numeric($srcWidth)) {
				throw new \InvalidArgumentException("src-width '".$srcWidth."' must be numeric");
			}
			$useParams = ['id'=>$id, 'w'=>(int)$srcWidth];
			if (isset($params['quality'])) {
				$useParams = (int)$params['quality'];
			}
			if (!isset($params['src'])) {
				$params['src'] = $this->firefly_image($useParams);
			}
		}
		if ($srcSizeParts && !isset($params['sizes'])) {
			$params['sizes'] = implode(', ', $srcSizeParts);
		}
		if ($srcsetParts && !isset($params['srcset'])) {
			$params['srcset'] = implode(', ', $srcsetParts);
		}

		// finally generate the tag ... note that these can overwrite the previously generated params
		$tagAttributes =  [];
		foreach ($params as $key=>$value) {
			$tagAttributes[] = $key."='".htmlspecialchars($value)."'";
		}

		return "<img ".implode(' ',$tagAttributes).">";
	}

	/**
	 * tag: setting
	 *
	 * Get a setting value from the settings manager
	 *
	 * ```{setting app='aaa' name='nnn' [default='xxx' assign='yyy']}```
	 *
	 * @param array $params
	 *   app - required - the settings app area
	 *   name - required - the setting required
	 *   default - optional - a default value if it's not set
	 *   assign - optional - if set assigns to this variable else returns the value
	 * @param object $smarty
	 * @return string
	 */
	public function setting($params, $smarty)
	{
		$app = isset($params['app']) ? $params['app'] : null;
		$name = isset($params['name']) ? $params['name'] : null;
		$default = isset($params['default']) ? $params['default'] : null;
		$assign = isset($params['assign']) ? $params['assign'] : null;
		if ($app && $name) {
			$value = neon('settings')->manager->get($app, $name, $default);
			if ($assign)
				$smarty->assign($assign, $value);
			else
				return $value;
		} else {
			return "Usage: you must provide an 'app' and a 'name' parameter. You provided ".print_r($params,true);
		}
	}

	/**
	 * tag: time
	 *
	 * Convert a timestamp into a YYYY-MM-DD HH:MM:SS string
	 *
	 * Usage:
	 * ```
	 *   {time} - returns the current time
	 *   {time stamp='123456'} - returns the timestamp formatted
	 * ```
	 *
	 * @param $params empty for now and integer timestamp otherwise
	 * @return string YYYY-MM-DD HH:MM:SS
	 */
	public function time($params)
	{
		$format = 'Y-m-d H:i:s';
		if (isset($params['stamp']))
			return date($format, (integer)$params['stamp']);
		return date($format);
	}

	/**
	 * Output a pre tag of nicely formatted json - useful for debugging
	 *
	 * @param $params
	 * @return string
	 */
	public function json($params)
	{
		return '<pre>' . json_encode($params, JSON_PRETTY_PRINT) . '</pre>';
	}

	/**
	 * Looks for a `position` or `pos` key in the params and returns the integer value for the string position
	 * ```
	 * $this->getAssetPosition(['pos' => 'end']) // gives: 3
	 * ```
	 * @see SmartySharedPlugins::registerAsset
	 * @see SmartySharedPlugins::js()
	 * @param array $params
	 * @param string $default - defaults to 'end'
	 * @return int
	 * @throws \Exception
	 */
	public function getAssetPosition($params, $default='end')
	{
		$position = Arr::get($params, 'pos', Arr::get($params, 'position', $default));
		$positions = [
			'head'  => View::POS_HEAD,
			'begin' => View::POS_BEGIN,
			'end'   => View::POS_END,
			'ready' => View::POS_READY,
			'load'  => View::POS_LOAD,
		];
		if (!array_key_exists($position, $positions)) {
			throw new \Exception('The javascript position specified must be one of ' . print_r(array_keys($positions)) . ', you specified "'.$position.'"');
		}
		return $positions[$position];
	}

	/**
	 * Smarty block function plugin
	 * Usage is the following:
	 *
	 * ```
	 * {Js} Javascript code here {/Js}
	 * {Js pos='end'} Javascript code here {/Js}
	 * {Js pos='ready'} jquery on ready code here {/Js}
	 * ```
	 *
	 * @param $params
	 *
	 *   [position|pos] with values:
	 *   The position param specified where the javascript block will be rendered on the page it can have
	 *   the following values:
	 *
	 *   - "head"  : This means the javascript is rendered in the head section.
	 *   - "begin" : This means the javascript is rendered at the beginning of the body section.
	 *   - "end"   : This means the javascript is rendered at the end of the body section.
	 *   - "ready" : This means the JavaScript code block will be enclosed within `jQuery(document).ready()`.
	 *   - "load"  : This means the JavaScript code block will be enclosed within `jQuery(window).load()`.
	 *
	 *
	 * @param $content
	 * @return void
	 * @throws \Exception
	 */
	public function js($params, $content)
	{
		if ($content == null) return;
		$key = Arr::get($params, 'key', null);
		$content = str_replace(['<script>', '</script>'], '', $content);
		neon()->view->registerJs($content, $this->getAssetPosition($params), $key);
	}

	/**
	 * Register a block of css code
	 *
	 * ```
	 * {css position='head'}
	 * .myStyle {color:'red'}
	 * {/css}
	 * ```
	 *
	 * @param $params
	 * [position|pos] string - the string position in the page where css should be output - defaults to 'head'
	 * [key] string - a key to uniquely identify this css block so it will not be rendered twice
	 * @param $content
	 * @throws \Exception
	 */
	public function css($params, $content)
	{
		if ($content == null) return;
		$key = Arr::get($params, 'key', null);
		$content = str_replace(['<style>', '</style>'], '', $content);
		neon()->view->registerCss($content, ['position'=>$this->getAssetPosition($params, 'head')], $key);
	}

	/**
	 * Register Asset bundle
	 *
	 * ```
	 * {registerAsset name='\yii\web\JqueryAsset'} // jquery will be loaded before the end body tag
	 * {registerAsset path='\yii\web\JqueryAsset' pos='end'} // jquery will be loaded before the end body tag
	 * {registerAsset path='\yii\web\ThemeBootstrap' assignUrl='themeUrl'} // jquery will be loaded before the end body tag
	 *```
	 *
	 * @param array $params
	 * - [name|path] {string} = a asset bundle path to register for e.g. \yii\web\JqueryAsset
	 * - [pos|position] {string:head|begin|end|ready|load} = the position where the bundle should be output on the page defaults to 'end'
	 * @param Smarty $smarty
	 * @throws InvalidConfigException if the asset bundle does not exist or a circular dependency is detected
	 * @throws \Exception if incorrect asset position is given
	 * @return void
	 */
	public function registerAsset($params, $smarty)
	{
		// get the bundle - will look for `name` or `bundle` or `path` keys
		$class = Arr::get($params, 'name', Arr::get($params, 'path', null));
		if ($class == null) return;

		// get the position - will looks for `pos` or `position` keys - defaults View::POS_END
		$position = Arr::get($params, 'pos', Arr::get($params, 'position', 'end'));

		$bundle = neon()->view->registerAssetBundle($class, $this->getAssetPosition($position));
		$assign = Arr::get($params, 'assignUrl', Arr::get($params, 'assign', false));
		$smarty->assign($assign, $bundle->baseUrl);
	}

	/**
	 * @param array $params
	 * - [src|url]    string - the js src attribute
	 * - [attributes] array  - additional html attributes for the script tag also supports the special `depends`
	 *                           attribute enabling this file to depend on an asset bundle
	 * - [depends]    string - asset bundle dependencies
	 * - [key]        string - key to uniquely identify this file - optional
	 */
	public static function jsFile($params)
	{
		$src = Arr::get($params, 'src', Arr::get($params, 'url', null));
		if ($src === null) {
			trigger_error("jsFile: missing 'url' or 'src' parameter");
		}
		$attributes = Arr::get($params, 'attributes', []);
		$attributes['depends'] = Arr::get($params, 'depends', null);
		$key = Arr::get($params, 'key', null);
		neon()->view->registerJsFile($src, $attributes, $key);
	}

	/**
	 * @param array $params
	 * - [src|url]    string - the js src attribute
	 * - [attributes] array  - additional html attributes for the script tag also supports the special `depends`
	 *                           attribute enabling this file to depend on an asset bundle
	 * - [depends]    string - asset bundle dependencies
	 * - [key]        string - key to uniquely identify this file - optional
	 */
	public static function cssFile($params)
	{
		$src = Arr::get($params, 'src', Arr::get($params, 'url', null));
		if ($src === null) {
			trigger_error("jsFile: missing 'url' or 'src' parameter");
		}
		$attributes = Arr::get($params, 'attributes', []);
		$attributes['depends'] = Arr::get($params, 'depends', null);
		$key = Arr::get($params, 'key', null);
		neon()->view->registerCssFile($src, $attributes, $key);
	}


	/**
	 * Generates a UUID
	 * return string $UUID
	 */
	public function uuid($params, $template) {
		return Hash::uuid64();
	}

	/**
	 * Outputs scripts at the head
	 * - this tag marks where the - 'head' - View::POS_HEAD - position is
	 */
	public function neonHead()
	{
		return neon()->view->head();
	}

	/**
	 * Outputs scripts after the first body tag
	 * - this tag marks where the - 'begin' - View::POS_BEGIN - position is
	 */
	public function neonBodyBegin()
	{
		return neon()->view->beginBody();
	}

	/**
	 * Outputs scripts at the end body tag position
	 * - this tag marks where the - 'end' - View::POS_END - position is
	 */
	public function neonBodyEnd()
	{
		return neon()->view->endBody();
	}

	/**
	 * check if a user has permission to do something
	 * @param array $params
	 * - [permission] string - the permission to check
	 * - [assign] string  - if set then the result is set to this parameter.
	 *   if not set then it is set to the same name as the permission
	 * - additional params - any additional params will be passed through to the
	 *   permission test
	 * @param Smarty $smarty
	 */
	public function hasPermission($params, $smarty)
	{
		if (empty($params['permission']))
			trigger_error("permission: missing 'permission' parameter");
		$permission = $params['permission'];
		$assign = empty($params['assign']) ? $permission : $params['assign'];
		$canDo = neon()->user->can($permission, $params);
		$smarty->assign($assign, $canDo);
	}
}
