<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 17/11/2016 17:03
 * @package neon
 */

namespace neon\core\helpers;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Intervention\Image\Exception\NotFoundException;
use \yii\helpers\Inflector;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class Str - is a string helper class to provide handy string functions
 * This essentially merges the Yii Inflector helper and the Yii StringHelper as well as
 * providing a few additional functions too.
 *
 * @package neon\core\helpers
 */
class Hash
{
	/**
	 * Generates a uuid
	 * @param  string  $separator the section separator for the uuid - normally '-'
	 * @return string
	 */
	public static function uuid($separator='-')
	{
		$sha = sha1(uniqid('', true).rand(1000000,9999999));
		return substr($sha,0,8).$separator.substr($sha,9,4).$separator.'3'.substr($sha,15,3).$separator.'8'.substr($sha,19,3).$separator.substr($sha,22,12);
	}

	/**
	 * Generates a 22 character long uuid using 0-9A-Za-z_-
	 * @return string
	 */
	public static function uuid64()
	{
		$uuid = self::uuid(''); // create a uuid without any '-' in it
		return self::uuid2uuid64($uuid);
	}

	/**
	 * Converts a uuid into a uuid64. If the uuid is already a uuid64
	 * it is simply returned.
	 * @param string $uuid
	 * @return string
	 */
	public static function uuid2uuid64($uuid)
	{
		if (strlen($uuid)==22)
			return $uuid;

		// remove any hyphens
		$uuid = str_replace('-','',$uuid);

		$uuid .= '0'; // 33 hexadecimal characters = 22 64-cimal chars.
		// to convert from base-16 to base-64 (not Base64)
		// 64 characters
		$encoding = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
		$uuid64 = '';
		$position = 0;
		while ($position < 33) {
			$snip = substr($uuid, $position, 3);
			$number = hexdec($snip);
			$uuid64 .= $encoding[(integer)floor($number/64)].$encoding[(integer)($number%64)];
			$position += 3;
		}
		return $uuid64;
	}

	/**
	 * Whether the given string is in the correct format of a uuid64
	 * @return boolean
	 */
	public static function isUuid64($test)
	{
		return (is_string($test) && strlen($test) === 22);
	}

	/**
	 * Get a token - that can be used to get a fresh instance of the object with the same configurations applied
	 * To get the object back from the token call self::getObjectFromToken
	 * @return string token
	 * @throws \ReflectionException
	 */
	public static function setObjectToToken($object, $duration=86400)
	{
		// get public properties of child class
		$data = \Neon::getObjectVars($object);
		$data['class'] = get_class($object);
		if (method_exists($object, 'toArray'))
			$data = array_merge($data, $object->toArray(['objectToken']));
		$token = sha1(serialize($data) . env('NEON_SECRET'));
		neon()->cache->set($token, $data, $duration); // 24 hour
		return neon()->security->maskToken($token);
	}

	/**
	 * Get an object from a previously defined token
	 * @param string $token
	 * @param string $expectClass - specify additional class check
	 * @return object
	 * @throws BadRequestHttpException
	 * @throws NotFoundHttpException
	 */
	public static function getObjectFromToken($token, $expectClass=null)
	{
		$data = neon()->cache->get(neon()->security->unmaskToken($token));
		if ($data === null)
			throw new NotFoundHttpException('Does not exist or has timed out');

		if (!isset($data['class']) || !class_exists($data['class']))
			throw new BadRequestHttpException('Not a valid class');

		// instantiate the class
		$class = Arr::remove($data, 'class');
		$object = new $class($data);
		if ($expectClass !== null && !$object instanceof $expectClass)
			throw new BadRequestHttpException('Not a valid class');

		return $object;
	}
}