<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2015 Newicon Ltd
 * @license http://www.newicon.net/neon/license
 */

namespace neon\core\db;

use \neon\core\db\Query;

/**
 * @inheritdoc
 */
class Connection extends \yii\db\Connection
{
	/**
	 *
	 * @inheritdoc
	 * @var \neon\core\db\Command
	 */
	public $commandClass = 'neon\core\db\Command';

	/**
	 * Use a fluid query interface
	 * @return \neon\core\db\Query
	 */
	public function query()
	{
		return new Query();
	}

	/**
	 * Returns a boolean indicating whether the database name has been set in the dsn
	 * @returns boolean
	 */
	public function hasDatabaseName()
	{
		// find the db name in the dsn string
		// https://regex101.com/r/aQO6vq/1 - use this link for a nice breakdown of how this regex works.
		$result = preg_match('/dbname=(.*?)(;|$)/i', $this->dsn, $matches);
		return $result;
	}
}