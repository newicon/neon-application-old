<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace neon\core\console;

use \Yii;
use \yii\helpers\Console;
use \yii\helpers\Inflector;

/**
 * Set up a hook so git triggers the correct badges for the branch in the readme
 */
class HookController extends \yii\console\controller
{
	public $defaultAction = 'update-readme';
	/**
	 * Updates the project readme file with correct badge links for the branch
	 *
	 * @param string $readmeFile - the readme file path to update e.g. ./README.md
	 * @return int
	 */
	public function actionUpdateReadme($readmeFile)
	{
		$branch = exec('git rev-parse --abbrev-ref HEAD');
		// find readme file
		$readme =  $readmeFile;
		// read the entire readme
		$str = file_get_contents($readme);
		// replace the branch tags
		// match branch in Quality score
		$matchBranch = '[0-9a-z_]+';
		$replace = [
			"/badges\\/quality-score\\.png\\?b=$matchBranch/" => "badges/quality-score.png?b=$branch",
			"/badges\\/coverage\\.png\\?b=$matchBranch/" => "badges/coverage.png?b=$branch",
			"/badges\\/build.png\\?b=$matchBranch/" => "badges/build.png?b=$branch",
			"/\\?branch=$matchBranch/" => "?branch=$branch",
		];
		$str = preg_replace(array_keys($replace), array_values($replace), $str);
		// write the new readme
		file_put_contents('README.md', $str);
	}
}
