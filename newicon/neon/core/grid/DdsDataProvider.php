<?php
/**
 * Created by PhpStorm.
 * User: newicon
 * Date: 23/08/2017
 * Time: 12:40
 */

namespace neon\core\grid;

use neon\core\grid\query\DdsQuery;
use neon\core\grid\query\IQuery;
use \yii\data\BaseDataProvider;


/**
 * @property DdsQuery $queryBuilder
 *
 *
 * Class DdsDataProvider
 * @package neon\core\grid
 */
class DdsDataProvider extends BaseDataProvider
{
	/**
	 * The dds class this data provider should represent
	 * @var string
	 */
	public $classType;

	/**
	 * Whether to include deleted records
	 * @var bool
	 */
	public $includeDeleted = false;

	/**
	 * @var DdsQuery
	 */
	private $_queryBuilder = null;

	/**
	 * On cloning make sure you clone the query builder
	 */
	public function __clone()
	{
		parent::__clone();
		if ($this->_queryBuilder)
			$this->_queryBuilder = clone $this->_queryBuilder;
	}

	/**
	 * @return DdsQuery
	 */
	public function getQueryBuilder()
	{
		if ($this->_queryBuilder === null) {
			$this->_queryBuilder = new DdsQuery();
		}
		return $this->_queryBuilder;
	}

	/**
	 * @inheritDoc
	 */
	protected function prepareModels()
	{
		// prepare pagination
		if (($pagination = $this->getPagination()) !== false) {
			$pagination->totalCount = $this->getTotalCount();
			if ($pagination->totalCount === 0) {
				return [];
			}
		}

		$limit =  [
			'start' => $pagination->getOffset(),
			'length' => $pagination->getLimit(),
			'total' => $pagination->totalCount
		];
		// get all result rows
		$filter = $this->getQueryBuilder()->getFilter();
		$order = $this->getQueryBuilder()->getOrder();
		$results = neon()->getDds()->IDdsObjectManagement
			->runSingleObjectRequest($this->classType, $filter, $order, $limit, $this->hasDeletedFilter());

		$this->getPagination()->totalCount = $results['total'];

		return $results['rows'];
	}

	/**
	 * @var \neon\core\form\Form
	 */
	private $_ddsForm;

	/**
	 * Get the DDS form for the class type
	 * @return \neon\core\form\Form
	 */
	public function getDdsForm()
	{
		if ($this->_ddsForm === null) {
			$this->_ddsForm = new \neon\core\form\Form(neon()->phoebe->getFormDefinition('daedalus', $this->classType));
		}
		return $this->_ddsForm;
	}

	public function getTotalCount()
	{
		return $this->prepareTotalCount();
	}

	/**
	 * @inheritDoc
	 */
	protected function prepareKeys($models)
	{
		// TODO: Implement prepareKeys() method.
	}

	/**
	 * Store a total count to save multiple recalculations
	 * @var type
	 */
	private $_totalCountCache=[];

	/**
	 * @inheritDoc
	 */
	protected function prepareTotalCount()
	{
		$filters = $this->queryBuilder->getFilter();
		$key = md5(serialize($filters));
		if (!isset($this->_totalCountCache[$key])) {
			// remove the need for this query!!

			$order = []; // no need to order for a count
			$results = neon()->getDds()->IDdsObjectManagement
				->runSingleObjectRequest($this->classType, $filters, $order, ['start'=>0, 'length'=>0, 'total' => true], $this->hasDeletedFilter());
			$this->_totalCountCache[$key] = $results['total'];
		}
		return $this->_totalCountCache[$key];
	}

	/**
	 * Checks if the deleted filter exists in the filter query (getQueryBuilder)
	 * @return boolean - true if the filtering on deleted rows
	 */
	public function hasDeletedFilter()
	{
		$includeDeleted = false;
		foreach ($this->queryBuilder->getFilter() as $filter) {
			if ($filter[0] === '_deleted') {
				$includeDeleted = true;
				break;
			}
		}
		return $includeDeleted;
	}

}