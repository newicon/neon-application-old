<?php

namespace neon\core\types;

/*
| ----------------------------
| Functions defining data flow
| ----------------------------
|
| These functions define how data flows form the user (typically a user http request) through to the database
| and from the database back to the user to be displayed
| A type is aware of three states for data it controls.  In all three cases its possible these are the same however
|
|      +---------------------+ Http  <-----------------------------+
|      |                                                           |
|      |                   +----------+                            |
|      +--setFromRequest-->|          |---->| Input Field |--------+
|                          |          |
|                          |          |
|                          |          |---->| Display | (context)
|                          |          |
|                          |          |
|                          |   Type   |---->| Process Filter |
|                          |          |
|                          |          |
|                          |          |---->| Filter Type |
|                          |          |
|                          |          |
|      +-----setFromDb---->|          |--getData()-->| Daedalus |--+
|      |                   +----------+                            |
|      |                                                           |
|      +-----------------------------------------------------------+
|
*/

/**
 * Types - application types
 */
interface IType
{
	public function setFromDb();

	public function setFromRequest();

	public function getForDb();

	public function getFilterInputField();

	public function renderField();

	public function renderView();

	public function getDisplayForSummary();

	public function getDisplayGrid();

	public function getDisplayEditable();

	public function appendFilter(IQuery $query, $searchData);

	public function getFilterField(); // IType
}