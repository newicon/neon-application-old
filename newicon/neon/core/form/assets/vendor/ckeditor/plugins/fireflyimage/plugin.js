/**
 * Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_intro
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'fireflyimage', {

	// Register the icons. They must match command names.
	icons: 'fireflyimage',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define the editor command that inserts a timestamp.
		editor.addCommand( 'showMediaBrowser', {

			// Define the function that will be fired when the command is executed.
			exec: function( editor ) {
				FIREFLY.picker('ckedit', function(item){
					editor.insertHtml('<img src="' + FIREFLY.getImageUrl(item.id) + '" />');
				});
			}
		});

		// Create the toolbar button that executes the above command.
		editor.ui.addButton( 'Fireflyimage', {
			label: 'Insert Image',
			command: 'showMediaBrowser',
			toolbar: 'insert'
		});
	}
});
