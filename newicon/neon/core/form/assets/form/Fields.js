/**
 * Created by newicon on 26/01/2018.
 */
(function() {

	Vue.component('neon-core-form-field', {
		inject: ['parentForm', 'rootForm'],
		props: {
			required: Boolean,
			errors: {type:[Array, Object], default: () => []},
			label: String,
			hint: {type: String},
			labelFor: String,
			inline: {type: Boolean, default: () => false},
			visible: {type: Boolean, default: true},
			attributes: {type: [Object,Array], default: function () { return {} }},
			showIf: {type: [Array, Object]},
			flags: {type:Object},
			placeholder: String,
			placeholderLabel: {type: Boolean, default: false}
		},
		template: `
			<div :class="[classes, attributes.class]" class="neonField">
				<transition name="neonFade">
					<div class="neonField_content" v-show="fieldShouldShow()">
						<label v-if="label" class="neonField_label control-label" :for="labelFor">{{ label }}</label>
						<div class="hint-block hint-top neonField_hint">
							<neon-markdown :markdown="hint"></neon-markdown>
						</div>
						<div class="neonInput">
							<slot></slot>
							<label v-if="placeholderLabel" class="neonInput_placeholderLabel">{{ placeholder ? placeholder : label }}</label>
							<div class="neonField_errors">
								<transition name="neonFade">
									<div class="neonField_error help-block" v-if="getFirstError() && (isSubmitted || field.flags.touched)">{{getFirstError()}}</div>
								</transition>
							</div>
						</div>
					</div>
				</transition>
			</div>
		`,
		computed: {
			classes() {
				var touched = this.isSubmitted || (_.isDefined(this.field.flags) && this.field.flags.touched);
				return {
					// Old styles to deprecate
					'has-error': this.hasError && touched,
					'form-group': !this.inline,
					'form-inline': this.inline,

					'isRequired' : this.required,
					'isError': this.hasError && touched,
					'isValid': !this.hasError,
					'isFocused': _.isDefined(this.field.flags) && this.field.flags.focused,
					'isTouched': touched,
					'isDirty': this.field.value && _.isDefined(this.field.flags) && this.field.flags.dirty,
					'niForm': this.placeholderLabel,
					'isFixedPlaceholder': this.placeholderLabel
				}
			},
			path: function() {
				return this.getPath();
			},
			field: {
				set(val) {
					//console.log(val, 'field set');
				},
				get() {
					var flags = this.$store.getters['FORMS/getField'](this.path);
					return flags || {};
				}
			},
			/**
			 * Whether the parent form has been submitted
			 * @returns {neon-core-form-field.computed.submitted|(function())|neon-core-form-fields-submit.computed.submitted|neon-core-form-form.computed.submitted|boolean}
			 */
			submitted: function() {
				return this.$store.getters['FORMS/FORM'](this.rootForm.id).submitted
			},
			/**
			 * Whether the form has been submitted in the past
			 */
			isSubmitted: function(){
				return this.$store.getters['FORMS/FORM'](this.rootForm.id).isSubmitted;
			},
			firstError: function() {
				return this.$store.getters['FORMS/getErrors'](this.path);
			},
			hasError: function() {
				return this.$store.getters['FORMS/hasErrors'](this.path);
			},
		},
		methods: {

			fieldShouldShow: function(showIf = this.showIf) {
				if (_.isEmpty(showIf))
					return true;

				let booleanOperators = [
					'and', 'or', '&&', '||', '!=', '!==', '=', '==', '==='
				];
				let key = _.findKey(showIf);
				if (_.includes(booleanOperators, key)) {
					let length = showIf[key].length || Object.keys(showIf[key]).length;
					if (length < 2) {
						console.error("A boolean expression requires at least two operands")
						return true;
					}

					let field = this;
					function evaluateOrStatement() {
						// keep checking terms - return true as soon as we hit a true statement for 'or'
						for (let i=0; i<length; i++) {
							if (field.fieldShouldShow(showIf[key][i]))
								return true;
						}
						return false;
					}
					function evaluateAndStatement() {
						// keep checking terms - return false as soon as we hit a false statement for 'and'
						for (let i=0; i<length; i++) {
							if (!field.fieldShouldShow(showIf[key][i]))
								return false;
						}
						return true;
					}
					switch (key) {
						case '&&': case 'and': {
							return evaluateAndStatement();
						}
						case '||': case 'or': {
							return evaluateOrStatement();
						}
						case '!=': case '!==': {
							let lhs = this.fieldShouldShow(showIf[key][0]);
							let rhs = this.fieldShouldShow(showIf[key][1]);
							return lhs !== rhs;
						}
						case '=': case '==': case '===': {
							let lhs = this.fieldShouldShow(showIf[key][0]);
							let rhs = this.fieldShouldShow(showIf[key][1]);
							return lhs === rhs
						}
						default: {
							return true;
						}
					}
				}

				if (!_.isString(showIf[0])) {
					console.error('The first part of the rule array must be a string path to an object a global or this.$props.item')
					return true;
				}

				var path = showIf[0];
				var operator = showIf[1];
				var value = showIf[2];
				var targetValue;

				if (path.substring(0,7) === 'window.') {
					targetValue = _.get(window, path.substring(7));
				} else if (path.substring(0,5) === 'this.') {
					targetValue = _.get(this, path.substring(5));
				} else if (path.indexOf('.') > -1) { //assume this means a fully qualified path - required eg for showIfs to work with appForms
					let pathParts = path.split('.');
					let root = this.rootForm.value;
					for (let i = 0; i < pathParts.length; i++) {
						if (_.isUndefined(root[pathParts[i]])) break;
						root = root[pathParts[i]];
						if (i + 1 === pathParts.length)
							targetValue = root;
					}
				} else {
					// assume we are checking form values on the immediate parent form.
					targetValue = _.get(this.parentForm.value, path);
				}
				return neon.filter(operator)(targetValue, value);
			},
			field: function () {
				var field = this.$store.getters['FORMS/getField'](this.path)
				return field || {};
			},
			getPath: function () {
				if (this.$parent.getPath)
					return this.$parent.getPath();
			},
			getFirstError: function() {
				return _.values(this.field.errors)[0];
			},
		},
	});


// region: Base
	var BaseField = {
		store: neon.Store,
		inject: ['parentForm', 'rootForm'],
		// separate props into a standard field props object to import in
		props: {
			name: {type: String, required: true, editor: false},
			label: {type: String, editor: {order:128, component: 'neon-core-form-fields-text', placeholder: 'Set the label for the field'}},
			hint: {type: String, editor: {order:256, component: 'neon-core-form-fields-markdown', editorOptions: {showGutter: false, highlightActiveLine: false}, label:'Guide Text', placeholder: 'Add additional guide text'}},
			placeholder: {type: String, editor: {order:384, component: 'neon-core-form-fields-text', placeholder: 'Add helper placeholder text'}},
			placeholderLabel: {type: Boolean, editor: {order:400}},
			value: {type: String, editor: {order:512, label: 'Default Value'}, default:''},
			required: {type: Boolean, editor: {order:640}},
			errors: {type: [Array, Object], editor: false },
			visible: {type: Boolean, default: true, editor:{order:768}},
			disabled: {type: Boolean, default: false, editor:{order:12800}},
			showIf: {type: [Array, Object], editor:{order: 12928} },
			inline: {type: Boolean, default: false, editor: false},
			readOnly: {type: Boolean, default: false, editor: false},
			printOnly: {type: Boolean, default: false, editor: false},
			validators: {type: [Array, Object], default: function() { return {}; }, editor:false },
			attributes: {type: [Array, Object], default: function() {return {class: ''}}, editor:false },
			validateOnBlur: {type: Boolean, default: true},
			enableAjaxValidation: {type: Boolean, default:null, editor:false}, // if not set gets property from parent

			flags: {
				type: Object, editor: false, default: function () {
					return {
						// true if the field is currently focused
						focused: false,
						// true if the field has previously been blurred
						touched: false,
						// dirty if the field value has been changed or the field has received input
						dirty: false,
					};
				}
			}
		},
		created: function() {
			this.$store.dispatch('FORMS/createFieldState', {path: this.getPath(), props: this.$props});
		},
		methods: {
			updateField(updateData) {
				return this.$store.commit('FORMS/UPDATE_FIELD', {path: this.getPath(), updates: updateData});
			},
			/**
			 * set the field flags
			 * @example this.setFlags({touched:true}) // make the field touched
			 * @param flags
			 */
			setFlags(flags) {
				this.updateField({flags: Object.assign({}, this.field.flags, flags)});
			},
			onFocus() {
				this.setFlags({focused: true});
				this.$emit('focus');
			},
			onBlur() {
				this.setFlags({focused: false,  touched: true});

				if (this.shouldValidateOnBlur) {
					this.validate();
				}
				this.$emit('blur');
			},
			getPath() {
				if (this.parentForm) {
					return this.parentForm.getPath() + '.' + this.name;
				} else {
					// no parent form
					console.warn('This does not have a parent form', this.name);
					return this.name;
				}
			},
			getData() {
				return this.value;
			},
			hasError() {
				return Object.values(this.errors).length;
			},
			validate() {
				this.$store.dispatch('FORMS/VALIDATE_FIELD', {path: this.getPath()});
				if (this.shouldAjaxValidate) {
					this.rootForm.ajaxValidate(this.getPath());
				}
			},
			/**
			 * Get the root form vue instance
			 * @returns {Object}
			 */
			getRootFormInstance() {
				return this.rootForm;
			}
		},
		computed: {
			shouldValidateOnBlur() {
				return this.validateOnBlur === null ? this.rootForm.validateOnBlur : this.validateOnBlur
			},
			shouldAjaxValidate() {
				return this.enableAjaxValidation === null ? this.rootForm.enableAjaxValidation : this.enableAjaxValidation
			},
			/**
			 * Whether the form field is valid
			 * @returns {boolean}
			 */
			isValid() {
				return Object.values(this.errors).length === 0;
			},
			/**
			 * Whether the root form is currently submitting a request
			 * @returns {neon-core-form-field.computed.submitted|(function(): (neon-core-form-field.computed.submitted|(function())|BaseField.computed.submitted|neon-core-form-form.computed.submitted|boolean))|BaseField.computed.submitted|(function())|neon-core-form-form.computed.submitted|boolean}
			 */
			submitted: function() {
				return this.$store.getters['FORMS/FORM'](this.rootForm.id).submitted
			},
			/**
			 * Whether the form has been submitted in the past
			 */
			isSubmitted: function(){
				return this.$store.getters['FORMS/FORM'](this.rootForm.id).isSubmitted;
			},
			path: function() {
				return this.getPath();
			},
			isReadOnly() {
				if (this.rootForm) {
					return this.readOnly || this.rootForm.readOnly;
				}
				return false;
			},
			isPrintOnly() {
				if (this.rootForm) {
					return this.printOnly || this.rootForm.printOnly;
				}
				return false;
			},
			/**
			 * Get the object representing properties of the field
			 * these include:
			 * - label
			 * - error
			 * - id
			 * - name
			 */
			fieldProps() {
				return {
					required: this.required,
					name: this.name,
					// computed prop
					labelFor: this.id,
					label: this.label,
					hint: this.hint,
					placeholder: this.placeholder,
					placeholderLabel: this.placeholderLabel,
					errors: this.errors,
					value: this.value,
					inline: this.inline,
					visible: this.visible,
					dirty: this.dirty,
					attributes: this.attributes,
					showIf: this.showIf,
					flags: this.flags,
				};
			},
			field() {
				return this.$store.getters['FORMS/getField'](this.path);
			},
			modelValue: {
				get() {
					if (_.isUndefined(this.field))
						return this.value;
					return this.field.value;
				},
				set(value) {
					if (!this.$store) this.$store = neon.Store;
					this.setFlags({dirty: true});
					neon.form.updateField(this.path, {value: value});
					neon.form.validateField(this.path);
					this.$emit('input', value);
					// raise an event on the root form, to allow hooking into the update
					this.rootForm.bubbleInput();
					this.rootForm.$emit('change',{path: this.path, name: this.name, value: value});
				}
			},
			/**
			 * @duplicate todo: remove duplicate function in field and form
			 * @returns String correct html input name string
			 */
			inputName() {
				var bits = this.path.split('.');
				// replace the first part of the path with the root forms name NOT its id
				// a path is rootForm.id then each fields name.
				bits.shift(); // remove the initial id
				return this.rootForm.name + '[' + bits.join('][') + ']';
			},
			id() {
				return this.path.replace(/\./g, '-');
			}
		}
	};

	Vue.component('neon-core-form-fields-base', BaseField);
// endregion

// region: Text
	Vue.component('neon-core-form-fields-text', {
		extends: BaseField,
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldText">
				<div :name="inputName" v-if="isPrintOnly">{{value}}</div>
				<input v-else @blur="onBlur()" @focus="onFocus()" v-model="modelValue" type="text" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readOnly="isReadOnly" :disabled="disabled" />
			</neon-core-form-field>
		`,
	});
// endregion

	// region: Text
	Vue.component('neon-core-form-fields-uuid64', {
		extends: BaseField,
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldText">
				<div :name="inputName" v-if="isPrintOnly">{{value}}</div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" type="text" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readOnly="isReadOnly" :disabled="disabled" />
			</neon-core-form-field>
		`,
	});
// endregion


	Vue.component('neon-core-form-fields-filters-file', Vue.component('neon-core-form-fields-text'));

// region: Text
/*	Vue.component('neon-core-form-fields-hidden', {
		extends: BaseField,
		template: `
			<neon-core-form-field v-if="!isPrintOnly" v-bind="fieldProps" >
				<input v-model="modelValue" type="text" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readonly="isReadOnly" />
			</neon-core-form-field>
		`,
	});*/
// endregion

// region: Textarea
	Vue.component('neon-core-form-fields-textarea', {
		extends: BaseField,
		_component:{
			title: 'Texarea'
		},
		props: {
			value: String
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldTextarea">
				<div v-if="isPrintOnly" v-nl2br="value"></div>
				<textarea v-else @focus="onFocus()" @blur="onBlur()"  v-model="modelValue" type="text" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readonly="isReadOnly" :disabled="disabled"></textarea>
			</neon-core-form-field>
		`
	});
// endregion

// region: Masked Input
	Vue.component('neon-core-form-fields-maskedinput', {
		extends: BaseField,
		props: {
			value: String,
			delimiter: {type: String},
			prefix: {type: String},
			blocks: {type: Object, default: function() { return {}; }}
		},
		data: function () {
			 return { cleave : null};
		},
		methods: {
			update: function () {
				this.modelValue = this.cleave.getRawValue();
			}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps"  class="neonFieldText">
				<div v-if="isPrintOnly">{{value}}</div>
				<input v-else @blur="validate()" type="text" class="form-control" :placeholder="placeholder" :readOnly="isReadOnly"
				:disabled="disabled" v-on:change="update()" :id="id"  />
				<input type="hidden" :name="inputName" :value="modelValue" />
			</neon-core-form-field>
		`,
		mounted: function() {
			if (this.cleave)
				return;
			let blockValues = [];
			for (let key in this.blocks) {
				blockValues.push(this.blocks[key]);
			}
			this.cleave = new Cleave('#' + this.id, {delimiter: this.delimiter, blocks: blockValues, prefix: this.prefix});
		}
	});
// endregion

// region: url
	Vue.component('neon-core-form-fields-urllink', {
		extends: BaseField,
		props: {
			absolute: { type: Boolean, editor: {order:420, label:'Absolute Link'} }
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldUrl">
				<div :name="inputName" v-if="isPrintOnly"><a :href='value' target='_blank' noreferrer noopener>{{value}}</a></div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" type="text" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readOnly="isReadOnly" :disabled="disabled" />
			</neon-core-form-field>
		`,
	});
// endregion

// region: Markdown
	Vue.component('neon-core-form-fields-markdown', {
		extends: BaseField,
		props: {
			value: {type: String, default: '' },
			editorOptions: {type: Object, editor: {name: "editorOptions", label: 'Ace editor config options', hint: 'An object defining various options for the ace editor: https://github.com/ajaxorg/ace/wiki/Configuring-Ace'}}
		},
		created: function() {
			if (_.isNull(this.value)) {
				this.modelValue = '';
			}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldMarkdown">
				<div v-if="isPrintOnly"><neon-markdown :markdown="value"></neon-markdown></pre></div>
				<div v-else>
					<neon-input-ide v-model="modelValue" lang="markdown" :options="editorOptions"></neon-input-ide>
					<textarea :value="modelValue" class="hidden" :name="inputName" :id="id" :readonly="isReadOnly" :disabled="disabled"></textarea>
				</div>
			</neon-core-form-field>
		`,
	});
// endregion

// region: Json
	Vue.component('neon-core-form-fields-json', {
		extends: BaseField,
		props: {
			parseValue: {type:Boolean, default:false},
			value: [String, Object, Array, Number],
			editorOptions: {type: [Object,String], editor: {name: "editorOptions", label: 'Ace editor config options', hint: 'An object defining various options for the ace editor: https://github.com/ajaxorg/ace/wiki/Configuring-Ace'}}
		},
		data: function() {
			return {
				editor: Object,
				contentBackup: ''
			}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldJson">
				<div v-if="isPrintOnly"><pre>{{value}}</pre></div>
				<div v-else>
					<neon-input-ide v-model="jsonValue" lang="json" :options="editorOptions"></neon-input-ide>
					<textarea :value="jsonValue" class="hidden" :name="inputName" :id="id" :readonly="isReadOnly"></textarea>
				</div>
			</neon-core-form-field>
		`,
		mounted() {
			// atempt to parse value
			try {
				if (!_.isString(this.value)) return;
				var json = JSON.parse(this.value);
				this.jsonValue = json;
			} catch (error) {
				// we don't mind - the string value could not be valid json - however we still want to display it
			}
		},
		watch: {
			value: function(newVal) {
				this.jsonValue = newVal
			}
		},
		computed: {
			// this computed property converts the ide string representation back to json
			// and converts json back to string for use in the editor
			jsonValue: {
				get: function() {
					var v = this.modelValue;
					if (!_.isObject(v) && _.isEmpty(v))
						return '';
					if (_.isString(v))
						return v;
					if (_.isObject(v))
						return JSON.stringify(v, null, "\t");
					console.error('Unknown format');
				},
				set: function(value) {
					try {
						if (this.parseValue) {
							this.modelValue = _.isString(value)
								? (_.isEmpty(value) ? '' : JSON.parse(value))
								: value;
						} else {
							this.modelValue = value;
						}
					} catch(err) {
						console.error('Json parse failed', err);
						return;
					}
				}
			}
		}
	});

// endregion

// region: Ide
	Vue.component('neon-core-form-fields-ide', {
		extends: BaseField,
		props: {
			value: String
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldIde">
				<div v-if="isPrintOnly"><pre>{{value}}</pre></div>
				<div v-else>
					<div :id="id+'-editor'">{{value}}</div>
					<textarea v-model="modelValue" class="hidden" :name="inputName" :id="id" :readonly="isReadOnly"></textarea>
				</div>
			</neon-core-form-field>
		`,
		mounted: function() {
			let textarea = $("#"+this.id);
			let editor = ace.edit(this.id + '-editor');
			//editor.setTheme("ace/theme/monokai");
			editor.getSession().setMode("ace/mode/php");
			editor.getSession().setTabSize(4);
			editor.getSession().setLines
			editor.getSession().on('change', function () {
				textarea.val(editor.getSession().getValue());
			});
			editor.setOptions({
				autoScrollEditorIntoView: true,
				maxLines: 30,
				minLines: 1
			});
			editor.renderer.setScrollMargin(10, 10, 10, 10);
		}
	});
// endregion

// region: Password
	Vue.component('neon-core-form-fields-password', {
		extends: BaseField,
		props: {
			value: String,
			checkPasswordDatabases: {type: Boolean, default: true}
		},
		data: function() {
			return {
				showPassword: false,
				hackedTimes: null,
				factor: 0
			};
		},
		template: `
			<neon-core-form-field v-if="!isPrintOnly" v-bind="fieldProps" class="neonFieldPassword">
				<div class="has-feedback">
					<span title="Toggle password visibility" @click="showPassword = !showPassword" class="form-control-feedback" style="pointer-events:all; cursor:pointer; top: 50%; transform: translateY(-50%);"><i class="fa" :class="showPassword ? 'fa-eye' : 'fa-eye-slash'"></i></span>
					<input @blur="onBlur()" @focus="onFocus()" v-model="modelValue" :type="showPassword ? 'text' : 'password'" class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readonly="isReadOnly"/>
				</div>
				<el-progress style="margin: 0px 1px 5px 1px;" :percentage="strengthDetails.percent" :color="strengthDetails.color" :show-text="false"></el-progress>
				<div v-show="flags.touched && flags.dirty" class="hint-block">{{length ?  'Password strength: ' + strengthDetails.label : ''}}</div>
				<div v-show="flags.touched && flags.dirty" class="hint-block" v-if="minLength">Your password must be at least <strong>{{minLength}}</strong> characters long. It is currently {{length}} characters.</div>
				<div v-show="flags.touched && flags.dirty" class="hint-block" v-if="hackedTimes != null && checkPasswordDatabases">
					<div v-if="hackedTimes == 'loading'"><i class=" neonSpinner el-icon-loading"></i> Checking...</div>
					<div v-if="hackedTimes == 0 && length > 0" class=" "><i class="fa fa-check"></i> This password has not been compromised.</div>
					<div v-if="hackedTimes > 0" class="" ><i class="fa fa-exclamation"></i> Oh no! This password was found <strong>{{hackedTimes}}</strong> times in compromised passwords databases!</div>
				</div>
			</neon-core-form-field>
		`,
		watch: {
			'value': function(val) {
				this.debouncedPasswordCrackCheck();
			}
		},
		created() {
			var vm = this;
			this.debouncedPasswordCrackCheck = _.debounce(_.throttle(function() {
				return vm.passwordCrackCheck();
			}, 1500, {leading:true}), 200);
		},
		methods: {
			passwordCrackCheck: function() {
				if (!this.checkPasswordDatabases)
					return;
				this.hackedTimes = 'loading';
				var vm = this;
				neon.hibpPasswordCheck(this.value).then(function(timesComprimised) {
					vm.hackedTimes = timesComprimised;
				})
			}
		},
		computed: {
			length: function() {
				return this.value ? this.value.length : 0;
			},
			strength:  function() {
				var length = this.length;
				var lower = /[a-z]/, upper = /[A-Z]/, number = /[0-9]/, special = /[^a-zA-Z0-9]/;
				this.factor = 0;
				if (lower.test(this.value)) this.factor += 26;
				if (upper.test(this.value)) this.factor += 26;
				if (number.test(this.value)) this.factor += 10;
				if (special.test(this.value)) this.factor += 30; // approximation but represents most common special characters
				var strength = Math.pow(this.factor, length);
				return strength ? strength : 0;
			},
			/**
			 * This attempts to approximate how long it would take to crack a password
			 * it does this by simply dividing the number of hashing algorithms a computer
			 * can run per second by the strength of the password, where the strength is given by
			 * the total number of combinations.
			 */
			calculateTimeToCrack: function() {},
			strengthDetails: function() {
				var bad   = 10e+8;
				var weak   = 10e+10;
				var medium = 10e+14; // quintillion
				var high   = 10e+21; // Sextillion
				var strong = 10e+24; // Septillion
				var veryStrong = 10e+30; // Nonillion
				var googol = 10e+100; // Googol - google :-)

				var percent = (this.length * this.factor / 1200) * 100;
				percent = (percent < 100) ? percent : 100;
				var hue = (((percent/100))*120).toString(10);
				var color = ["hsl(",hue,",100%,40%)"].join("");
				var label = (function(strength) {
					if (strength <= bad) return 'Bad';
					if (strength < weak) return 'Bad';
					if (strength < medium) return 'Weak';
					if (strength < high) return 'Medium';
					if (strength < strong) return 'High';
					if (strength < veryStrong) return 'Strong';
					if (strength < googol) return 'Very Strong';
					if (strength > googol) return 'Googoliciously Strong!';
					return 'unknown';
				})(this.strength);
				return {label:label, percent:percent, color:color};
			},
			/**
			 * Attempts to find the min password length from the validator
			 * @returns {Number}
			 */
			minLength: function() {
				// find the string validator
				// - it cuts to root of how validators are managed which is a little messy
				var stringValidator = _.find(this.validators, function(v) {
					var c = v.component || v.class;
					return c == 'neon\\core\\validators\\StringValidator' || c == 'string';
				});
				return _.get(stringValidator,'min');
			}
		}
	});
// endregion

// region: Hidden
	Vue.component('neon-core-form-fields-hidden', {
		extends: BaseField,
		props: {
			value: String
		},
		template: `
			<input v-if="!isPrintOnly" v-model="modelValue" type="hidden" :name="inputName" :id="id" :readonly="isReadOnly" />
		`,
	});
// endregion

// region: Color
	Vue.component('neon-core-form-fields-color', {
		extends: BaseField,
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldColor">
				<div v-if="isPrintOnly" :style="{backgroundColor: value, height:'1em', width:'5em;'}"></div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" type="color" :name="inputName" :id="id" :placeholder="placeholder" :readonly="isReadOnly" />
			</neon-core-form-field>
		`,
	});
// endregion

// region: Switchbutton
	Vue.component('neon-core-form-fields-switchbutton', {
		extends: BaseField,
		props: {
			value: [Boolean, String, Number],
			trueLabel: String,
			falseLabel: String
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSwitch">
				<div v-if="(isPrintOnly || isReadOnly)">
					<span v-if="value==true">{{trueLabel}}</span>
					<span v-else>{{falseLabel}}</span>
				</div>
				<neon-switch-input v-else v-model="modelValue" v-bind="$props" :name="inputName" :readOnly="isReadOnly"></neon-switch-input>
			</neon-core-form-field>
		`,
	});
// endregion

// region: SwitchMultipleState

	Vue.component('neon-core-form-fields-switchmultiplestate', {
		extends: BaseField,
		props: {
			items: {type: Object, default: function() {return {}}, editor:{component: 'form-builder-field-form-element-list-items'} },
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSwitchMultipleState">
				<div v-if="isPrintOnly">
					{{ value }}
				</div>
				<div v-else class="neonFieldSwitchMultipleState_radio" v-for="(value, key) in items">
					<label class="neonFieldSwitchMultipleState_label">
						<input class="neonFieldSwitchMultipleState_radioInput" v-model="modelValue" :name="inputName" type="radio" :value="key" :readOnly="isReadOnly" @change="validate"/>
						<span class="neonSwitchMultipleState_radioSpan">{{ value }}</span>
					</label>
				</div>
			</neon-core-form-field>
		`
	});

// endregion

// region: Image
	Vue.component('neon-core-form-fields-image', {
		extends: BaseField,
		props: {
			value: String,
			startPath: {type:String, editor: { "label": 'Browser Start Path', hint: 'The folder location the browser will open at'}, default: '/'},
			crop: {type:Boolean, default:false, editor: {"label": 'Aspect Ratio'}},
			cropWidth: {type:Number, default: 200, editor:{
					label: 'Width',
					hint: 'Set the aspect ratio, if the uploaded image does not fit the aspect ratio given by width x height the cropper will appear',
					showIf:  ['crop', '==', true]
				}},
			cropHeight: {type:Number, default: 200, editor:{ "label": 'Height', showIf: ['crop', '==', true]  }}
		},
		template: `
			<neon-core-form-field v-bind="$props" class="neonFieldImage">
				<firefly-form-image
					v-model="modelValue"
					:name="inputName"
					:startPath="startPath"
					:crop="crop"
					:crop-width="cropWidth"
					:crop-height="cropHeight"
					:read-only="isReadOnly">
				</firefly-form-image>
			</neon-core-form-field>
		`,
	});
// endregion

// region: Image
	Vue.component('neon-core-form-fields-image', {
		extends: BaseField,
		props: {
			value: String,
			startPath: {type: String, default: '/'},
			crop: {type:Boolean, default:false, editor: {"label": 'Aspect Ratio'}},
			cropWidth: {type:Number, default: 200, editor:{
				label: 'Width',
				hint: 'Set the aspect ratio, if the uploaded image does not fit the aspect ratio given by width x height the cropper will appear',
				showIf:  ['crop', '==', true]
			}},
			cropHeight: {type:Number, default: 200, editor:{ "label": 'Height', showIf: ['crop', '==', true]  }}
		},
		template: `
			<neon-core-form-field v-bind="$props" class="neonFieldImage">
				<firefly-form-image
					v-model="modelValue"
					:name="inputName"
					:startPath="startPath"
					:crop="crop"
					:crop-width="cropWidth"
					:crop-height="cropHeight"
					:read-only="isReadOnly"
					@on-error="onError($event)"
					@click="launchedPicker()">
				</firefly-form-image>
			</neon-core-form-field>
		`,
		methods: {
			launchedPicker: function() {
				this.setFlags({focused: true,  touched: true});
			},
			onError: function($event) {
				var errors = {};
				errors[$event.error.code] = $event.error.message;
				this.$store.commit('FORMS/UPDATE_FIELD', {
					path: this.getPath(),
					updates: {errors: errors}
				});
			}
		}
	});
// endregion

// region: Email
	Vue.component('neon-core-form-fields-email', {
		extends: BaseField,
		props: {
			value: String
		},
		template:`
			<neon-core-form-field v-bind="fieldProps" class="neonFieldEmail">
				<div v-if="isPrintOnly">{{value}}</div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" type="text" :id="id" :name="inputName" class="form-control" :placeholder="placeholder" :readonly="isReadOnly" />
			</neon-core-form-field>
		`
	});
// endregion

// region: Submit
	Vue.component('neon-core-form-fields-submit', {
		extends: BaseField,
		props: {
			/**
			 * The button label text shown to the user when the form is submitted and the button is disabled
			 */
			submittingLabel: {type:String, default:'Submitting...', editor: {"hint": 'The button label text shown to the user when the form is submitted and the button is disabled'}},
			value: {type: [String, Boolean]}
		},
		data: function (){
			return {loading:true};
		},
		template: `
			<neon-core-form-field v-if="!isPrintOnly" :inline="inline" class="neonFieldSubmit">
				<button :name="inputName" @click="modelValue=true" type="submit" :disabled="submitted"  class="neonButton btn btn-primary" :class="attributes.class" :id="id">
					<!--<i class="el-icon-check" v-if="!submitted"></i>-->
					<i class="el-icon-loading" v-if="submitted"></i>
					<template v-if="submitted">{{submittingLabel}}</template>
					<template v-else><slot>{{label ? label : name}}</slot></template>
				</button>
				<span class="text-danger" v-show="hasErrors"> Please fix the issues with the form to submit</span>
			</neon-core-form-field>
		`,
		computed: {
			hasErrors: function() {
				var rootFormId = this.getPath().split('.')[0];
				return this.$store.getters['FORMS/hasErrors'](rootFormId);
			}
		}
	});
// endregion

// region: Checkbox
	Vue.component('neon-core-form-fields-checkbox', {
		extends: BaseField,
		props: {
			value: [String, Boolean, Number],
			checkboxLabel: {type: String, editor: {component: 'neon-core-form-fields-wysiwyg', config: 'simple'}}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldCheckbox">
				<div class="checkbox">
					<input type="hidden" :name="inputName" value="0" />
					<label><input @change="validate()" v-model="modelValue" type="checkbox" :name="inputName" :id="id" :readonly="isReadOnly || isPrintOnly" /><span class="neonCheckbox_label" v-html="checkboxLabel"></span></label>
				</div>
			</neon-core-form-field>
		`,
	});
// endregion

// region link
	Vue.component('neon-core-form-fields-link',  {
		extends: BaseField,
		props: {
			multiple: {type: Boolean, default: true},
			items: {type: Object, editor: false},
			// the value may be an array of uuids,
			// or an object of a serialised associative array - the object keys are ignored
			// can also receive an empty string for no data
			value: [Array, Object, String],
			/**
			 * The data map provider for e.g. "cms" or "user" this typically assumes a neon application
			 * accessible via neon($dataMapProvider) neon applications implement the IFormBuilderInterface
			 * which should probably be renamed to IDataProviderMap
			 */
			dataMapProvider: {type: String, editor:{component:'form-builder-field-form-dynamic-map'}},
			/**
			 * Specify the key
			 */
			dataMapKey: {type: String, editor: false},
			endpoint: {type: String, default: '/core/form/get-map-objects', editor: false},
		},
		data: function() {
			return {selectBoxItems: this.items };
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldLink">
				<neon-select :load="load" @focus="onFocus()" @blur="onBlur()" :name="inputName" :multiple="true" v-model="modelValue" :items="selectBoxItems" :placeholder="placeholder" :readOnly="isReadOnly" :printOnly="isPrintOnly" ></neon-select>
			</neon-core-form-field>
		`,
		watch: {
			dataMapKey: function (val) {
				if (val === undefined || val == null) {
					this.selectBoxItems = {};
				} else {
					this.modelValue = null;
					this.load('');
				}
			}
		},
		methods: {
			load: function(query, callback) {
				var url = neon.url(this.endpoint, {objectToken: this.rootForm.objectToken, formField: this.getPath(), query:query});
				$.getJSON(url, {}, (data) => {
					if (callback) callback(data);
					this.selectBoxItems = data;
				});
			}
		}
	});


// region: Selectdynamic
	Vue.component('neon-core-form-fields-selectdynamic', {
		extends: BaseField,
		props: {
			items: {type: [Object, Array], default: () => {return {};}},
			/**
			 * The data map provider for e.g. "cms" or "user" this typically assumes a neon application
			 * accessible via neon($dataMapProvider) neon applications implement the IFormBuilderInterface
			 * which should probably be renamed to IDataProviderMap
			 */
			dataMapProvider: {type: String, editor:{component:'form-builder-field-form-dynamic-map'}},
			/**
			 * Specify the key
			 */
			dataMapKey: {type: String, editor: false},
			endpoint: {type: String, default: '/core/form/get-map-objects'},
			create:  {type: Boolean, default: false}
		},
		data() {
			return {
				selectBoxItems: this.items
			};
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSelectdynamic">
				<neon-select @focus="onFocus()" @blur="onBlur()" :id="id" v-model="modelValue" :load="load" :name="inputName" :items="selectBoxItems" :placeholder="placeholder" :readOnly="isReadOnly" :printOnly="isPrintOnly" :create="create ? doCreate : false"></neon-select>
			</neon-core-form-field>
		`,
		methods: {
			// if we can create then we return the function that does the creation -
			// this is in format asynchronous
			// https://github.com/selectize/selectize.js/blob/master/docs/usage.md - see create
			doCreate: function(input, callback) {
				let vm = this;
				let popFormModalId = neon.uuid64();
				let popFormId = popFormModalId+'PopForm';
				let savedNewItem = false;
				neon.modal.show(
					{
						props: {title:'', message:'', popFormId:''},
						template:`
						<div style="padding:10px" v-if="form">
							<h4 style="padding:10px;">Add new {{form ? form.label : ''}}</h4> <hr/>
							<neon-core-form-form v-bind="form" @mounted="formCreated"  @afterSubmit="afterSubmit" :id="popFormId" :enableAjaxSubmission="true" ></neon-core-form-form>
						</div>
						`,
						data: function() {
							return {form:null}
						},
						methods: {
							formCreated: function(formInstance) {
								window.myform = formInstance;
							},
							afterSubmit: function(event) {
								if (event.success) {
									vm.selectBoxItems = event.response;
									let value = _.keys(event.response);
									let text = _.values(event.response);
									savedNewItem = true;
									vm.$nextTick(function() {
										callback({value: value, text: text});
									});
									neon.modal.hide(popFormModalId);
								}
							}
						},
						mounted() {
							this.enableAjaxSubmission = true;
							let modalVm = this;
							$.ajax(neon.url('/daedalus/index/get-form-definition', {'type':vm.dataMapKey})).then(function(data) {
								modalVm.form = data;
								// Create dynamically
								// var form = new Vue.extend(Vue.component('neon-core-form-form'));
								// var formInstance = new form({propsData: { name: data.name,  fields: data.fields}  });
							});
						}
					},
					{popFormId:popFormId},
					{
						name: popFormModalId,
						height:'80%',
						width:'80%',
						buttons: [
							{title: 'Close'},
							{
								title:'Save',
								handler: function() {
									// submit the form
									neon.form.forms[popFormId].submit();
									// document.getElementById(popFormId).submit();
									return false;
								}
							}
						]
					},
					{
						'before-close': function(event) {
							if (!savedNewItem) {
								// return the focus back to the select box and allow it to continue
								// if this is not called the select box will be in a permanent disabled state
								callback();
							}
						}
					}
				);
			},
			load: function(query, callback) {
				var url = neon.url(this.endpoint, {objectToken: this.rootForm.objectToken, formField: this.getPath(), query:query});
				$.getJSON(url, {}, (data) => {
					if (callback) callback(data);
					this.selectBoxItems = data;
				});
			}
		},
		watch: {
			dataMapKey: function(val) {
				if (val === undefined || val == null) {
					this.selectBoxItems = {};
				} else {
					this.modelValue = null;
					this.load('');
				}
			}
		}
	});
// endregion

	Vue.component('neon-core-form-fields-selectmultiple',  {
		extends: BaseField,
		props: {
			multiple: true,
			// the value may be an array of uuids,
			// or an object of a serialised associative array - the object keys are ignored
			// can also receive an empty string for no data
			value: [Array, Object, String],
			items: {type: Object, default: function() {return {};}, editor:{component: 'form-builder-field-form-element-list-items'} },
			/**
			 * Whether new options can be created from the typed in input
			 */
			create: {type: [Function, Boolean], default: false}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSelectmultiple">
				<neon-select @focus="onFocus()" @blur="onBlur()" :name="inputName" v-model="modelValue" :multiple="true" :items="items" :placeholder="placeholder" :readOnly="isReadOnly" :create="create" :printOnly="isPrintOnly" ></neon-select>
			</neon-core-form-field>
		`
	});

// region radio
	Vue.component('neon-core-form-fields-radio', {
		extends: BaseField,
		props: {
			items: {type: Object, default: function() {return {};}, editor:{component: 'form-builder-field-form-element-list-items'} }
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldRadio">
				<div class="radio" v-for="(value, key) in items">
					<label>
						<input v-model="modelValue" :name="inputName" type="radio" :value="key" :readOnly="isReadOnly" :disabled="isPrintOnly" @change="validate"/>
						<span class="neonRadio_radioText">{{ value }}</span>
					</label>
				</div>
			</neon-core-form-field>
		`
	});
// endregion

// region: Selectpicker
	Vue.component('neon-core-form-fields-selectpicker', {
		extends: BaseField,
		props: {
			value: [Array, Object],
			items: {type: [Object, Array], default: () => {return {}}},
			dataMapProvider: {type: String, editor:{component:'form-builder-field-form-dynamic-map'}},
			dataMapKey: {type: String, editor: false},
			searchString: String,
			selectedItemsListLabel: String
		},
		created: function() {
			if (_.isObject(this.value)) {
				// When a definition is serialised arrays can get turned to objects.
				// normally this is preferable, a checklist however works only with arrays
				this.modelValue = Object.values(this.value);
			}
		},
		data: function() {
			return {
				search: ''
			};
		},
		template: `
			<neon-core-form-field v-bind="fieldProps">
				<div class="neonFieldSelectpicker">
					<neon-select style="display:none" :multiple="true" :id="id" v-model="modelValue" :name="inputName" :items="items" :placeholder="placeholder" :readOnly="true" :printOnly="isPrintOnly"></neon-select>
					<div class="neonFieldSelectpicker_unselectedItems">
						<input class="form-control" v-model="search" type="text" :visible="isReadOnly" v-if="modelValue" :placeholder="searchPlaceholder"/>
						<ul class="neonFieldSelectpicker_unselectedItemsList" v-show="_.keys(itemsMatchingSearch).length">
							<li class="neonFieldSelectpicker_unselectedItem" v-for="(value, key) in itemsMatchingSearch" v-on:click="selectItem(key)"> <span v-html="value"></span> <i class="fa fa-check"></i></li>
						</ul>
					</div>
					<div class="neonFieldSelectpicker_selectedItems">
						<div class="neonFieldSelectpicker_selectedItemsLabel" v-html="selectedItemsListLabel" />
						<ul class="neonFieldSelectpicker_selectedItemsList" v-show="_.keys(selectedItems).length">
							<li class="neonFieldSelectpicker_selectedItem" v-for="(value, key) in selectedItems" v-on:click="deselectItem(key)">{{ value }} <i class="fa fa-times"></i></li>
						</ul>
					</div>
				</div>
			</neon-core-form-field>
		`,
		computed: {

			// Map the keys from modelValue (which contains the selected item keys)
			//  and the items themselves (which contain the key => value) to get an
			//  array of active item values
			selectedItems: function() {
				let selectedItems = {};
				for (const key in this.modelValue) {
					const itemKey = this.modelValue[key];
					selectedItems[itemKey] = this.items[itemKey];
				}
				return selectedItems;
			},
			// Map the difference in keys between the current modelValue and the component items,
			//  and return the items matching only those keys
			unselectedItems: function() {
				let unselectedItems = {};
				const unselectedKeys = _.difference(Object.keys(this.items), this.modelValue);
				for (const key in unselectedKeys) {
					const itemKey = unselectedKeys[key];
					unselectedItems[itemKey] = this.items[itemKey];
				}
				return unselectedItems;
			},
			itemsMatchingSearch: function() {
				if (!this.search)
					return this.unselectedItems;
				let matches = {};
				for (const key in this.unselectedItems) {
					if (_.includes(_.lowerCase(this.unselectedItems[key]), _.lowerCase(this.search))) {
						const regex = new RegExp(this.search,"ig");
						matches[key] = _.replace(this.unselectedItems[key], regex, function(match) {
							// todo: potential xss
							return '<span class="neonBgHighlight" style="background-color:#ccc;">'+match+'</span>';
						});
					}
				}
				return matches;
			},
			searchPlaceholder: function() {
				if (this.placeholder)
					return this.placeholder;
				return "Search for "+_.lowerCase(this.dataMapKey);
			}
		},
		methods: {
			// Add the given item to the field value
			selectItem: function(item) {
				var items = _.clone(this.modelValue);
				items.push(item);
				this.modelValue = items;
			},
			// Remove the given item from the field value
			deselectItem: function(item) {
				let value = [...this.modelValue];
				_.remove(value, function(n) {
					return n === item;
				});
				this.modelValue = value;
			}
		}
	});
// endregion

// region: Checklist
	Vue.component('neon-core-form-fields-checklist', {
		extends: BaseField,
		props: {
			value: {type: [Array, Object], default: function() { return []; } },
			items: {type: Object, default: function() { return {} }, editor:{component: 'form-builder-field-form-element-list-items'} }
		},
		created: function() {
			if (_.isObject(this.value)) {
				// When a definition is serialised arrays can get turned to objects.
				// normally this is preferable, a checklist however works only with arrays
				this.$store.commit('FORMS/UPDATE_FIELD', {
					path: this.getPath(),
					updates: {value: Object.values(this.value)}
				});
			}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldChecklist">
				<div class="checkbox" v-for="(value, key) in items">
					<label>
						<input v-model="modelValue" :name="inputName+'[]'" type="checkbox" :value="key" :readOnly="isReadOnly" :disabled="isPrintOnly"  /><span class="neonCheckbox_label">{{ value }}</span></label>
					</label>
				</div>
			</neon-core-form-field>
		`
	});
// endregion

// region Select
	Vue.component('neon-core-form-fields-select', {
		extends: BaseField,
		props: {
			value: {type: [String, Number]},
			items: {type: Object, default: function() {return {};}, editor:{component: 'form-builder-field-form-element-list-items'} },
			allowClear: {type: Boolean, default: true},
			create: {type: Boolean, default: false, editor: {hint: 'Allow users to create new options based on what they have typed'}}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSelect">
				<neon-select @focus="onFocus()" @blur="onBlur()" :name="inputName" :id="id" v-model="modelValue" :items="items" :placeholder="placeholder" :readOnly="isReadOnly" :printOnly="isPrintOnly" :allow-clear="allowClear" :create="create"></neon-select>
			</neon-core-form-field>
		`
	});
// endregion

// region SelectChain
	Vue.component('neon-core-form-fields-selectchain', {
		extends: BaseField,
		props: {
			chainValues: {
				type: Object,
				default: function() { return []; }
			},
			endPoint: {
				type: String,
				default: function() { return '';},
				editor: {
					component: 'neon-core-form-fields-text'
				}
			},
			classMemberMap: {
				type: Object,
				default: function() {
					return {};
				},
				editor: {
					component: 'neon-core-form-fields-json'
				}
			},
			allowClear: {type: Boolean, default: true}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldSelect">
				<neon-select-chain v-bind="$props" @focus="onFocus()" @blur="onBlur()" :id="id" :placeholder="placeholder" :inputName="inputName" v-model="modelValue" :readOnly="isReadOnly" :printOnly="isPrintOnly" :allow-clear="allowClear"></neon-select-chain>
			</neon-core-form-field>
		`,
	});
// endregion

// region: AppFormClass Selector
	Vue.component('neon-phoebe-form-fields-appformclassselector', {
		extends: Vue.component('neon-core-form-fields-selectdynamic')
	});
// endregion

// region: AppFormObject Selector
	Vue.component('neon-phoebe-form-fields-appformobjectselector', {
		extends: Vue.component('neon-core-form-fields-selectdynamic')
	});
// endregion

// region: Pageselector
	Vue.component('neon-cms-form-fields-pageselector', {
		extends: Vue.component('neon-core-form-fields-selectdynamic'),
	});
// endregion

// region: Currency
	Vue.component('neon-core-form-fields-currency', {
		extends: BaseField,
		props: {
			value: [Number,String]
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldCurrency">
				<div v-if="isPrintOnly">{{value}}</div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" :name="inputName" :id="id" type="number" step="0.01" class="form-control" :placeholder="placeholder" :readOnly="isReadOnly"/>
			</neon-core-form-field>
		`
	});
// endregion

// region: FileBrowser
	Vue.component('neon-core-form-fields-filebrowser', {
		extends: BaseField,
		props: {
			value: String,
			startPath: {type: String, editor: {label: 'Browser Start Path', hint: 'The folder location the browser will open at'}, default: '/'}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldFilebrowser">
				<firefly-form-file-browser v-model="modelValue" :name="inputName" :id="id" :readOnly="isReadOnly || isPrintOnly" :startPath="startPath"></firefly-form-file-browser>
			</neon-core-form-field>
		`
	});
// endregion

// region: File Single
	Vue.component('neon-core-form-fields-file', {
		extends: BaseField,
		props: {
			value: {type: [String, Array, Object]},
			multiple: {type: Boolean, editor:false, default:false},
			startPath: {type: String},
			urlUpload: {type: String},
			urlDownload: {type: String},
			urlMeta: {type: String},
			uploading: {type: Boolean, editor: false}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldFile">
				<firefly-form-input-file-upload @input="validate()" @uploading="onUploading" v-model="modelValue" :id="id" v-bind="$props" :name="inputName" :readOnly="isReadOnly" :printOnly="isPrintOnly" :multiple="multiple"></firefly-form-input-file-upload>
			</neon-core-form-field>
		`,
		mounted: function() {
			var vm = this;
			this.rootForm.onBeforeSubmit(function (event) {
				if (vm.uploading === true) {
					event.preventDefault();
					vm.$store.commit('FORMS/UPDATE_FIELD', {
						path: vm.getPath(),
						updates: {errors: {uploading: 'All uploads must be complete before you can submit the form' } }
					});
				}
			});
		},
		methods: {
			onUploading: function(value) {
				this.$store.commit('FORMS/UPDATE_FIELD', {
					path: this.getPath(),
					updates: {uploading: value}
				});
			}
		}
	});
// endregion
//
// region: File Multiple
	Vue.component('neon-core-form-fields-filemultiple', Vue.component('neon-core-form-fields-file'));
// endregion

// region: Integer
	Vue.component('neon-core-form-fields-integer', {
		extends: BaseField,
		props: {
			value: [String, Number]
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldInteger">
				<div v-if="isPrintOnly">{{value}}</div>
				<input v-else @focus="onFocus()" @blur="onBlur()" v-model="modelValue" :name="inputName" :id="id" type="number" class="form-control" :readOnly="isReadOnly" />
			</neon-core-form-field>
		`
	});
// endregion

	/**
	 * neon-time-input
	 */
	Vue.component('neon-core-form-fields-time', Vue.component('neon-core-form-fields-base').extend({
		props: {
			value: String,
			fieldType: {
				type: String,
				default: 'input',
				validator: function (value) {
					return ['input', 'select',].indexOf(value) !== -1;
				},
				editor: {component:'neon-core-form-fields-radio', label:'Type', items:{input:'input', 'select':'select'}}
			},
			/**
			 * selectSeconds - {Boolean} whether or not we want to show a seconds input
			 */
			showSeconds: {type: Boolean, default: false},
			inline: {type: Boolean, default: false}
		},
		template:`
			<neon-core-form-field v-bind="fieldProps" :inline="inline" :labelFor="id+'-hh'"   class="neonFieldTime">
				<div v-if="isPrintOnly">{{value}}</div>
				<neon-input-time v-else v-model="modelValue" :name="inputName" :field-type="fieldType" :show-seconds="showSeconds" :readOnly="isReadOnly"></neon-input-time>
			</neon-core-form-field>
		`,
	}));

	/**
	 * @deprecated use neon-core-form-fields-date
	 */
	Vue.component('neon-core-form-fields-date', {
		extends: BaseField,
		props: {
			placeholder: {type: String, default: 'DD/MM/YYYY'},
			value: String,
			datePickerFormat: String,
			tabindex: 0,
			inline: {type: Boolean, default: () => false},
			yearRange: {type: String, default: 'c-10:c+10'}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldDate">
				<div v-if="isPrintOnly">{{value}}</div>
				<neon-input-date v-else v-model="modelValue" @input="validate()" :id="id" :name="inputName" :datePickerFormat="datePickerFormat" :placeholder="placeholder" :tabindex="tabindex" :readOnly="isReadOnly" :yearRange="yearRange"></neon-input-date>
			</neon-core-form-field>
		`
	});

	Vue.component('neon-core-form-fields-daterange', {
		extends: BaseField,
		props: {
			value: {type: [Array, Object], default: function() { return {"from": "", "to": ""}; }},
			datePickerFormat: {type: String, default: 'dd/mm/yy'},
			yearRange: {type: String, default: 'c-10:c+10'}
		},
		created: function() {
			if (this.value === '') {
				this.modelValue = {"from": "", "to": ""};
			}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldDaterange">
				<neon-input-date v-model="from" :id="id+'-from'" :name="inputName+'[from]'" :datePickerFormat="datePickerFormat" :placeholder="placeholder" :readOnly="isReadOnly" :yearRange="yearRange"></neon-input-date>
				<neon-input-date v-model="to" :id="id+'to'" :name="inputName+'[to]'" :datePickerFormat="datePickerFormat" :placeholder="placeholder" :readOnly="isReadOnly" :yearRange="yearRange"></neon-input-date>
			</neon-core-form-field>
		`,
		computed: {
			from: {
				set(value) {
					this.modelValue = {from: value, to: this.to};
				},
				get() {
					var d = this.modelValue;
					if (!_.isObject(d))
						return '';
					return this.modelValue['from'];
				}
			},
			to: {
				set(value) {
					this.modelValue = {from: this.from, to: value};
				},
				get() {
					var d = this.modelValue;
					if (!_.isObject(d))
						return '';
					return this.modelValue['to'];
				}
			}
		}
	});

	Vue.component('neon-core-form-fields-datetime', {
		extends: BaseField,
		props: {
			placeholder: {type: String, default: ''},
			value: String,
			datePickerFormat: String,
			yearRange: {type: String, default: 'c-10:c+10'},
			// options for the time input
			timeOptions: {
				type: Object,
				default: function () {
					return {
						showSeconds: false,
						fieldType: 'select'
					};
				},
				editor: {
					component: 'neon-core-form-form',
					fields: {
						showSeconds: {
							label: 'Show Seconds',
							class: 'neon\\core\\form\\fields\\SwitchButton',
							name: 'showSeconds'
						},
						fieldType: {
							label: 'Field Type',
							class: 'neon\\core\\form\\fields\\Select',
							name: 'fieldType',
							allowClear: false,
							items: {'input' : 'Input Box', 'select': 'Drop down'},
							value: 'select'
						}
					}
				}
			},
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" :labelFor="id+'-date'" class="neonFieldDatetime" style="overflow:hidden">
				<div class="clearfix">
					<neon-input-date style="display:block;float:left;" class="mr-1" v-model="date" :id="id" :datePickerFormat="datePickerFormat" :placeholder="placeholder" :readOnly="isReadOnly" :yearRange="yearRange"></neon-input-date>
					<neon-input-time style="display:block;float:left;" v-model="time" v-bind="timeOptions" :readOnly="isReadOnly"></neon-input-time>
					<input style="display:none;" type="hidden" :name="inputName" :value="modelValue" />
				</div>
			</neon-core-form-field>
		`,
		computed: {
			time: {
				set(value) {
					this.modelValue = this.date + ' ' + value;
				},
				get() {
					if (!_.isString(this.modelValue))
						return '';
					var time = this.modelValue.split(' ')
					return time[1];
				}
			},
			date: {
				set(value) {
					this.modelValue = value + ' ' + this.time;
				},
				get() {
					if (!_.isString(this.modelValue))
						return '';
					var date = this.modelValue.split(' ');
					return date[0];
				}
			}
		}
	});

	/**
	 * Text Field
	 */
	var FieldReal = {
		extends: BaseField,
		props: {
			decimal: { type: Number, default: 1 },
			value: {type: [Number, String] }
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFieldReal">
				<div v-if="isPrintOnly">{{value}}</div>
				<input v-else type="number" :min="options.min" :max="options.max" :step="options.step"
					:value="modelValue" @focus="onFocus()" @blur="setValue($event.target.value)"
					class="form-control" :name="inputName" :id="id" :placeholder="placeholder" :readOnly="isReadOnly" />
			</neon-core-form-field>
		`,
		methods: {
			setValue(value) {
				this.modelValue = parseFloat(value);
				this.onBlur();
			}
		},
		computed: {
			options: function(){
				let options = {};
				options.max = Math.pow(10, (16-this.decimal));
				options.min = -1*options.max;
				options.step = (this.decimal >= 0) ? Math.pow(10, -1*this.decimal) : 1;
				return options;
			}
		}
	};
	Vue.component('neon-core-form-fields-real', FieldReal);
	Vue.component('neon-core-form-fields-integer', FieldReal);

	Vue.component('neon-core-form-fields-display-text', {
		props: {
			text: String,
		},
		template: `
			<div class="form-group neonFormDisplayText">
				<div class="neonFormDisplayText_text">{{text}}</div>
			</div>
		`,
	});

	Vue.component('neon-core-form-fields-heading', {
		props: {
			text: String,
		},
		template: `
			<div class="form-group neonFormHeading">
				<div class="neonFormHeading_content">
					<h2>{{text}}</h2>
				</div>
			</div>
		`
	});

	Vue.component('neon-core-form-fields-guidance', {
		extends: BaseField,
		props: {
			text: String,
			showIf: {type: [Array, Object]}
		},
		template: `
			<neon-core-form-field v-bind="fieldProps">
				<div class="neonFormGuidance form-group">
					<div class="neonFormGuidance_text"><neon-markdown :markdown="text"></neon-markdown></div>
				</div>
			</neon-core-form-field>
		`
	});

	Vue.component('neon-core-form-fields-description', {
		props: {
			text: String
		},
		template: `
			<div class="neonFormDescription form-group">
				<div class="neonFormDescription_text">{{text}}</div>
			</div>
		`
	});


	Vue.component('neon-core-form-fields-spacer', {
		props: {
			height: Number
		},
		template: `<div class="neonFormSpacer form-group" :style="{height:height+'px'}"></div>`
	});

	/**
	 * TODO - 20180828 NJ - Fix firefly image to allow for chrooted directories
	 * so that members can only access their files in the system. Then uncomment
	 * these in the wysiwyg. Also remove the simpleImageUpload plugin added as a temporary fix for Palladium
	 */
	Vue.component('neon-core-form-fields-wysiwyg', {
		extends: BaseField,
		props: {
			value: {type: String},
			/**
			 * The wysiwyg config options - If this is an object this is passed to Ckeditor's config
			 * If this is a string, then it assumes it is the name of a preconfigured config set.
			 * By default it returns the config string of 'default' - this is looked up in the components configSets.
			 * If the string config key is found in the configSets object it uses the returned object as the config object.
			 * If the key is not found then it will use the 'default' configSet option e.g. `this.configSets['default']`
			 */
			config: {
				type: String,
				default: 'default',
				editor: {
					order: 1,
					component: 'neon-core-form-fields-select',
					items: {'simple': 'Simple', 'default': 'Default'},
					allowClear: false,
					create: true,
					label: 'Toolbar Set',
					hint: 'Select the toolbar set to use. You can add a custom one which must exist in the provided config sets',
				}
			},

			configSets: {
				type: [String, Object],
				default: '',
				editor: false
			}
		},
		watch: {
			isReadOnly: function(value){
				this.toggleReadOnly(value);
			},
			config: function(value) {
				if (CKEDITOR.instances[this.id]) {
					CKEDITOR.instances[this.id].destroy();
				}
				this.attachEditor();
			}
		},
		template: `
			<neon-core-form-field v-bind="$props" class="neonFieldWysiwyg">
				<div v-if="isPrintOnly" v-html="value"></div>
				<div v-else>
					<textarea ref="editor" :name="inputName" :value="value" :id="id" ></textarea>
				</div>
			</neon-core-form-field>
		`,
		computed: {
			resolvedConfig: function() {
				var configSets = this.configSets;
				if (_.isString(configSets))
					configSets = JSON.parse(configSets);
				if (_.isString(this.config) && _.isDefined(configSets[this.config]))
					return configSets[this.config];
				if (_.isDefined(configSets['default']))
					return configSets['default'];
				return [];
			}
		},
		mounted: function() {
			this.attachEditor();
		},
		methods: {
			attachEditor: function() {
				var config = this.resolvedConfig;
				config.readOnly = this.isReadOnly;
				var editor = $(this.$refs.editor).ckeditor(config).editor;
				if (typeof editor !== 'undefined') {
					editor.on('focus', (e) => {
						this.onFocus();
					});
					editor.on( 'blur', (e) => {
						this.onBlur();
					});
					editor.on('change', () => {
						let html = editor.getData();
						if (html !== this.value) {
							this.modelValue = html;
							this.$emit('input', html);
						}
					});
				}
			},
			toggleReadOnly: function(value){
				var editor = $(this.$refs.editor).ckeditor().editor;
				if (typeof editor !== 'undefined')
					editor.setReadOnly(value);
			}
		},
		beforeDestroy: function () {
			if (CKEDITOR.instances[this.id]) {
				CKEDITOR.instances[this.id].destroy();
			}
		}
	});

	Vue.component('neon-wysiwyg', Vue.component('neon-core-form-fields-wysiwyg'));


// region: User Selector
	Vue.component('neon-user-form-fields-userselector', {
		extends: Vue.component('neon-core-form-fields-selectdynamic')
	});
// endregion

// region: User Selector
	Vue.component('neon-user-form-fields-userselectormultiple', {
		extends: Vue.component('neon-core-form-fields-link'),
		props: {
			multiple: {type: Boolean, default: true}
		}
	});
// endregion


	const MAP_STATES = {
		IDLE:'IDLE',
		ADDRESS_LOOKUP:'ADDRESS_LOOKUP',
		ADDRESS_LOOKUP_ERROR: 'ADDRESS_LOOKUP_ERROR'
	}
	Vue.component('neon-core-form-fields-locationmap', {
		extends: Vue.component('neon-core-form-fields-base'),
		props: {
			googleMapsKey: '',
			value: {type: Object, default:function() { return {lat:0,lng:0,address:''} }},
		},
		data: function() {
			return {
				// handle the core data value is an object containing lat, lng, address
				lat: this.value.lat,
				lng: this.value.lng,
				address: this.value.address,

				// internal state
				map: false,
				geocoder: false,
				status: MAP_STATES.IDLE,
				activeTab: 'coorodinates',
				addressSuggestions: []
			};
		},
		template: `
			<neon-core-form-field v-bind="fieldProps" class="neonFormLocationMap">

				<input :name="inputName+'[lat]'" :value="modelValue.lat" type="hidden" />
				<input :name="inputName+'[lng]'" :value="modelValue.lng" type="hidden" />
				<input :name="inputName+'[address]'" :value="modelValue.address" type="hidden" />

				<div ref="map" style="width: 100%; height: 400px;margin: 0 auto;background: gray;" class="google-map" ></div>
				<el-tabs v-model="activeTab" >
					<el-tab-pane label="Address" name="address">
						<div style="display:flex;align-items: center;">
							<div class="neonInput" style="width:100%;" :class="{'has-error': isAddressLookupError, 'isLoading': isAddressLoading}">
								<input name="inputName+'[address]" type="text" :value="address" @input="addressSearch($event)" placeholder="Type an address or click on the map" class="form-control">
								<div v-if="addressSuggestions.length" style="padding: 8px 0 6px 0; border: 1px solid #d8dde6;border-top: 0;border-radius: 0 0 3px 3px;margin-top: -2px;" >
									<a @click="setAddress(address)" class="btn btn-link" style="width:100%;text-align:left;outline:0" v-for="address in addressSuggestions">{{address.formatted_address}}</a>
								</div>
								<div v-if="status=='ADDRESS_LOOKUP_ERROR'" class="neonField_error help-block">
									<i class="fa exclamation"></i> No results found for <span style="color:black">{{address}}</span>. Please make sure that the address is spelled correctly.
								</div>
							</div>
							<a class="btn" @click="clear">Clear</a>
						</div>
					</el-tab-pane>
					<el-tab-pane label="Coordinates" name="coorodinates">
						<div style="display:flex;justify-content: space-evenly;align-items: center;">
							<label style="padding:8px;">Lattitude</label>
							<input name="inputName+'[lat]" type="number" class="form-control" placeholder="Between -90 and 90" v-model.number="lat" max="90" min="-90" step="any"  />
							<label style="padding:8px;" >Longitude</label>
							<input name="inputName+'[lng]"  type="number" class="form-control" placeholder="Between -180 and 180" v-model.number="lng" max="180" min="-180" step="any"" />
							<a class="btn" @click="clear">Clear</a>
						</div>
					</el-tab-pane>
				</el-tabs>
			</neon-core-form-field>
		`,
		watch: {
			lat: function(lat) {
				this.setPosition(lat, this.lng);
				this.modelValue = {lat: lat, lng:this.lng, address:this.address};
			},
			lng: function(lng) {
				this.setPosition(this.lat, lng);
				this.modelValue = {lat: this.lat, lng:lng, address:this.address};
			},
			address: function(address) {
				this.modelValue = {lat: this.lat, lng:this.lng, address:this.address};
			}
		},
		mounted() {
			var vm = this;
			// creates a map object and stores in this.map
			this.createMap();
			// Create a marker to represent the location
			this.marker = new google.maps.Marker({map: this.map, position: this.map.getCenter(), draggable: true});
			// Move marker to where the map was clicked
			google.maps.event.addListener(this.map, 'click', function(event) {
				vm.setPosition(event.latLng.lat(), event.latLng.lng());
			});
			// After the marker is dragged
			google.maps.event.addListener(this.marker, "dragend", function(event) {
				vm.setPosition(event.latLng.lat(), event.latLng.lng());
			})
		},
		computed: {
			isAddressLookupError: function() { return this.status === MAP_STATES.ADDRESS_LOOKUP_ERROR; },
			isAddressLoading: function() {return this.status === MAP_STATES.ADDRESS_LOOKUP; },
		},
		methods: {
			createMap() {
				var myStyles =[{featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]}];
				this.geocoder = new google.maps.Geocoder;
				this.map = new google.maps.Map(this.$refs.map, {
					center: {lat: this.lat, lng: this.lng},
					zoom: 7,
					draggable: !this.disabled,
					options: {maxZoom: 20, minZoom: 3, scrollwheel: false, mapTypeId: "roadmap", styles: myStyles, zoomControl: true, mapTypeControl: false, scaleControl: true, streetViewControl: false, rotateControl: false, fullscreenControl: true},
					defaultZoom: 6,
					yesIWantToUseGoogleMapApiInternals: true,
				});
			},
			clear: function() {
				this.lat = null;
				this.lng = null;
				this.address = '';
				this.addressSuggestions = [];
			},
			/**
			 * Adds a marker to the map
			 * @param location - latLng
			 * @param map - the google map object
			 */
			addMarker: function(location, map) {
				this.marker = new google.maps.Marker({
					position: location,
					draggable:true,
					map: map
				});
			},
			setPosition: function(lat, lng, lookupAddress) {
				lookupAddress = lookupAddress || true;
				var latLng = new google.maps.LatLng({lat: lat, lng: lng})
				this.marker.setPosition(latLng);
				this.map.panTo(latLng);
				if (lookupAddress)
					this.getAddressFromLocation();
				this.addressSuggestions = [];
				this.lat = lat;
				this.lng = lng;
			},
			/**
			 * Handles the user address input field
			 * @param event
			 */
			addressSearch: function(event) {
				this.address = event.target.value;
				var vm = this;
				this.status = MAP_STATES.ADDRESS_LOOKUP;
				this.lookupAddress();
			},
			lookupAddress: _.debounce(function() {
				var vm = this;
				this.geocoder.geocode({address: this.address}, function(results, status) {
					if (status !== 'OK') {
						vm.status = MAP_STATES.ADDRESS_LOOKUP_ERROR;
						return;
					}
					vm.addressSuggestions = results;
					vm.status = MAP_STATES.IDLE;
				});
			}, 100),
			getAddressFromLocation: function() {
				var latLng = new google.maps.LatLng({lat: this.lat, lng: this.lng})
				var vm = this;
				this.geocoder.geocode({location: latLng}, function(results, status) {
					if (status === 'OK' && results && results.length > 0) {
						vm.address = results[0].formatted_address;
					}
				});
			},
			setAddress: function(address) {
				this.address = address.formatted_address;
				var loc = address.geometry.location;
				this.setPosition(loc.lat(), loc.lng(), false);
			}
		}
	});

})();



