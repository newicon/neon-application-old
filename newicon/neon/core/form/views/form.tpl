{*
 * receives data:
 * - fields => array of field objects
 * - csrf_token => the cross site request forgery token string
 * - form => the form object
 *}
<neon-core-form-form v-bind='{$form->toJson()}' id="{$form->id}" ></neon-core-form-form>