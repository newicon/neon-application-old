<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license
 */

namespace neon\core\form\fields;

/**
 * Class SwitchMultipleState
 * @package neon\core\form
 */
class SwitchMultipleState extends Radio
{
}