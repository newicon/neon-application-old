<?php


/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 27/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\core\web;

/**
 * Class Response
 * @author Steve O'Brien - Newicon Ltd
 * @package neon\core\web
 */
class Response extends \yii\web\Response
{
	public $formatters = [
		self::FORMAT_JSON => '\neon\core\web\JsonResponseFormatter'
	];

	/**
	 * Prepares for sending the response.
	 * The default implementation will convert [[data]] into [[content]] and set headers accordingly.
	 * @throws \InvalidArgumentException if the formatter for the specified format is invalid or [[format]] is not supported
	 */
	protected function prepare()
	{
		// check if we should guess the format
		// mainly if we should convert the response to be json
		if ($this->shouldBeJson($this->data) && $this->format == self::FORMAT_HTML) {
			$this->format = self::FORMAT_JSON;
		}

		parent::prepare();
	}

	/**
	 * Set the response format to be JSON
	 */
	public function asJson()
	{
		$this->format = self::FORMAT_JSON;
	}

	/**
	 * Determine if the given content should be turned into JSON.
	 *
	 * @param  mixed  $content
	 * @return bool
	 */
	protected function shouldBeJson($content)
	{
		return !is_string($content);
	}
}