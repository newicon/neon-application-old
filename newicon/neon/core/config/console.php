<?php
/**
 * Console application specific config
 * merged into common.php
 * merge(common, app_console, env_*, local)
 */
$config = [
	'id' => 'console',
	'bootstrap' => ['log'],
	'defaultRoute' => 'help',
	'controllerNamespace' => 'neon\core\console\controllers',
	'components' => [
		'db' => [
			'enableSchemaCache' => true,
			'schemaCacheDuration' => 3600,
		],
		// override the request property
		'request' => [
			'class' => '\yii\console\Request',
		],
		'errorHandler' => [
			'class' => '\yii\console\ErrorHandler',
		],
		// the user module could define this on boot.
		// but this is useful if you need to make any project specific adjustments
		'session' => [ // for use session in console application
			'class' => 'yii\web\DbSession'
		],
	],
];
if (env('NEON_DEBUG')) {
	//$config['bootstrap'][] = 'gii';
	$config['modules'] = [
		//'gii' => ['class' => 'yii\gii\Module'],
	];
}
return $config;