<?php

/**
 * Web Application specific config
 * merged into common.php
 * merge(common, web, local)
 */
$config = [
	'defaultRoute' => 'core/site',
	// enable the redactor widget
	'components' => [
		'request' => [
			'cookieValidationKey' => neon_encryption_key(),
			'enableCookieValidation' => true,
			'enableCsrfValidation' => true,
			'csrfParam' => 'neon_csrf',
			'parsers' => [
				'application/json' => 'yii\web\JsonParser',
			]
		],
		'response' => [
			'class' => 'neon\core\web\Response',
			'formatters' => [
				'json' => 'neon\core\web\JsonResponseFormatter',
				'php' => 'neon\core\web\PhpResponseFormatter',
			]
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
		],
		'errorHandler' => [

			'class' => 'yii\web\ErrorHandler',

			/**
			 * @var string the route (e.g. `site/error`) to the controller action that will be used
			 * to display external errors. Inside the action, it can retrieve the error information
			 * using `Yii::$app->errorHandler->exception`. This property defaults to null, meaning ErrorHandler
			 * will handle the error display.
			 */
			'errorAction' => '/core/site/error',
			/**
			 * @var integer the size of the reserved memory. A portion of memory is pre-allocated so that
			 * when an out-of-memory issue occurs, the error handler is able to handle the error with
			 * the help of this reserved memory. If you set this value to be 0, no memory will be reserved.
			 * Defaults to 256KB.
			 */
			'memoryReserveSize' => 262144,
			/**
			 * @var string the path of the view file for rendering exceptions without call stack information.
			 */
			'errorView' => '@yii/views/errorHandler/error.php',
			/**
			 * @var string the path of the view file for rendering exceptions.
			 */
			'exceptionView' => '@yii/views/errorHandler/exception.php',
			/**
			 * @var string the path of the view file for rendering exceptions and errors call stack element.
			 */
			'callStackItemView' => '@yii/views/errorHandler/callStackItem.php',
			/**
			 * @var string the path of the view file for rendering previous exceptions.
			 */
			'previousExceptionView' => '@yii/views/errorHandler/previousException.php'
		],
		'view' => [
			'class' => 'neon\core\web\View',
			// Whether css files should be concatenated
			'cssConcatenate' => false,
			// Whether to process the css for variables using the registered css renderer
			// currently only applicable id cssConcatenate is true
			'cssProcess' => false,
			// whether to minify the css content
			// It may be useful to set this to false in development mode
			// applies to css blocks and only to concatenated file
			'cssMinify' => true,
			// Whether to concatenate js files
			'jsConcatenate' => false,
			'theme' => [
				'class' => 'neon\core\view\Theme',
				'basePath' => '@neon/core/themes/neon',
				'pathMap' => [
					'@neon' => '@neon/core/themes/neon',
					'@app/views' => '@neon/core/themes/neon',
				],
			],
			'renderers' => [
				'twig' => [
					'class' => '\neon\core\view\TwigRenderer',
					'cachePath' => '@runtime/Twig/cache',
					// Array of twig options:
					'options' => [
						'auto_reload' => true,
					],
					'globals' => [
						'html' => '\neon\core\helpers\Html',
						'url' => '\neon\core\helpers\Url'
					],
					'uses' => [
						'/neon/cms/widgets',
					],
				],
				// smarty
				'tpl' => [
					'class' => '\neon\core\view\SmartyRenderer',
					/**
					 * @var string the directory or path alias pointing to where Smarty cache will be stored.
					 */
					'cachePath' => '@runtime/Smarty/cache',
					/**
					 * @var string the directory or path alias pointing to where Smarty compiled templates will be stored.
					 */
					'compilePath' => '@runtime/Smarty/compile',
					/**
					 * @var array Add additional directories to Smarty's search path for plugins.
					 * 'pluginDirs' => ['my/dir'],
					 */
					/**
					 * @var array Class imports similar to the use tag
					 */
					'imports' => [],
					/**
					 * @var array Widget declarations
					 */
					'widgets' => ['functions' => [], 'blocks' => []],
					/**
					 * @var string extension class name
					 */
					'extensionClass' => '\neon\core\view\SmartyExtension',
					/**
					 * @var array additional Smarty options
					 * @see http://www.smarty.net/docs/en/api.variables.tpl
					 */
					'options' => [
						// Compile check will check the modified time of the source template file
						// if it has been modified after it was compiled it will recompile
						// for performance we can turn this off in production
						'compile_check' => env('NEON_DEBUG'),
						// Forces smarty to recompile its templates every time.
						// useful for development
						// 'force_compile' => env('NEON_DEBUG')
					],
				],
				'css' => [
					'class' => '\neon\core\view\SmartyRenderer',
					'cachePath' => '@runtime/Smarty/css/cache',
					'compilePath' => '@runtime/Smarty/css/compile',
					'imports' => [],
					'widgets' => ['functions' => [], 'blocks' => []],
					'extensionClass' => '\neon\core\view\SmartyExtension',
					/**
					 * @var array additional Smarty options
					 * @see http://www.smarty.net/docs/en/api.variables.tpl
					 */
					'options' => [
						'escape_html' => true,
						'left_delimiter' => '[[',
						'right_delimiter' => ']]',
						// Compile check will check the modified time of the source template file
						// if it has been modified after it was compiled it will recompile
						// for performance we can turn this off in production
						'compile_check' => env('NEON_DEBUG'),
						// Forces smarty to recompile its templates every time.
						// useful for development
						'force_compile' => env('NEON_DEBUG')
					],
				],
				// markdown renderer
				'md' => [
					'class' => '\neon\core\view\MarkdownRenderer',
				]
			],
		],
	]
];
return $config;