<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>

FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 */
	data: {
		class: "neon\\core\\form\\fields\\Textarea",
		classLabel: "Multiline Text",
		name: 'textarea',
		label: 'Text area',
		placeholder: '',
		required: false
	},

	/* --------------------------------------------------------------------------------------------
	 * Icon
	 * --------------------------------------------------------------------------------------------
	 */
	icon: {name: 'Multiple Line', icon: 'fa fa-font', group: 'Text', order:10}

});

</script>
<?php Page::jsEnd(); ?>