<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 */
		data: {
			class: "neon\\core\\form\\fields\\Json",
			classLabel: "JSON Text",
			name: 'json',
			label: 'Json',
			required: false
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'JSON', icon: 'fa fa-code', group: 'Data', order: 16},

		/* ----------------------------------------------------------------------------------------
		 * Form Component
		 * ----------------------------------------------------------------------------------------
		 */
		form: Vue.component('form-builder-field-form-default').extend({}),

	});
</script>
<?php Page::jsEnd(); ?>