<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Neill Jones <neill.jones@newicon.net> 12/03/2017
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
	<script>

		FormBuilderApp.addFieldComponent({

			/* ----------------------------------------------------------------------------------------
			 * Default State Data
			 * ----------------------------------------------------------------------------------------
			 * The default data to populate this component with when it is first added to the form
			 */
			data: {
				class: "neon\\core\\form\\fields\\Time",
				classLabel: "Time",
				name: "time",
				label: 'Time',
				required: false,
				fieldType: 'input',
				showSeconds: false
			},

			/* ----------------------------------------------------------------------------------------
			 * Icon Definition
			 * ----------------------------------------------------------------------------------------
			 */
			icon:  {name: 'Time',  icon: 'fa fa-clock-o', group: 'Date and Time', order: 91}

		});
	</script>
<?php Page::jsEnd(); ?>