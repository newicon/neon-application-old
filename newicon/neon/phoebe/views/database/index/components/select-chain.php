<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\SelectChain",
			classLabel: "Select Chain",
			name: 'select_chain',
			label:"Select Chain",
			required: false,
			placeholder: '- Select an option -',
			items: [],
			endPoint: '',
			classMemberMap: {},
			chainValues: [],
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Chain Select', icon: 'fa fa-ellipsis-h', group: 'Choice', order: 40},

	});

</script>
<?php Page::jsEnd(); ?>