<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\user\\form\\fields\\UserSelector",
			classLabel: "User Selector",
			name: "user_id",
			label:"User Selector",
			hint: "Select a user",
			required: false,
			placeholder: '- Select User -',
			dataMapProvider: 'user',
			dataMapKey: 'users'
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'User Selector', icon: 'fa fa-files-o', group: 'Deprecated', order: 999},

	});

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\user\\form\\fields\\UserSelectorMultiple",
			classLabel: "User Selector Multiple",
			name: "user_id",
			label:"User Selector Multiple",
			hint: "Select a user",
			required: false,
			placeholder: '- Select User -',
			dataMapProvider: 'user',
			dataMapKey: 'users'
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'User Selector Multiple', icon: 'fa fa-files-o', group: 'Deprecated', order: 999},

	});


</script>
<?php Page::jsEnd(); ?>
