<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\el\\Color",
		classLabel: "Color",
		name: 'color',
		label: 'Colour'
	},
	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'El Colour',  icon: 'fa fa-paint-brush', group: 'element ui', order: 100},

});

FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\el\\DateTime",
		classLabel:"Date & Time Picker",
		name: "datetime",
		label: 'Date & Time',
		required:false
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon:  {name: 'Date & Time',  icon: 'fa fa-calendar-plus-o', group: 'element ui', order: 100},

});

FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\el\\Date",
		classLabel:"Date Picker",
		name: "date",
		label: 'Date',
		required:false
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon:  {name: 'Date',  icon: 'fa fa-calendar-plus-o', group: 'element ui', order: 100},

});

FormBuilderApp.addFieldComponent({
	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\el\\Slider",
		classLabel:"Slider",
		name: "slider",
		label: 'Slider',
		required:false
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon:  {name: 'Slider',  icon: 'fa fa-sliders', group: 'element ui', order: 100},
});
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * - if the field component - is actually a standard type that takes a field configuration
	 * as a param then this might be sorted out by the component itself
	 */
	data: {
		class: "neon\\core\\form\\fields\\el\\SwitchInput",
		classLabel: "Switch",
		name: 'switch',
		label: 'Switch'
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Switch',  icon: 'fa fa-toggle-on', group: 'element ui', order: 100},

});


</script>
<?php Page::jsEnd(); ?>