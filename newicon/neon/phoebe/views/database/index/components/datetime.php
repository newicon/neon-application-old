<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
	<script>

		FormBuilderApp.addFieldComponent({

			/* ----------------------------------------------------------------------------------------
			 * Default State Data
			 * ----------------------------------------------------------------------------------------
			 * The default data to populate this component with when it is first added to the form
			 */
			data: {
				class: "neon\\core\\form\\fields\\DateTime",
				classLabel:"Date & Time Picker",
				name: "datetime",
				label: 'Date & Time',
				required: false,
				placeholder: 'DD/MM/YYYY',
			},

			/* ----------------------------------------------------------------------------------------
			 * Icon Definition
			 * ----------------------------------------------------------------------------------------
			 */
			icon:  {name: 'Date & Time',  icon: 'fa fa-calendar-plus-o', group: 'Date and Time', order: 91}

		});
	</script>
<?php Page::jsEnd(); ?>