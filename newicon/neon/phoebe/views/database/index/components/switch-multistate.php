<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Luke Manson <luke.manson@newicon.net> 25/01/2019 16:31
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\SwitchMultipleState",
		classLabel: "Switch Multiple State",
		name: 'switch_multiple_state',
		label: 'Switch Multiple State',
		hint: "",
		required: false,
		items: {
			"Key 1": "Option 1",
			"Key 2": "Option 2"
		}
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Switch Multiple',  icon: 'fa fa-square-o', group: 'Choice', order: 60},

});

</script>
<?php Page::jsEnd(); ?>