<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * - if the field component - is actually a standard type that takes a field configuration
		 * as a param then this might be sorted out by the component itself
		 */
		data: {
			class: "neon\\core\\form\\fields\\Select",
			classLabel: "Select List (Fixed)",
			name: 'select',
			label:"Select list",
			required: false,
			placeholder: '- Select an option -',
			items: {
				"Key 1": "Option 1",
				"Key 2": "Option 2",
				"Key 3": "Option 3"
			}
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Single Select', icon: 'fa fa-list-ul', group: 'Choice', order: 40},

	});

</script>
<?php Page::jsEnd(); ?>