<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>
	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\File",
			classLabel: "File",
			name: "file",
			label: 'Single File Upload',
			startPath: '/'
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'File Uploader', icon: 'fa fa-upload', group: 'Media', order: 69},

	});

</script>
<?php Page::jsEnd(); ?>