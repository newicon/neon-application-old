<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
	<script>

		FormBuilderApp.addFieldComponent({

			/* ----------------------------------------------------------------------------------------
			 * Default State Data
			 * ----------------------------------------------------------------------------------------
			 * The default data to populate this component with when it is first added to the form
			 */
			data: {
				class: "neon\\core\\form\\fields\\Date",
				classLabel: "Date Picker",
				name: "date",
				label: 'Date'
			},

			/* ----------------------------------------------------------------------------------------
			 * Icon Definition
			 * ----------------------------------------------------------------------------------------
			 */
			icon:  {name: 'Date',  icon: 'fa fa-calendar-plus-o', group: 'Date and Time', order: 90}

		});
	</script>
<?php Page::jsEnd(); ?>