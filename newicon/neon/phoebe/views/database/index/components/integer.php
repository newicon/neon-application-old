<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\Integer",
		classLabel: "Integer Number",
		name: 'integer',
		label: 'Integer'
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Integer',  icon: 'fa fa-italic', group: 'Number', order: 80},

});

</script>
<?php Page::jsEnd(); ?>