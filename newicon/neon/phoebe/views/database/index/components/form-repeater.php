<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
	<script>
		FormBuilderApp.addFieldComponent({

			/* ----------------------------------------------------------------------------------------
			 * Default State Data
			 * ----------------------------------------------------------------------------------------
			 * The default data to populate this component with when it is first added to the form
			 */
			data: {
				class: "neon\\core\\form\\FormRepeater",
				classLabel: "Single Line Text",
				name: 'form-repeater',
				label: 'Subform',
				count: 1,
				countMax: 0,
				countMin: 0,
				allowAdd: true,
				allowRemove: true,
				fields: {},
				order: 0,
				value: {}
			},

			/* ----------------------------------------------------------------------------------------
			 * Icon Definition
			 * ----------------------------------------------------------------------------------------
			 */
			icon: {name: 'Subform',  icon: 'fa fa-font', group: 'dev', order: 1000, groupOder: 1000},

		});

	</script>
<?php Page::jsEnd(); ?>