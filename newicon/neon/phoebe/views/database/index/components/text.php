<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\Text",
		classLabel: "Single Line Text",
		name: 'text',
		label: 'Text'
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Single Line',  icon: 'fa fa-font', group: 'Text', order: 0},

	/* ----------------------------------------------------------------------------------------
	 * Form Component
	 * ----------------------------------------------------------------------------------------
	 */
	form: Vue.component('form-builder-field-form-default').extend({}),

});

</script>
<?php Page::jsEnd(); ?>