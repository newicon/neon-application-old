<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * - if the field component - is actually a standard type that takes a field configuration
		 * as a param then this might be sorted out by the component itself
		 */
		data: {
			class: "neon\\core\\form\\fields\\Radio",
			classLabel: "Radio List",
			name: 'radio',
			label:"Radio options",
			hint: "",
			required: false,
			items: {
				"key 1": "Option 1",
				"key 2": "Option 2",
				"key 3": "Option 3"
			}
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon:  {name: 'Radio List', icon: 'fa fa-circle-o', group: 'Choice', order: 50},

	});


</script>
<?php Page::jsEnd(); ?>