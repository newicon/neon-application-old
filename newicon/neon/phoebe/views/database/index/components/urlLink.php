<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2019 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Neill Jones <neill.jones@newicon.net>
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>
FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 * The default data to populate this component with when it is first added to the form
	 */
	data: {
		class: "neon\\core\\form\\fields\\UrlLink",
		classLabel: "Url Link",
		name: 'url_link',
		label: 'Url Link',
		absolute: true
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Url Link',  icon: 'fa fa-link', group: 'Text', order: 20},


});

</script>
<?php Page::jsEnd(); ?>