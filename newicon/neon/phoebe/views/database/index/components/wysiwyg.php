<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>


FormBuilderApp.addFieldComponent({

	/* ----------------------------------------------------------------------------------------
	 * Default State Data
	 * ----------------------------------------------------------------------------------------
	 */
	data: {
		class: "neon\\core\\form\\fields\\Wysiwyg",
		classLabel: "Wysiwyg Multiline Text",
		name: 'wysiwyg',
		label: 'Label',
	},

	/* ----------------------------------------------------------------------------------------
	 * Icon Definition
	 * ----------------------------------------------------------------------------------------
	 */
	icon: {name: 'Wysiwyg', icon: 'fa fa-font', group: 'Rich Text', order: 15},

});
</script>
<?php Page::jsEnd(); ?>