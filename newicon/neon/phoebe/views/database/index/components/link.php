<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * Image
		 */
		data: {
			class: "neon\\core\\form\\fields\\Link",
			classLabel: 'Link',
			name: 'link',
			label:"Many to Many",
			required: false,
			placeholder: '- Select Objects -',
			dataMapProvider: 'dds',
			dataMapKey: '',
			multiple: true,
			value: []
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Many to Many', icon: 'fa fa-link', group: 'Table Links', order: 40},

		
	});

</script>
<?php Page::jsEnd(); ?>