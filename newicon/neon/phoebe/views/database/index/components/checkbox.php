<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\Checkbox",
			classLabel: "Checkbox",
			name: "checkbox",
			label:"Checkbox",
			checkboxLabel:"label",
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: { name: 'Check Box', icon: 'fa fa-check-square-o', group: 'Boolean', order: 60 },
	});
</script>
<?php Page::jsEnd(); ?>