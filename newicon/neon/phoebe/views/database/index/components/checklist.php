<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
?>

<?php Page::jsBegin(); ?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\CheckList",
			classLabel: "Checkbox List",
			label:"Checkbox list",
			items: {
				"Key 1":  "Option 1",
				"Key 2": "Option 2",
				"Key 3": "Option 3"
			}
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: { name: 'Checkbox List', icon: 'fa fa-check-square-o', group: 'Choice', order: 60 },

		/* ----------------------------------------------------------------------------------------
		 * Form Component
		 * ----------------------------------------------------------------------------------------
		 */
		form: Vue.component('form-builder-field-form-default').extend({
			template: `
				<div>
					<form-builder-field-form-element-type :field="field"></form-builder-field-form-element-type>
					<form-builder-field-form-element-name :field="field"></form-builder-field-form-element-name>
					<form-builder-field-form-element-label :field="field"></form-builder-field-form-element-label>
					<form-builder-field-form-element-hint :field="field"></form-builder-field-form-element-hint>
					<form-builder-field-form-element-required :field="field"></form-builder-field-form-element-required>
					<form-builder-field-form-element-list-items :field="field"></form-builder-field-form-element-list-items>
					<div class="form-group"><form-builder-field-form-element-delete :field="field"></form-builder-field-form-element-delete></div>
				</div>
			`,
		}),

	});


</script>
<?php Page::jsEnd(); ?>