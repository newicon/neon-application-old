<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\SelectMultiple",
			classLabel: "Multiple Select List (Fixed)",
			name: 'select',
			label: "Multiple Choice List",
			required: false,
			multiple: true,
			placeholder: 'Select an option',
			items: {
				"Key 1": "Option 1",
				"Key 2": "Option 2",
				"Key 3": "Option 3"
			}
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Multi Select', icon: 'fa fa-list-ul', group: 'Choice', order: 40},

	});

</script>
<?php Page::jsEnd(); ?>