<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

//FormBuilderApp.addFieldComponent({
//
//	/* ----------------------------------------------------------------------------------------
//	 * Default State Data
//	 * ----------------------------------------------------------------------------------------
//	 * - if the field component - is actually a standard type that takes a field configuration
//	 * as a param then this might be sorted out by the component itself
//	 */
//	data: {
//		class: "\\neon\\core\\form\\fields\\Section",
//		classLabel: "Section",
//		name: 'section',
//		label: 'Section',
//		hint: '',
//		deleted: 0
//	},
//
//	/* ----------------------------------------------------------------------------------------
//	 * Icon Definition
//	 * ----------------------------------------------------------------------------------------
//	 */
//	icon: {name: 'Section Broken',    icon: 'fa fa-minus', group: 'display', order: 200},
//
//});

</script>
<?php Page::jsEnd(); ?>