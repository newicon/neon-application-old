<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\cms\\form\\fields\\PageSelector",
			classLabel: "Page Selector",
			name: "page_id",
			label:"Page Selector",
			hint: "Select the required page",
			required: false,
			placeholder: '- Select Page -',
			dataMapProvider: 'cms',
			dataMapKey: 'pages'
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Page Selector', icon: 'fa fa-files-o', group: 'Common Links', order: 30},

	});



</script>
<?php Page::jsEnd(); ?>
