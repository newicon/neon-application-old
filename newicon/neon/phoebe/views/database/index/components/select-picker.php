<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Chris Woollon <chris.woollon@newicon.net> 09/10/2018 17:03
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\core\\form\\fields\\SelectPicker",
			classLabel: 'Select Picker',
			name: 'select_picker',
			label: "Select Picker",
			required: false,
			placeholder: '',
			dataMapProvider: 'dds',
			dataMapKey: '',
			multiple: true,
			value: [],
			selectedItemsListLabel: ''
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'Select Picker', icon: 'fa fa-hand-pointer-o', group: 'Choice', order: 40},

	});

</script>
<?php Page::jsEnd(); ?>