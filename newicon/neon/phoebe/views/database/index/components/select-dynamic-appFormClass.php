<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 23/10/2016 15:35
 * @package neon
 */
use \neon\core\helpers\Page;
Page::jsBegin();
?>
<script>

	FormBuilderApp.addFieldComponent({

		/* ----------------------------------------------------------------------------------------
		 * Default State Data
		 * ----------------------------------------------------------------------------------------
		 * The default data to populate this component with when it is first added to the form
		 */
		data: {
			class: "neon\\phoebe\\form\\fields\\AppFormClassSelector",
			classLabel: "App Form Class",
			name: "appform_class",
			label:"Application Form Class Selector",
			hint: "Select an application form class",
			required: false,
			placeholder: '- Select A Form Class -'
		},

		/* ----------------------------------------------------------------------------------------
		 * Icon Definition
		 * ----------------------------------------------------------------------------------------
		 */
		icon: {name: 'App Form Class', icon: 'fa fa-wpforms', group: 'Common Links', order: 40},

	});

</script>
<?php Page::jsEnd(); ?>
