<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @created 11/10/2016 12:20 by steve
 * @author Steve O'Brien <steve.obrien@newicon.net>
 * @package forms
 */


namespace neon\phoebe\builders\database\builder;


class Form
{
	/**
	 * Get a form with one of each field representing the default field states
	 * @return \neon\core\form\Form
	 */
	public function getDefaultForm()
	{
		$form = new \neon\core\form\Form(['id' => 'default_fields_form']);
		$form->setName('DefaultFieldForm');

		$form->addFieldCheckbox('checkboxy')
			->setLabel('one lonely checkbox')
			->setCheckboxLabel('Engage');

		$cl = new \neon\core\form\fields\CheckList('checklist', ['items' => ['key_1' => 'option 1', 'key_2' => 'option 2']]);
		$cl->setLabel('Checkbox list');
		$form->add($cl);

		$form->add(['class'=>'\neon\core\form\fields\el\DateTime', 'name'=>'eldatetime']);

		$form->addFieldDate('datey');

		// Single line text
		$form->addFieldText('texty')
			->setLabel('Label');
		// Multi line text
		$form->addFieldFile('file')
			->setLabel('File');


		// Multi line text
		$form->addFieldTextarea('textareay')
			->setLabel('Label');

		// Drop down list
		$form->addFieldSelect('selecty')
			->setLabel('Label')
			->setPlaceholder('Select an option')
			->setItems([
				'1' => 'Option 1',
				'2' => 'Option 2',
				'3' => 'Option 3'
			]);

		// Section field
		$form->addFieldSection('sectiony')
			->setLabel('My New Section')
			->setHint('Optional section information');

		// Visual editor field
		$form->addFieldWysiwyg('wysiwygy')
			->setLabel('Large description label')
			->setHint('Large description hint');


		// testcookie - for some reason file stack sets an emtpy cookie and a testcookie which breaks our test runner.
		// This is quite strange
		// $form->addFieldConfig([
		//	'class' => 'neon\core\form\fields\FileStack',
		//	'name' => 'filestack',
		//	'label' => 'Upload your file'
		// ]);

		$form->add([
			'class' => 'neon\core\form\fields\Markdown',
			'name' => 'markdown',
			'label' => 'Markdown'
		]);

		$form->add([
			'class' => 'neon\core\form\fields\Json',
			'name' => 'json',
			'label' => 'Json'
		]);

		$form->add([
			'class' => 'switch',
			'name' => 'Switch_from_alias',
			'label' => 'Switchy!'
		]);

		$form->add([
			'class' => 'image',
			'name' => 'imagey',
		]);

		$form->add([
			'class' => 'section',
			'name' => 'section',
		]);

		$form->addFieldSubmit('submity')
			->setValue('Post')
			->setLabel('Post');


		return $form;
	}

	/**
	 * @return \neon\core\form\Form
	 */
	public function getForm()
	{
		return $this->_form;
	}

	/**
	 * @return \neon\daedalus\App
	 */
	public function getDds()
	{
		return neon('dds');
	}
}