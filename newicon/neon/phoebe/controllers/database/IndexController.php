<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 11/10/2016 12:20
 * @package neon/phoebe
 */

namespace neon\phoebe\controllers\database;

use neon\core\helpers\Url;
use neon\phoebe\controllers\common\PhoebeController;
use neon\phoebe\controllers\common\PhoebeValidator;
use yii\web\HttpException;

/**
 * The Phoebe Database controller manages the manipulation of Daedalus
 * definitions using the Phoebe UI
 */
class IndexController extends PhoebeController
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = parent::rules();
		$rules[] = [
			'allow' => true,
			'actions' => ['validate-form'],
			'roles' => ['?', '@'] // anyone for validate form
		];
		return $rules;
	}

	/**
	 * Set this up as a daedalus phoebe adapter
	 * @param array $config
	 */
	public function __construct($id, $module, $config = [])
	{
		$config['phoebeType'] = 'daedalus';
		parent::__construct($id, $module, $config);
	}

	/**
	 * Edit a table definition
	 * @param string $type - the class type
	 * @return String
	 * @throws HttpException
	 */
	public function actionEdit($type = null)
	{
		if (!$this->canDevelop())
			throw new HttpException(403, 'This operation is not permitted here');

		$form = $this->getPhoebeForm($type, $isNewForm, false, true);

		// We have to get a form that has all the components available as the builder
		// then we can ask each field to register any necessary javascript and assets
		// Ideally we would tweak this so that the builder registers which components are available
		// to be included in the builder GUI - the components on the left.
		// This way it can perform both jobs - register necessary component assets and also control
		// the list of components available.
		// The builder does one last job which is also a configuration one, it sets the default configuration props
		// for each component - for example when the component is dragged onto the builder
		$builder = new \neon\phoebe\builders\database\builder\Form();
		$builder->getDefaultForm()->registerScripts($this->getView());

		return $this->render('edit-fields', [
			'definition' => $form->toJson(),
			'data' => [
				'urls' => [
					'list' => url('/daedalus/index/list'),
					'save' => url('/phoebe/database/api/table/save'),
				]
			],
			'dds' => [
				'data_map_info' => neon()->app('core')->getDataMapProviders(),
				'map_types' => neon()->dds->getDataMapTypes(),
				'class_type' => $type,
				'exists' => !$isNewForm,
				'can_develop' => $this->canDevelop(),
			]
		]);
	}

	/**
	 * Validation of a daedalus form object
	 * @param string $classType
	 * @return type
	 */
	public function actionValidateForm($classType, $name = '')
	{
		return PhoebeValidator::validateForm('daedalus', $classType, $name);
	}

	/**
	 * Create a table from its raw definition
	 */
	public function actionCreateTableRaw()
	{
		$formDefinition = [
			'name' => 'create-table',
			'fields' => [
				'name' => [
					'class' => 'text',
					'label' => 'Table Name',
					'hint' => 'Set the database table name',
					'required' => true
				],
				'label' => [
					'class' => 'text',
					'label' => 'Table User Label',
					'hint' => 'Set the table user friendly label',
					'required' => true
				],
				'fields' => [
					'class' => 'json',
					'label' => 'Field Definitions',
					'hint' => 'Enter the table fields. These should be in JSON format e.g. [ '.
						'{ "class": "text", "name": "field_name", "label":"Field Name", "hint":"Something about this" }, {etc} ]',
					'required' => true
				],
				'submit' => [
					'class' => 'submit',
					'label' => 'Submit Defintion'
				]
			]
		];
		$form = new \neon\core\form\Form($formDefinition);
		if (neon()->request->isAjax)
			return $form->ajaxValidation();
		if ($form->processRequest()) {
			$data = $form->getData();
			$definition = $this->canonicaliseDefinition($data);
			if ($definition) {
				neon('phoebe')
					->getService()
					->getPhoebeType('daedalus')
					->saveClassDefinition($data['name'], $definition, 'phoebe');
				return $this->redirect(['/daedalus/index']);
			}
		}
		return $this->render('create-table-raw.tpl', [
			'class' => [ 'label' => 'Create Table'],
			'form'=>$form
		]);
	}

	/**
	 * Canonicalise a raw definition by adding any missing parameters that
	 * might be needed (e.g. classLabel)
	 * @param array $data
	 * @return array
	 */
	protected function canonicaliseDefinition($data)
	{
		$definition = [
			'name' => $data['name'],
			'label' => $data['label'],
			'fields' => []
		];
		$fields = json_decode($data['fields'],true);
		if (is_array($fields)) {
			foreach ($fields as $field) {
				$field['classLabel'] = isset($field['classLabel']) ? $field['classLabel'] : ucwords($field['class']);
				$field['label'] = isset($field['label']) ? $field['label'] : ucwords(str_replace(['_','-'],' ',$field['name']));
				$definition['fields'][] = $field;
			}
			return $definition;
		}
		return null;
	}



}
