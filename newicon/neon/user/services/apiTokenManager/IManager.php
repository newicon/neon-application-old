<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2017 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 18/05/2017
 * @package neon
 */

namespace neon\user\services\apiTokenManager;

interface IManager
{
	/**
	 * Get all of the tokens generated by the user.
	 *
	 * @param  string  $userId
	 * @return array
	 */
	public function all($userId);

	/**
	 * Get the given token by its value if it has not expired.
	 *
	 * @param  string  $token
	 * @return Token
	 */
	public function validToken($token);

	/**
	 * Generate a new "web" token for the user.
	 *
	 * @param  $userId
	 * @param  string  $name
	 * @return Token
	 */
	public function createToken($userId, $name);

	/**
	 * Create a new token cookie.
	 *
	 * @param   $user
	 * @return
	 */
	public function createTokenCookie($user);

	/**
	 * Update the given token.
	 *
	 * @param  Token  $token
	 * @param  string  $name
	 * @param  array  $abilities
	 * @return void
	 */
	public function updateToken($token, $name, array $abilities = []);

	/**
	 * Delete all of the expired tokens for the user.
	 *
	 * @param  $user
	 * @return void
	 */
	public function deleteExpiredTokens($user);
}
