<div class="workbenchBody_entity-sidebar" style="padding:20px; background:white;">
	<div class="dds-sidebar " >
		<h4>Object Record</h4>
		{if isset($object._uuid)}
			Uuid: {$object._uuid} <br/>
			Created: {$object._created} <br/>
			Updated: {$object._updated} <br/>
			Deleted: {if ($object._deleted)} true {else} false {/if} <br/>	
		{else}
			Uuid: - <br/>
			Created: - <br/>
			Updated: - <br/>
			Deleted: - <br/>
		{/if}
	</div>
</div>
