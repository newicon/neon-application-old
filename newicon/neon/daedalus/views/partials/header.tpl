{css}.table img { max-width:50px; max-height:50px } .neonGrid .table td, .neonGrid .table tr { max-width:16em; }{/css}
<header class="workbenchHeader">
	<a href="{url route={$back|default:'/daedalus/index/index'}}" class="btn btn-link"><i class="fa fa-chevron-left"></i></a>
	<div class="workbenchHeader_left">
		<h1 class="workbenchHeader_title">{if (isset($title))} {$title} {else} {$class.label} {/if}</h1>
	</div>
	<div class="toolbar">
		{if ($showAdd|default:true)}
			<a href="{url route='/daedalus/index/add-object' type=$class.class_type }" class="btn btn-primary "><i class="fa fa-plus"></i> Add New</a>
		{/if}
		{if ($showEdit|default:true && $can_develop)}
			<a href="{url route='/phoebe/database/index/edit' type=$class.class_type }" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit Definition</a>
		{/if}
	</div>
</header>