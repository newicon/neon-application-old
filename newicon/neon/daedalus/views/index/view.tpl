<div class="workbench">
	{include '../partials/header.tpl' back="/daedalus/index/list?type={$class.class_type}" showAdd=false showEdit=false}
	<div class="workbenchBody">
		<div class="workbenchBody_content workbenchBody_content--form" style="background:#fff">
			<div class="workbenchBody__form">
				{$form->run() nofilter}
			</div>
		</div>
		{include '../partials/objectSidebar.tpl' object=$object}
	</div>
</div>

