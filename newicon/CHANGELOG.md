# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## 1.1.9 
- Security: Updated to latest Yii 2.0.16
- Feature: full support for PHP 5.6 testing `./codecept run`
- Feature: User invites section
- Enhancement: Migrations run in date order
- Security: Url input prevents XSS injection

## 1.1.8
- Enhancement: new neon admin theme (early version)
- Bugs: various minor bug fixes

## 1.1.7
- Enhancement: Cobe CMS page editor updates
- Security: Default sanitation on all get requests
- Enhancement: Ajax form submission

## Media Manager - 2017-06-07
A rather large branch...

### Update instructions
- Update all uses of `$useUuidAsId` in active record classes to a static property and rename to `$idIsUuid64`;
- Change active record public properties `$timestamps`, `$blame` and `$idIsUuid64` to be static
- Config has been split between three files instead of sub folders its now just `common` `console` and `web`

### Changed
- Changed ActiveRecord `$timestamps`, `$blame`, and `$useUuidAsId` to be static properties
- Changed the active record `$useUuidAsId` property to `$idIsUuid64`

### Removed
- Removed config folders and environment specific config files - use environment variables

### Added
- Added the media manager!
- Added more tests
- Mutliple PHP version tested - Scrutinizer config now runs all tests against PHP 5.6 and PHP 7.1
- Added new media manager picker and browser
- Added image editor
- Added User token authentication methods and management table (service manager needs adding)
- Added JWT (json web token) based authentication for API methods 
- Added REST api controller - (\neon\core\web\ApiController) automatically authenticates against apitoken auth and jwt token auth
- Added api tests and public api for firefly media manager
- Added model soft delete behaviour - set any model public property softDelete = true and add a deleted column

### Fixed
- Fix form setAction (can change the action url of a form object)