[![Latest Stable Version](https://poser.pugx.org/newicon/neon/v)](//packagist.org/packages/newicon/neon) [![Build Status](https://scrutinizer-ci.com/b/newicon/neon/badges/build.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/build-status/releases) [![Code Coverage](https://scrutinizer-ci.com/b/newicon/neon/badges/coverage.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/?branch=releases) [![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/newicon/neon/badges/quality-score.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/?branch=releases) [![License](https://poser.pugx.org/newicon/neon/license)](//packagist.org/packages/newicon/neon)


Neon PHP Framework
==================

This is the core Neon Framework.

This is repository should not be included directly in your application.

Use the `neon-application` repository to start a new application project using this.