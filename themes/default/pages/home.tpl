{extends "layouts/layout.tpl"}
{block "content"}
	<h2>Welcome to your new Neon application!</h2>

	<p>Cobe has rendered this page for you because there are entries in the database tables cms_page and cms_url under the base URL '/'.</p>
	<p>These were created from the migration /apps/main/migrations/m20200101_000000_main_create_home_page.php during the installation of this project.</p>
	<p>Cobe renders any pages or partials defined in these tables and stored in the themes directories automatically.<p>

	<br>

	<h3>Themes</h3>
	<p>This file is /themes/default/pages/home.tpl and it extends the /theme/default/layouts/layout.tpl file.</p>
	<p>The layout file in turn uses these widgets from /themes/default/widgets : header.tpl, footer.tpl, and navigation.tpl </p>

	<br>

	<h3>Repository</h3>
	<p>Before making any changes to the code you should dissociate this application from the 'neon-application' repo:
		<ul>
			<li>On the command line go to the project root : Execute 'rm -rf .git'</li>
			<li>You will now probably want to <a href="https://confluence.atlassian.com/bitbucketserver/importing-code-from-an-existing-project-776640909.html#Importingcodefromanexistingproject-Importanexisting,unversionedcodeprojectintoBitbucketServer" target="_blank" rel="noopener noreferrer">setup a bitbucket repo</a> for this project. </li>
		</ul>
	</p>

	<br>

	<h3>Assets</h3>
	<p><a href="https://gruntjs.com/" target="_blank" rel="noopener noreferrer">Grunt</a> is used to watch for changes and automatically compile assets such as css and javascript.</p>
	<div id="grunt-not-installed" style="display:none">
		If you are seeing this message it is because the main stylesheet was not found ... this is probably because grunt has not yet been setup and/or executed:
		<ul>
			<li>On the command line go to /theme</li>
			<li>execute 'npm install', this will install grunt locally to the project</li>
			<li>execute 'grunt', this will compile css/js/etc and begin watching for changes</li>
		</ul>
	</div>
	<script>
		var xhr = new XMLHttpRequest();
		xhr.open('GET', document.querySelector('link[rel=stylesheet]').getAttribute('href'), true);
		xhr.onreadystatechange = function(){
			if (this.readyState === 4 && this.status === 404) {
				document.querySelector("#grunt-not-installed").style.display = 'block';
			}
		};
		xhr.send(null);
	</script>

	<br>

	<h3>Neo</h3>
	<p>Neo is a tool for assisting with various command line tasks.</p>
	<p>To see the available tasks on the comment line go to the project root and execute './neo'</p>

	<br>

	<h3>Automated Tests</h3>
	<p>There are some basic automated tests setup (in /tests) using <a href="https://phpunit.de/">phpunit</a> and <a href="https://codeception.com/">codeception</a></p>
	<p>To run these on the command line got the project root and execute './codecept run'</p>
	<p>The final one of these (/tests/unit/apps/main/ExampleTest.php) deliberately fails!</p>

	<br><br>

	<style>
		body {
			padding:10px;
			background-color:#eee
		}
		h1 {
			color:#0066ff
		}
	</style>

{/block}
