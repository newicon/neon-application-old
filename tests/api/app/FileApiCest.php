<?php

use neon\user\models\AuthAssignment;
use neon\user\models\User;
use \neon\user\services\apiTokenManager\models\UserApiToken;

/**
 * test the firefly upload api
 */
class FileApiCest
{
	/**
	 * @var
	 */
	protected $token;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var AuthAssignment
	 */
	protected $adminAuthAssignment;

	public function _before()
	{
		User::deleteAll();
		$user = new User([
			'email' => 'test@test.com',
			'username' => 'test',
			'password' => 'testesttest'
		]);
		if (!$user->save()) {
			dd($user->getErrors());
		}

		$this->adminAuthAssignment = new AuthAssignment();
		$this->adminAuthAssignment->item_name = 'neon-administrator';
		$this->adminAuthAssignment->user_id = $user->id;
		$this->adminAuthAssignment->save();

		neon()->user->autoRenewCookie = false;
		$token = UserApiToken::generateTokenFor($user->id);
		$this->user = $user;
		$this->token = $token->token;
	}

	public function _after()
	{
		$this->adminAuthAssignment->delete();
		$this->user->delete();
		UserApiToken::deleteAll(['token' => $this->token]);
	}

	public function testActionMeta($I)
	{
		// lets add a file to the filemanager
		$id = neon()->firefly->save('Hello im a new file', 'testfile.txt');
		// lets get meta about it.
		$meta = neon()->firefly->getMeta($id);
		// lets get meta from the API
		$I->sendGET("/firefly/api/file/meta?id=$id&api_token=".$this->token);
		$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);// currently returns 403
		$I->seeResponseContainsJson($meta);
	}
}





