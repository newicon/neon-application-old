<?php

use neon\user\models\User;

/**
 * test logging in with a regular user
 */
class LoginUserCest
{
	const USER_NAME = 'testuser';
	const USER_EMAIL = 'test@test.com';
	const USER_PASSWORD = 'testtesttest';


	/**
	 * @var User
	 */
	public $user;

	public function _before(AcceptanceTester $I)
	{
		$user = new User();
		$user->username = self::USER_NAME;
		$user->email = self::USER_EMAIL;
		$user->password = self::USER_PASSWORD;
		$user->save();
		$this->user = $user;
	}

	public function _after(AcceptanceTester $I)
	{
		$this->user->delete();
	}

	/**
	 * @param AcceptanceTester $I
	 */
	protected function _login($I, $email, $password)
	{
		$I->amOnPage('/login');
		$I->fillField('#login-username', $email);
		$I->fillField('#login-password', $password);
		$I->click('button[type="submit"]');
		$I->wait(1);
	}

	public function login(AcceptanceTester $I)
	{
		$this->_login($I,self::USER_EMAIL,self::USER_PASSWORD);
		$I->amOnPage('/');
	}

}





