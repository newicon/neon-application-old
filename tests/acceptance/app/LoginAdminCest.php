<?php

use neon\user\models\AuthAssignment;
use neon\user\models\User;

require_once('LoginUserCest.php');

/**
 * test logging in with an admin user
 */
class LoginAdminCest extends LoginUserCest
{
	const USER_NAME = 'admintestuser';
	const USER_EMAIL = 'admin@test.com';
	const USER_PASSWORD = 'admintesttest';

	/**
	 * @var User
	 */
	public $user;

	/**
	 * @var AuthAssignment
	 */
	public $adminAuthAssignment;

	/**
	 * @var AuthAssignment
	 */
	public $devAuthAssignment;

	public function _before(AcceptanceTester $I)
	{
		$user = new User();
		$user->username = self::USER_NAME;
		$user->email = self::USER_EMAIL;
		$user->password = self::USER_PASSWORD;
		$user->save();

		$this->adminAuthAssignment = new AuthAssignment();
		$this->adminAuthAssignment->item_name = 'neon-administrator';
		$this->adminAuthAssignment->user_id = $user->id;
		$this->adminAuthAssignment->save();

		$this->devAuthAssignment = new AuthAssignment();
		$this->devAuthAssignment->item_name = 'neon-developer';
		$this->devAuthAssignment->user_id = $user->id;
		$this->devAuthAssignment->save();

		$this->user = $user;
	}

	public function _after(AcceptanceTester $I)
	{
		$this->devAuthAssignment->delete();
		$this->adminAuthAssignment->delete();
		$this->user->delete();
	}

	public function login(AcceptanceTester $I)
	{
		$this->_login($I, self::USER_EMAIL, self::USER_PASSWORD);
		$I->amOnPage('/admin');
		$I->see('dashboard');
		$I->clickWithLeftButton(['css' => '.navbarUserDropdown'],10,10);
		$I->wait(1);
		$I->click(' Log Out');
		$I->wait(1);
		$I->amOnPage('/');
	}

}





