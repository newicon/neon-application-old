<?php

use neon\core\db\Migration;
use neon\core\helpers\Hash;

class m20200101_000000_main_create_home_page extends Migration
{
	public function safeUp()
	{
		$homePageId = Hash::uuid64();
		// create a home page
		$sql = <<<EOS
INSERT INTO {{cms_page}}
	([[id]], [[nice_id]], [[name]], [[title]], [[status]], [[layout]], [[template]], [[keywords]], [[description]], [[languages]], [[page_params]], [[created_at]], [[updated_at]], [[created_by]], [[updated_by]], [[deleted]])
VALUES
	('$homePageId', 'home', 'Home', NULL, 'PUBLISHED', NULL, 'pages/home.tpl', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);
EOS;
		$this->execute($sql);
		$this->execute("INSERT INTO {{cms_url}} ([[url]], [[nice_id]]) VALUES ('/', 'home');");
	}

	public function safeDown()
	{
		// remove the home page
		$this->execute("DELETE FROM {{cms_url}} WHERE [[nice_id]]='home';");
		$this->execute("DELETE FROM {{cms_page}} WHERE [[nice_id]]='home';");
	}
}
