<?php

namespace main;

/**
 * Main app app class
 * @package app
 */
class App extends \neon\core\BaseApp
{
	/**
	 * @inheritdoc
	 */
	public function setup()
	{
		// e.g. url rules
	}

	/**
	 * @inheritdoc
	 */
	public function getName()
	{
		return 'main';
	}


}

